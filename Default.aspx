﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>LOT</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
    <script src="js/jquery.min.js"></script>
    <script src="js/jquery.poptrox.min.js"></script>
    <script src="js/skel.min.js"></script>
    <script src="js/init.js"></script>
    <link href="css/styles.css" rel="stylesheet"/>
    <script src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script src="js/menu_jquery.js"></script>
    <!-- jQuery library (served from Google) -->

    <!-- bxSlider Javascript file -->
    <script src="js/jquery.bxslider.js"></script>
    <!-- bxSlider CSS file -->
    <link href="css/jquery.bxslider.css" rel="stylesheet" />
    <link rel="stylesheet" href="css/skel-noscript.css" />
    <link rel="stylesheet" href="css/style.css" />

    <!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
   
</head>
<body>
    <form id="form1" runat="server" name="login" class="loginform" autocomplete="off">

            <!-- Intro -->
        <div style="width: 50%">
            <table style="background-color: #F0F0F0; margin: 29% 0 0 29%;">
                <tr>
                    <td>
                        <asp:Label ID="Label1" runat="server" Text="USER NAME"></asp:Label></td>
                    <td>
                        <asp:TextBox ID="txtUserName" runat="server" placeholder="User Name" ></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label2" runat="server" Text="PASSWORD"></asp:Label></td>
                    <td>
                        <asp:TextBox ID="txtPassword" runat="server" placeholder="Password" TextMode="Password"></asp:TextBox></td>
                </tr>
             
                <tr>
                    <td>
                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="lotbtn" style="margin-left: 65%;cursor:pointer; padding: 0px 20px 0px 15px; font-size: 22px;-moz-margin-start: 21%;-moz-margin-end: -131%;" OnClick="btnSubmit_Click" />
                        </td>
                    <td>
                        <asp:Button ID="Button1" runat="server" Text="Cancel" CssClass="lotbtn" style="margin-left: 11%;cursor:pointer; padding: 0px 20px 0px 15px; font-size: 22px;" CausesValidation="false" OnClick="Button1_Click"/>
                        </td>                       
                </tr>
            </table>

        </div>
        <script>
            function validateForm() {
                var x = document.forms["login"]["username"].value;
                if (x == null || x == "") {
                    alert("Please fill out the username");
                    return false;
                }
            }
        </script>
    </form>
</body>
</html>
