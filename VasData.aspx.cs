﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class VasData : System.Web.UI.Page
{
    string sGuid;
    object lockTarget = new object();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LotAdminUser"] == null)
        {
            Response.Redirect("Default.aspx");
        }
        else if (!Page.IsPostBack)
        {
            sGuid = Guid.NewGuid().ToString();
            hfGUID.Value = sGuid;
            FillDropDowns();
        }
       
    }

    private void FillDropDowns()
    {
        lock (lockTarget)
        {
            ddlfiletype.DataSource = Apps.lotContext.VASTabOne.ToList();
            ddlfiletype.DataBind();
        }
        lock (lockTarget)
        {
            ddlcateg.DataSource = Apps.lotContext.VASTabTwo.ToList();
            ddlcateg.DataBind();
        }
        lock (lockTarget)
        {
            ddlauthor.DataSource = Apps.lotContext.VASTabThree.ToList();
            ddlauthor.DataBind();
        }
        lock (lockTarget)
        {
            ddl4.DataSource = Apps.lotContext.VASTabFour.ToList();
            ddl4.DataBind();
        }
        lock (lockTarget)
        {
            ddl5.DataSource = Apps.lotContext.VASTabFive.ToList();
            ddl5.DataBind();
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (FUBrandLogo.HasFiles)
        {
             if (FUBrandLogo.FileName != "null" &&   ddlfiletype.SelectedItem.Text != "Select" && ddlcateg.SelectedItem.Text != "Select" && ddlauthor.SelectedItem.Text != "Select" && ddl4.SelectedItem.Text != "Select" && ddl5.SelectedItem.Text != "Select")
             {
                 lock (lockTarget)
                 {
                     string Imagesfolder1 = Server.MapPath("~/Songs/");
                     string strImagesFolder = "Songs";

                     foreach (HttpPostedFile Img in FUBrandLogo.PostedFiles)
                     {

                         Img.SaveAs((Path.Combine(Imagesfolder1, Img.FileName)));

                         lock (lockTarget)
                         {

                             LotDBEntity.Models.ValueAddedServicesData vasdata = new LotDBEntity.Models.ValueAddedServicesData();
                             vasdata.Name = Img.FileName;
                             vasdata.WEBItemLocation = Imagesfolder1 + Img.FileName;
                             vasdata.ItemLocation = "pack://siteoforigin:,,,/" + strImagesFolder + "/" + Img.FileName;
                             LotDBEntity.Models.VASTabOne vastabone = null;
                             LotDBEntity.Models.VASTabTwo vastabtwo = null;
                             LotDBEntity.Models.VASTabThree vastabthree = null;
                             LotDBEntity.Models.VASTabFour vastabfour = null;
                             LotDBEntity.Models.VASTabFive vastabfive = null;
                             lock (lockTarget)
                             {
                                 if (!string.IsNullOrEmpty(ddlfiletype.SelectedItem.Text.Trim()))
                                     vastabone = Apps.lotContext.VASTabOne.Where(c => c.Name.Equals(ddlfiletype.SelectedItem.Text)).FirstOrDefault();
                             }
                             lock (lockTarget)
                             {
                                 if (!string.IsNullOrEmpty(ddlcateg.SelectedItem.Text.Trim()))
                                     vastabtwo = Apps.lotContext.VASTabTwo.Where(c => c.Name.Equals(ddlcateg.SelectedItem.Text)).FirstOrDefault();
                             }
                             lock (lockTarget)
                             {
                                 if (!string.IsNullOrEmpty(ddlauthor.SelectedItem.Text.Trim()))
                                     vastabthree = Apps.lotContext.VASTabThree.Where(c => c.Name.Equals(ddlauthor.SelectedItem.Text)).FirstOrDefault();
                             }
                             lock (lockTarget)
                             {
                                 if (!string.IsNullOrEmpty(ddl4.SelectedItem.Text.Trim()))
                                     vastabfour = Apps.lotContext.VASTabFour.Where(c => c.Name.Equals(ddl4.SelectedItem.Text)).FirstOrDefault();
                             }
                             lock (lockTarget)
                             {
                                 if (!string.IsNullOrEmpty(ddl5.SelectedItem.Text.Trim()))
                                     vastabfive = Apps.lotContext.VASTabFive.Where(c => c.Name.Equals(ddl5.SelectedItem.Text)).FirstOrDefault();
                             }

                             vasdata.VASTabOne = vastabone;
                             vasdata.VASTabTwo = vastabtwo;
                             vasdata.VASTabThree = vastabthree;
                             vasdata.VASTabFour = vastabfour;
                             vasdata.VASTabFive = vastabfive;
                             lock (lockTarget)
                             {
                                 long id1 = 0;
                                 if (Apps.lotContext.ValueAddedServicesData.ToList().Count > 0)
                                     id1 = Apps.lotContext.ValueAddedServicesData.Max(c => c.ID);
                                 if (id1 > 0)
                                 {
                                     vasdata.ID = id1 + 1;
                                 }
                                 else
                                 {
                                     vasdata.ID = 1;
                                 }
                             }
                             vasdata.LastUpdatedTime = DateTime.Now;
                             Apps.lotContext.ValueAddedServicesData.Add(vasdata);
                             Apps.lotContext.SaveChanges();
                         }
                     }
                     
                     ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Saved successfully ');", true);
                     ddlfiletype.ClearSelection();
                     ddlfiletype.Items.FindByText("Select").Selected = true;
                     ddlcateg.ClearSelection();
                     ddlcateg.Items.FindByText("Select").Selected = true;
                     ddlauthor.ClearSelection();
                     ddlauthor.Items.FindByText("Select").Selected = true;
                     ddl4.ClearSelection();
                     ddl4.Items.FindByText("Select").Selected = true;
                     ddl5.ClearSelection();
                     ddl5.Items.FindByText("Select").Selected = true;
                 }
             }
             else
             {
                 ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Please Select all the fields ');", true);
             }
                
           
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {

    }
}