﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ValueAddedServices : System.Web.UI.Page
{
    string sGuid;
    object lockTarget = new object();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LotAdminUser"] == null)
        {
            Response.Redirect("Default.aspx");
        }
        else if (!Page.IsPostBack)
        {
            sGuid = Guid.NewGuid().ToString();
            hfGUID.Value = sGuid;
            FirstGridViewRow();

        }
        FillDropDowns();
        
    }
    private void FirstGridViewRow()
    {
        DataTable dt = new DataTable();
        DataRow dr = null;
        dt.Columns.Add(new DataColumn("RowNumber", typeof(string)));
        dt.Columns.Add(new DataColumn("Col1", typeof(string)));
        dt.Columns.Add(new DataColumn("Col2", typeof(string)));
        dt.Columns.Add(new DataColumn("Col3", typeof(string)));
        dt.Columns.Add(new DataColumn("Col4", typeof(string)));
        dt.Columns.Add(new DataColumn("Col5", typeof(string)));
        dt.Columns.Add(new DataColumn("Col6", typeof(string)));
        dt.Columns.Add(new DataColumn("Col7", typeof(string)));
        dt.Columns.Add(new DataColumn("Col8", typeof(string)));
        dr = dt.NewRow();
        dr["RowNumber"] = 1;
        dr["Col1"] = string.Empty;
        dr["Col2"] = string.Empty;
        dr["Col3"] = string.Empty;
        dr["Col4"] = string.Empty;
        dr["Col5"] = string.Empty;
        dr["Col6"] = string.Empty;
        dr["Col7"] = string.Empty;
        dr["Col8"] = string.Empty;
        dt.Rows.Add(dr);

        ViewState["CurrentTable"] = dt;


        gvServices.DataSource = dt;
        gvServices.DataBind();
        
        Button btnAdd = (Button)gvServices.FooterRow.Cells[5].FindControl("ButtonAdd");
        Page.Form.DefaultFocus = btnAdd.ClientID;

    }

    private void FillDropDowns()
    {
        DropDownList ddl1 = (DropDownList)gvServices.Rows[0].Cells[2].FindControl("ddlfiletype");
        DropDownList ddl2 = (DropDownList)gvServices.Rows[0].Cells[3].FindControl("ddlcateg");
        DropDownList ddl3 = (DropDownList)gvServices.Rows[0].Cells[4].FindControl("ddlauthor");
        DropDownList ddl4 = (DropDownList)gvServices.Rows[0].Cells[5].FindControl("ddl4");
        DropDownList ddl5 = (DropDownList)gvServices.Rows[0].Cells[6].FindControl("ddl5");
        //TextBox txn = (TextBox)gvServices.Rows[0].Cells[1].FindControl("txtName");
        //txn.Focus();
        lock (lockTarget)
        {
            ddl1.DataSource = Apps.lotContext.VASTabOne.ToList();
            ddl1.DataBind();
        }
        lock (lockTarget)
        {
            ddl2.DataSource = Apps.lotContext.VASTabTwo.ToList();
            ddl2.DataBind();
        }
        lock (lockTarget)
        {
            ddl3.DataSource = Apps.lotContext.VASTabThree.ToList();
            ddl3.DataBind();
        }
        lock (lockTarget)
        {
            ddl4.DataSource = Apps.lotContext.VASTabFour.ToList();
            ddl4.DataBind();
        }
        lock (lockTarget)
        {
            ddl5.DataSource = Apps.lotContext.VASTabFive.ToList();
            ddl5.DataBind();
        }
    }

    //private void AddNewRow()
    //{
    //    int rowIndex = 0;

    //    if (ViewState["CurrentTable"] != null)
    //    {
    //        DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
    //        DataRow drCurrentRow = null;
    //        DropDownList ddl1, ddl2, ddl3, ddl4, ddl5;
    //        if (dtCurrentTable.Rows.Count > 0)
    //        {
    //            for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
    //            {
    //                FileUpload fp1 = (FileUpload)gvServices.Rows[rowIndex].Cells[1].FindControl("fp1");
    //                ddl1 = (DropDownList)gvServices.Rows[rowIndex].Cells[2].FindControl("ddlfiletype");
    //                ddl2 = (DropDownList)gvServices.Rows[rowIndex].Cells[3].FindControl("ddlcateg");
    //                ddl3 = (DropDownList)gvServices.Rows[rowIndex].Cells[4].FindControl("ddlauthor");
    //                ddl4 = (DropDownList)gvServices.Rows[rowIndex].Cells[5].FindControl("ddl4");
    //                ddl5 = (DropDownList)gvServices.Rows[rowIndex].Cells[6].FindControl("ddl5");
    //                drCurrentRow = dtCurrentTable.NewRow();
    //                drCurrentRow["RowNumber"] = i + 1;
    //                //string fileName = Path.GetFileName(fp1.PostedFile.FileName) + System.IO.Path.GetExtension(fp1.PostedFile.FileName);
    //                dtCurrentTable.Rows[i - 1]["Col1"] = fp1.PostedFile.FileName;
    //                dtCurrentTable.Rows[i - 1]["Col2"] = ddl1.SelectedItem.Text;
    //                dtCurrentTable.Rows[i - 1]["Col3"] = ddl2.SelectedItem.Text;
    //                dtCurrentTable.Rows[i - 1]["Col4"] = ddl3.SelectedItem.Text;
    //                dtCurrentTable.Rows[i - 1]["Col5"] = ddl4.SelectedItem.Text;
    //                dtCurrentTable.Rows[i - 1]["Col6"] = ddl5.SelectedItem.Text;
    //                string str = dtCurrentTable.Rows[i - 1]["Col7"].ToString();
    //                dtCurrentTable.Rows[i - 1]["Col7"] = fp1.PostedFile.FileName; //dtCurrentTable.Rows[i - 1]["Col7"];
    //                rowIndex++;
    //            }
    //            dtCurrentTable.Rows.Add(drCurrentRow);
    //            ViewState["CurrentTable"] = dtCurrentTable;

    //            gvServices.DataSource = dtCurrentTable;
    //            gvServices.DataBind();
    //        }
    //    }
    //    else
    //    {
    //        Response.Write("ViewState is null");
    //    }
    //    SetPreviousData();
    //}
    //private void SetPreviousData()
    //{
    //    int rowIndex = 0;
    //    if (ViewState["CurrentTable"] != null)
    //    {
    //        DataTable dt = (DataTable)ViewState["CurrentTable"];
    //        if (dt.Rows.Count > 0)
    //        {
    //            for (int i = 0; i < dt.Rows.Count; i++)
    //            {
    //                FileUpload fp1 = (FileUpload)gvServices.Rows[rowIndex].Cells[1].FindControl("fp1");
    //                DropDownList ddl1 = (DropDownList)gvServices.Rows[rowIndex].Cells[2].FindControl("ddlfiletype");
    //                DropDownList ddl2 = (DropDownList)gvServices.Rows[rowIndex].Cells[3].FindControl("ddlcateg");
    //                DropDownList ddl3 = (DropDownList)gvServices.Rows[rowIndex].Cells[4].FindControl("ddlauthor");
    //                DropDownList ddl4 = (DropDownList)gvServices.Rows[rowIndex].Cells[5].FindControl("ddl4");
    //                DropDownList ddl5 = (DropDownList)gvServices.Rows[rowIndex].Cells[6].FindControl("ddl5");
    //                // drCurrentRow["RowNumber"] = i + 1;
                   
    //                gvServices.Rows[i].Cells[0].Text = Convert.ToString(i + 1);
    //                ddl1.SelectedItem.Text = dt.Rows[i]["Col2"].ToString();
    //                ddl2.SelectedItem.Text = dt.Rows[i]["Col3"].ToString();
    //                ddl3.SelectedItem.Text = dt.Rows[i]["Col4"].ToString();
    //                ddl4.SelectedItem.Text = dt.Rows[i]["Col5"].ToString();
    //                ddl5.SelectedItem.Text = dt.Rows[i]["Col6"].ToString();
    //                ddl1.Items.Add("Select");
    //                ddl2.Items.Add("Select");
    //                ddl3.Items.Add("Select");
    //                ddl4.Items.Add("Select");
    //                ddl5.Items.Add("Select");
    //                ddl1.DataSource = Apps.lotContext.VASTabOne.ToList();
    //                ddl1.DataBind();
    //                ddl2.DataSource = Apps.lotContext.VASTabTwo.ToList();
    //                ddl2.DataBind();
    //                ddl3.DataSource = Apps.lotContext.VASTabThree.ToList();
    //                ddl3.DataBind();
    //                ddl4.DataSource = Apps.lotContext.VASTabFour.ToList();
    //                ddl4.DataBind();
    //                ddl5.DataSource = Apps.lotContext.VASTabFive.ToList();
    //                ddl5.DataBind();
    //                rowIndex++;
    //            }
    //        }
    //    }
    //}
    //protected void ButtonAdd_Click(object sender, EventArgs e)
    //{
    //    AddNewRow();
    //}
    //protected void grvStudentDetails_RowDeleting(object sender, GridViewDeleteEventArgs e)
    //{
       
    //}

    //private void SetRowData()
    //{
    //    int rowIndex = 0;

    //    if (ViewState["CurrentTable"] != null)
    //    {
    //        DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
    //        DataRow drCurrentRow = null;
    //        if (dtCurrentTable.Rows.Count > 0)
    //        {
    //            for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
    //            {
    //                FileUpload fp1 = (FileUpload)gvServices.Rows[rowIndex].Cells[1].FindControl("fp1");
    //                DropDownList ddl1 = (DropDownList)gvServices.Rows[rowIndex].Cells[2].FindControl("ddlfiletype");
    //                DropDownList ddl2 = (DropDownList)gvServices.Rows[rowIndex].Cells[3].FindControl("ddlcateg");
    //                DropDownList ddl3 = (DropDownList)gvServices.Rows[rowIndex].Cells[4].FindControl("ddlauthor");
    //                DropDownList ddl4 = (DropDownList)gvServices.Rows[rowIndex].Cells[5].FindControl("ddl4");
    //                DropDownList ddl5 = (DropDownList)gvServices.Rows[rowIndex].Cells[6].FindControl("ddl5");
    //                drCurrentRow = dtCurrentTable.NewRow();
    //                drCurrentRow["RowNumber"] = i + 1;
    //                string fileName = Path.GetFileName(fp1.PostedFile.FileName);
    //                string folder = Server.MapPath("~/Songs/");
    //                string strFolder = "Songs";
    //                string strSongs = fileName;
    //                //Location = "pack://siteoforigin:,,,/" + strImagesFolder + "/" + hfGUID.Value + "~" + strRandomName, WebLocation = Imagesfolder1 + hfGUID.Value + "~" + strRandomName
    //                //item1.SaveAs(Path.Combine(folder, hfGUID.Value + "~" + fileName)); // ~ is for splitt
    //                fp1.SaveAs(Path.Combine(folder, hfGUID.Value + "~" + fileName));
    //                dtCurrentTable.Rows[i - 1]["Col1"] ="pack://siteoforigin:,,,/"+ strFolder+"/"+ hfGUID.Value + "~" + fileName;
    //                dtCurrentTable.Rows[i - 1]["Col2"] = ddl1.SelectedItem.Text;
    //                dtCurrentTable.Rows[i - 1]["Col3"] = ddl2.SelectedItem.Text;
    //                dtCurrentTable.Rows[i - 1]["Col4"] = ddl3.SelectedItem.Text;
    //                dtCurrentTable.Rows[i - 1]["Col5"] = ddl4.SelectedItem.Text;
    //                dtCurrentTable.Rows[i - 1]["Col6"] = ddl5.SelectedItem.Text;
    //                dtCurrentTable.Rows[i - 1]["Col7"] = fp1.PostedFile.FileName;
    //                dtCurrentTable.Rows[i - 1]["Col8"] = folder + hfGUID.Value + "~" + fileName;
    //                rowIndex++;
    //            }

    //            ViewState["CurrentTable"] = dtCurrentTable;
    //            //grvStudentDetails.DataSource = dtCurrentTable;
    //            //grvStudentDetails.DataBind();
    //        }
    //    }
    //    else
    //    {
    //        Response.Write("ViewState is null");
    //    }
    //    //SetPreviousData();
    //}
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            //SetRowData();
            DataTable table = ViewState["CurrentTable"] as DataTable;

            if (table != null)
            {
                foreach (DataRow row in table.Rows)
                {
                    string fpaudio = row.ItemArray[1] as string;
                    string name = row.ItemArray[7] as string;
                    string ddl1 = row.ItemArray[2] as string;
                    string ddl2 = row.ItemArray[3] as string;
                    string ddl3 = row.ItemArray[4] as string;
                    string ddl4 = row.ItemArray[5] as string;
                    string ddl5 = row.ItemArray[6] as string;
                    string fpweb = row.ItemArray[8] as string;

                    if (fpaudio != "null" && name != "Select" && fpweb != "Select" && ddl1 != "Select" && ddl2 != "Select" && ddl3 != "Select" && ddl4 != "Select" && ddl5 != "Select")
                    {
                        // Do whatever is needed with this data, 
                        // Possibily push it in database
                        // I am just printing on the page to demonstrate that it is working.
                        lock (lockTarget)
                        {
                            
                            LotDBEntity.Models.ValueAddedServicesData vasdata = new LotDBEntity.Models.ValueAddedServicesData();
                            vasdata.Name = name;
                            vasdata.WEBItemLocation = fpweb;
                            vasdata.ItemLocation = fpaudio;
                            LotDBEntity.Models.VASTabOne vastabone = null;
                            LotDBEntity.Models.VASTabTwo vastabtwo = null;
                            LotDBEntity.Models.VASTabThree vastabthree = null;
                            LotDBEntity.Models.VASTabFour vastabfour = null;
                            LotDBEntity.Models.VASTabFive vastabfive = null;
                            lock (lockTarget)
                            {
                                if (!string.IsNullOrEmpty(ddl1.Trim()))
                                    vastabone = Apps.lotContext.VASTabOne.Where(c => c.Name.Equals(ddl1)).FirstOrDefault();
                            }
                            lock (lockTarget)
                            {
                                if (!string.IsNullOrEmpty(ddl2.Trim()))
                                    vastabtwo = Apps.lotContext.VASTabTwo.Where(c => c.Name.Equals(ddl2)).FirstOrDefault();
                            }
                            lock (lockTarget)
                            {
                                if (!string.IsNullOrEmpty(ddl3.Trim()))
                                    vastabthree = Apps.lotContext.VASTabThree.Where(c => c.Name.Equals(ddl3)).FirstOrDefault();
                            }
                            lock (lockTarget)
                            {
                                if (!string.IsNullOrEmpty(ddl4.Trim()))
                                    vastabfour = Apps.lotContext.VASTabFour.Where(c => c.Name.Equals(ddl4)).FirstOrDefault();
                            }
                            lock (lockTarget)
                            {
                                if (!string.IsNullOrEmpty(ddl5.Trim()))
                                    vastabfive = Apps.lotContext.VASTabFive.Where(c => c.Name.Equals(ddl5)).FirstOrDefault();
                            }

                            vasdata.VASTabOne = vastabone;
                            vasdata.VASTabTwo = vastabtwo;
                            vasdata.VASTabThree = vastabthree;
                            vasdata.VASTabFour = vastabfour;
                            vasdata.VASTabFive = vastabfive;
                            lock (lockTarget)
                            {
                                long id1 = 0;
                                if (Apps.lotContext.ValueAddedServicesData.ToList().Count > 0)
                                    id1 = Apps.lotContext.ValueAddedServicesData.Max(c => c.ID);
                                if (id1 > 0)
                                {
                                    vasdata.ID = id1 + 1;
                                }
                                else
                                {
                                    vasdata.ID = 1;
                                }
                            }
                            vasdata.LastUpdatedTime = DateTime.Now;
                            Apps.lotContext.ValueAddedServicesData.Add(vasdata);
                            Apps.lotContext.SaveChanges();
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Saved successfully ');", true);
                            FirstGridViewRow();
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Please Select all the fields ');", true);
                    }

                }
            }
        }
        catch (Exception ex)
        {
            LogHelper.Logger.ErrorException("Exception: Saving  : JukeBox: " + ex.Message, ex);
        }
    }
    protected void gvServices_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        //SetRowData();
        if (ViewState["CurrentTable"] != null)
        {
            DataTable dt = (DataTable)ViewState["CurrentTable"];
            DataRow drCurrentRow = null;
            int rowIndex = Convert.ToInt32(e.RowIndex);
            if (dt.Rows.Count > 1)
            {
                dt.Rows.Remove(dt.Rows[rowIndex]);
                drCurrentRow = dt.NewRow();
                ViewState["CurrentTable"] = dt;
                gvServices.DataSource = dt;
                gvServices.DataBind();

                for (int i = 0; i < gvServices.Rows.Count - 1; i++)
                {
                    gvServices.Rows[i].Cells[0].Text = Convert.ToString(i + 1);
                }
                //SetPreviousData();
            }
        }
    }
}