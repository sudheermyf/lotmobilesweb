﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AddAccessories : System.Web.UI.Page
{
    LotDBEntity.Models.Category Categ;
    object lockTarget = new object();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LotAdminUser"] == null)
        {
            Response.Redirect("Default.aspx");
        }
        else
        {
            if (!IsPostBack)
            {
                ddlCategory.Items.Add("Select");
                lock (lockTarget)
                {
                    var brands = Apps.lotContext.Brands.ToList();
                    ddlBrand.DataTextField = "Name";
                    ddlBrand.DataValueField = "SNo";
                    ddlBrand.DataSource = brands;
                    ddlBrand.DataBind();
                }
                if (Request.QueryString["SNo"] != null)
                {
                    btnSubmit.Text = "Update";
                    btnCancel.Text = "Reset";
                    lbladd.Text = "Edit Accessory";
                    GetUpdateData();
                }
            }
        }
    }

    private void GetUpdateData()
    {
        LogHelper.Logger.Info("started GetUpdateData()");
        ddlBrand.Items.FindByText(Request.QueryString["Brand"].ToString()).Selected = true;
        ddlCategoryBinding();
        ddlCategory.Items.FindByText(Request.QueryString["Categ"].ToString()).Selected = true;
        ddlBrand.Enabled = false;
        ddlCategory.Enabled = false;

        long val = Convert.ToInt64(Request.QueryString["SNo"].ToString());

        lock (lockTarget)
        {
            LotDBEntity.Models.BasicAccessories AccessoryList = Apps.lotContext.BasicAccessories.Where(c => c.SNo == val).FirstOrDefault();
            lblId.Text = AccessoryList.SNo.ToString();
            lblImgLocation.Text = AccessoryList.ImageName;
            //string strFile = AccessoryList.ImageLocation;
            //fplogo.Attributes.Add(strFile,fplogo.FileName);
            txtaccesname.Text = AccessoryList.Name;
            txtprice.Text = AccessoryList.Price.ToString();
            txtkey1.Text = AccessoryList.KeyFeature1;
            txtkey2.Text = AccessoryList.KeyFeature2;
            txtkey3.Text = AccessoryList.KeyFeature3;
            txtkey4.Text = AccessoryList.KeyFeature4;
        }
        LogHelper.Logger.Info("started GetUpdateData()");
    }
    
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (btnSubmit.Text == "Submit")
        {
            if (txtexist.Text != "Already Exists" && ddlBrand.SelectedItem.Text != "Select" && ddlCategory.SelectedItem.Text != "Select" && !string.IsNullOrEmpty(txtaccesname.Text))
            {
                try
                {
                    lock (lockTarget)
                    {
                        LogHelper.Logger.Info("Starts Creating an accessory in Submit Button");
                        LotDBEntity.Models.BasicAccessories accessory = new LotDBEntity.Models.BasicAccessories();
                        long id = 0;
                        if (Apps.lotContext.BasicAccessories.ToList().Count > 0)
                            id = Apps.lotContext.BasicAccessories.Max(c => c.ID);
                        if (id > 0)
                        {
                            accessory.ID = id + 1;
                        }
                        else
                        {
                            accessory.ID = 1;
                        }

                        accessory.Name = txtaccesname.Text.ToUpper().Trim();
                        long val1 = Convert.ToInt64(ddlBrand.SelectedValue);
                        brand = Apps.lotContext.Brands.Where(c => c.SNo == val1).FirstOrDefault();
                        long val = Convert.ToInt64(ddlCategory.SelectedValue);
                        Categ = Apps.lotContext.Categories.Where(c => c.SNo == val).FirstOrDefault();
                        if (fplogo.HasFile)
                        {
                            string extension = Path.GetExtension(fplogo.PostedFile.FileName);
                            string fileName = Path.GetFileName(fplogo.FileName);
                            string strAccessoryImages = new Random().Next(1000, 100000).ToString() + System.IO.Path.GetExtension(fileName);
                            accessory.ImageName = strAccessoryImages;
                            string folder = Server.MapPath("~/AccessoryImages/");
                            string strFolder = "AccessoryImages";
                            Directory.CreateDirectory(folder);
                            fplogo.PostedFile.SaveAs(Path.Combine(folder, strAccessoryImages));
                            accessory.WEBImageLocation = folder + strAccessoryImages;
                            accessory.ImageLocation = "pack://siteoforigin:,,,/" + strFolder + "/" + strAccessoryImages;
                        }
                        accessory.Price = Convert.ToDecimal(txtprice.Text);
                        accessory.KeyFeature1 = txtkey1.Text.ToUpper();
                        accessory.KeyFeature2 = txtkey2.Text.ToUpper();
                        accessory.KeyFeature3 = txtkey3.Text.ToUpper();
                        accessory.KeyFeature4 = txtkey4.Text.ToUpper();
                        accessory.brand = brand;
                        accessory.category = Categ;
                        accessory.LastUpdatedTime = DateTime.Now;
                        Apps.lotContext.BasicAccessories.Add(accessory);
                        Apps.lotContext.SaveChanges();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Accessory Saved Successfully ');window.location='AddAccessories.aspx'", true);
                        btnSubmit.Enabled = true;
                        LogHelper.Logger.Info("Ends Created an accessory in Submit Button");
                        LogHelper.Logger.Info("Completes Saving an Accessory");
                    }
                }
                catch (Exception ex)
                {
                    LogHelper.Logger.ErrorException("Exception: Submit  : AddAccessories: " + ex.Message, ex);
                }
                
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "name", "SuccessMsg();", true);
            }
        }
        else if (btnSubmit.Text == "Update")
        {
            if (txtexist.Text != "Already Exists")
            {
                try
                {
                    lock(lockTarget)
                    {
                    long val = Convert.ToInt64(lblId.Text);
                    LogHelper.Logger.Info("Started When Update Button Clicks");
                    LotDBEntity.Models.BasicAccessories AccessoryUpdate = Apps.lotContext.BasicAccessories.Where(c => c.SNo == val).FirstOrDefault();
                    if (AccessoryUpdate != null)
                    {
                        if (AccessoryUpdate.Name != txtaccesname.Text.Trim())
                        {
                            checkAccessoryName();
                        }
                        if (txtexist.Text == "Already Exists")
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Item name already exists');", true);
                        }
                        else
                        {
                            btnSubmit.Enabled = false;
                            AccessoryUpdate.Name = txtaccesname.Text.ToUpper().Trim();
                            AccessoryUpdate.Price = Convert.ToDecimal(txtprice.Text);
                            if (fplogo.HasFile)
                            {
                                string extension = Path.GetExtension(fplogo.PostedFile.FileName);

                                string fileName = Path.GetFileName(fplogo.FileName);
                                string strAccessoryImages = new Random().Next(1000, 100000).ToString() + System.IO.Path.GetExtension(fileName);
                                AccessoryUpdate.ImageName = strAccessoryImages;
                                string folder = Server.MapPath("~/AccessoryImages/");
                                string strFolder = "AccessoryImages";
                                Directory.CreateDirectory(folder);

                                fplogo.PostedFile.SaveAs(Path.Combine(folder, strAccessoryImages));
                                AccessoryUpdate.WEBImageLocation = folder + strAccessoryImages;
                                AccessoryUpdate.ImageLocation = "pack://siteoforigin:,,,/" + strFolder + "/" + strAccessoryImages;
                            }
                            AccessoryUpdate.KeyFeature1 = txtkey1.Text.ToUpper();
                            AccessoryUpdate.KeyFeature2 = txtkey2.Text.ToUpper();
                            AccessoryUpdate.KeyFeature3 = txtkey3.Text.ToUpper();
                            AccessoryUpdate.KeyFeature4 = txtkey4.Text.ToUpper();
                            AccessoryUpdate.LastUpdatedTime = DateTime.Now;
                            Apps.lotContext.SaveChanges();
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Accessory Updated Successfully ');window.location='AddAccessories.aspx'", true);
                            LogHelper.Logger.Info("Ended When Update Button Clicks");
                            //ScriptManager.RegisterStartupScript(this, this.GetType(), "name", "UpdateMsg();", true);
                        }
                    }
                    }
                }
                catch (Exception ex)
                {
                    LogHelper.Logger.ErrorException("Exception: Update  : AddAccessories: " + ex.Message, ex);
                }
                
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Please Enter Required Fields')", true);
            }
        }
    }

    private void ClearAll()
    {
        Errtxtprice.Text = "";
        Errfplogo.Text = "";
        Errtxtaccesname.Text = "";
        ErrddlCateg.Text = "";
        ErrddlBrand.Text = "";
        txtaccesname.Text = "";
        txtkey1.Text = "";
        txtkey2.Text = "";
        txtkey3.Text = "";
        txtkey4.Text = "";
        txtprice.Text = "";
    }

    LotDBEntity.Models.Brands brand;
    protected void ddlBrand_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlCategoryBinding();
    }

    private void ddlCategoryBinding()
    {
        if (ddlBrand.SelectedItem.Text != "Select")
        {
            ddlCategory.Items.Clear();
            long val = Convert.ToInt64(ddlBrand.SelectedValue);
            brand = new LotDBEntity.Models.Brands();
            lock (lockTarget)
            {
                brand = Apps.lotContext.Brands.Where(c => c.SNo == val).FirstOrDefault();
                if (brand.LstCategory == null)
                {
                    ddlCategory.Items.Clear();
                    ddlCategory.Items.Add("Select");
                }
                else
                {
                    var category = brand.LstCategory;
                    ddlCategory.DataTextField = "Name";
                    ddlCategory.DataValueField = "SNo";
                    ddlCategory.DataSource = category;
                    ddlCategory.DataBind();
                }
            }
        }
        else
        {
            ddlBrand.ClearSelection();
            ddlBrand.Items.FindByText("Select").Selected = true;
            ddlCategory.Items.Clear();
            ddlCategory.Items.Add("Select");
            ClearAll();
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        
        if (btnCancel.Text == "Cancel")
        {
            LogHelper.Logger.Info("Starts when cancel button clicks");
            ClearAll();
            ddlBrand.ClearSelection();
            ddlBrand.Items.FindByText("Select").Selected = true;
            ddlCategory.Items.Clear();
            ddlCategory.Items.Add("Select");
            LogHelper.Logger.Info("Ends when cancel button clicks");
        }
        else
        {
            LogHelper.Logger.Info("Starts when reset button clicks");
            GetUpdateData();
            LogHelper.Logger.Info("Ends when reset button clicks");
        }
    }

    LotDBEntity.Models.BasicAccessories accessory;
    protected void txtaccesname_TextChanged(object sender, EventArgs e)
    {
        LogHelper.Logger.Info("Starts when txtaccesname_TextChanged");
        checkAccessoryName();
        LogHelper.Logger.Info("Starts when txtaccesname_TextChanged");
    }

    private void checkAccessoryName()
    {
        string strName = txtaccesname.Text.ToUpper().Trim();
        if (ddlCategory.SelectedItem.Text != "Select")
        {
            long val = Convert.ToInt64(ddlCategory.SelectedValue);
            lock (lockTarget)
            {
                LotDBEntity.Models.Category Categ = Apps.lotContext.Categories.Where(c => c.SNo == val).FirstOrDefault();
                if (Categ != null)
                {
                    if (Categ.LstAccessories != null)
                    {
                        lock (lockTarget)
                        {
                            accessory = Apps.lotContext.BasicAccessories.Where(c => c.Name == strName).FirstOrDefault();
                        }
                    }
                    else
                    {
                        accessory = null;
                    }
                }
            }
            if (accessory != null)
            {
                txtexist.Text = "Already Exists";
            }
            else
            {
                accessory = new LotDBEntity.Models.BasicAccessories();
                txtexist.Text = "";
            }
        }
        else
        {
            ClearAll();
        }
    }
}