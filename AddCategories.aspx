﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMasterPage.master" AutoEventWireup="true" CodeFile="AddCategories.aspx.cs" Inherits="AddCategories" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .ErrMsg {
            color: #DD4B39;
            font-family: Arial;
            font-size: 10px;
        }

        .pgHeading {
            font-weight: 600;
            font-family: sans-serif;
            color: #92278f;
        }
    </style>
    
     <script type="text/javascript">
         function Validate() {
             var ddlBrand = document.getElementById('<%=ddlBrand.ClientID %>');
             var errBrand = document.getElementById('<%=errBrand.ClientID %>');
             var txtCategoryName = document.getElementById('<%=txtCategoryName.ClientID %>');
             var txtexist = document.getElementById('<%=txtexist.ClientID %>'); 
             var ErrCategNull = document.getElementById('<%=ErrCategNull.ClientID %>');

             var ddlBrand1 = $(ddlBrand).find('option').filter(':selected').text();

             if (ddlBrand1 == "Select" || ddlBrand1 == "") {
                 errBrand.innerHTML = "Select Brand";
             }
             else {
                 errBrand.innerHTML = "";
             }
            
             if (txtCategoryName.value.length == 0) {
                 ErrCategNull.innerHTML = "Enter Category Name";
             }
             else{
                 ErrCategNull.innerHTML = "";
             }

             if (errBrand.innerHTML == "" && ErrCategNull.innerHTML == "" && txtexist.textContent != "Already Exists") {
                 return true;
             }
             return false;
         }
     </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div style="margin: 5% 0 0 0; text-align: left; padding: 2% 0 0% 11.5%;">
        <h2 style="font-size: 28px;">
            <asp:Label ID="lbladd" runat="server" Text="Add Category" class="pgHeading" />
        </h2>
    </div>
    <div style="width: 100%; padding: 3% 0 0 23%; margin: -8% 0 0 0;">
        <asp:UpdatePanel ID="upcategory" runat="server" UpdateMode="Always">
            <ContentTemplate>

                <table style="width: 100%; margin: 6% 0 4% 6%;">
                    <tr>
                        <td style="width:15%;">
                            <asp:Label ID="Label2" runat="server" Text="Select Brand"/></td>
                        <td>
                            <asp:DropDownList ID="ddlBrand" runat="server" CssClass="ddl" onblur="javascript:return Validate()" AutoPostBack="true" AppendDataBoundItems="true" Width="45%" OnSelectedIndexChanged="ddlBrand_SelectedIndexChanged">
                                <asp:ListItem Value="-1">Select</asp:ListItem>
                            </asp:DropDownList>
                            <asp:Label ID="errBrand" runat="server" class="ErrMsg"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label1" runat="server" Text="Category Name" /></td>
                        <td>

                            <asp:UpdatePanel ID="upbrand" runat="server" UpdateMode="Always">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtCategoryName" runat="server" style="text-transform:uppercase" placeholder="Category Name" onblur="javascript:return Validate()" Width="45%" AutoPostBack="true" OnTextChanged="txtCategoryName_TextChanged" />

                                    <asp:Label ID="txtexist" runat="server" class="ErrMsg" />
                                    <asp:Label ID="ErrCategNull" runat="server" class="ErrMsg" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <asp:Label ID="Label3" runat="server" Text="Category Description"/><br />
                            <asp:Label ID="Label7" Style="Font-Size: 12px;" runat="server" Text="(Optional)"/></td>
                        <td>
                            <asp:TextBox ID="txtDesc" runat="server" placeholder="Description" Width="45%" TextMode="MultiLine"/></td>
                    </tr>
                </table>

            </ContentTemplate>

        </asp:UpdatePanel>
        <table style="margin: 6% 0 0 0%;">
            <tr>
                <td></td>
                <td style="padding: 0 0 0 12.1%;">
                    <asp:Button ID="btnSubmit" runat="server" Style="margin-left: 10%;" Text="Submit" OnClientClick="javascript: return Validate()" CssClass="lotbtn" OnClick="btnSubmit_Click" />
                    <asp:Button ID="Button1" runat="server" Text="Cancel" CssClass="lotbtn" OnClick="Button1_Click" />
                </td>
            </tr>
        </table>

    </div>
</asp:Content>

