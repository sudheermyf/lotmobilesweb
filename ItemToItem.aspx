﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMasterPage.master" AutoEventWireup="true" CodeFile="ItemToItem.aspx.cs" Inherits="ItemToItem" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .ErrMsg {
            color: #DD4B39;
            font-family: Arial;
            font-size: 10px;
        }
        .pgHeading {
            font-weight: 600;
            font-family: sans-serif;
            color: #92278f;
        }
    </style>
     <script type="text/javascript">
         function ValidateListBox(sender, args) {
             var listBox = document.getElementById("<%=lstSelectedSimilarItems.ClientID %>");
            var options = listBox.getElementsByTagName("option");
            var count = 0;
            for (var i in options) {
                if (options[i].selected) {
                    count++;
                }
            }
            args.IsValid = count <= 4;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div style="margin: 5% 0 0 0; text-align: left; padding: 2% 0 0% 11.5%;">
        <h2 style="font-size: 28px;">
            <asp:Label ID="lbladd" runat="server" Text="Item To Similar Items" class="pgHeading" /></h2>
    </div>

    <div style="margin: 2% 0 3% 11.5%; float: left; width: 90%;">
        <asp:UpdatePanel ID="uplinkitem" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <div style="width: 35%; float: left;">
                    <br />
                    <asp:ListBox ID="lstAvalilableItems" runat="server" Width="90%" AutoPostBack="true" OnSelectedIndexChanged="lstAvalilableItems_SelectedIndexChanged" Height="353px"></asp:ListBox>
                    <asp:Label ID="lblnote" runat="server" Text="Note: Press Ctrl key to update Similar Products If not it will remove previous items." CssClass="ErrMsg"></asp:Label>
                </div>
                <div style="width: 35%; float: left;">
                     <asp:Label ID="lblerr" runat="server" CssClass="ErrMsg" Text="Maximum 3 Items To Be Linked"></asp:Label>
                    <asp:ListBox ID="lstSelectedSimilarItems" runat="server" Width="90%" SelectionMode="Multiple" Height="353px"></asp:ListBox>
                   <%-- <asp:CustomValidator ID="CustomValidator1"  runat="server" CssClass="ErrMsg" ControlToValidate="lstSelectedSimilarItems" ClientValidationFunction = "ValidateListBox" ErrorMessage="Please select only 4 Items"></asp:CustomValidator>--%>
                   
    
                </div>
                <div style="width: 9%;margin:14% 0 0 0%; float: left;">
                    <asp:Button ID="btnRemoveSimilarPrd" runat="server" CssClass="lotbtn" Text="Remove Similar Product" OnClick="btnRemoveSimilarPrd_Click" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div style="margin: 0 0 0 44.5%; float: left;">

        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="lotbtn" CausesValidation="true" OnClick="btnSave_Click" />
    </div>

    <br />
    <br />
    <br />
    <br />
</asp:Content>

