﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        txtUserName.Focus();
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (txtUserName.Text != null && txtPassword.Text != null)
            {
                //Session["LotAdminUser"] = txtUserName.Text;
                //Response.Redirect("Home.aspx");

                LotDBEntity.Models.SuperAdmin superAdmin = Apps.lotContext.SuperAdmins.Where(c => c.UserName == txtUserName.Text.Trim() && c.Password == txtPassword.Text.Trim()).FirstOrDefault();
                if (superAdmin != null)
                {
                    Session["LotAdminUser"] = txtUserName.Text;
                    Response.Redirect("Home.aspx");
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Please Enter Valid UserName And Password');", true);
                }
            }
        }
        catch (Exception ex)
        {
            
        }
        

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        txtUserName.Text = "";
        txtPassword.Text = "";

    }
}
