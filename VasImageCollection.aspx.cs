﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class VasImageCollection : System.Web.UI.Page
{
    string sGuid;
    object lockTarget = new object();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LotAdminUser"] == null)
        {
            Response.Redirect("Default.aspx");
        }
        else
        {
            if (!IsPostBack)
            {
                sGuid = Guid.NewGuid().ToString();
                hfGUID.Value = sGuid;
                GetImages();
            }
        }
    }

    private void GetImages()
    {
        lock (lockTarget)
        {
            if (Apps.lotContext.VASImgCollection.ToList().Count > 0)
            {
                DlstItemGallery.DataSource = Apps.lotContext.VASImgCollection.ToList();
                DlstItemGallery.DataBind();
            }
        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {

    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            LogHelper.Logger.Info("Started Saving in VasImg Collection");
            LotDBEntity.Models.VASImageCollection VasImgCollection = new LotDBEntity.Models.VASImageCollection();
            if (fpoffer.HasFiles)
            {
                lock (lockTarget)
                {
                    List<LotDBEntity.Models.VASImageCollection> VasImgCollect = Apps.lotContext.VASImgCollection.ToList();
                    foreach (var ritem in VasImgCollect)
                    {
                        Apps.lotContext.VASImgCollection.Remove(ritem);
                        Apps.lotContext.SaveChanges();
                    }
                }
                lock (lockTarget)
                {
                    int j = 1;
                    long VasImgCnt = 0;
                    if (Apps.lotContext.VASImgCollection.ToList().Count > 0)
                        VasImgCnt = Apps.lotContext.VASImgCollection.Max(c => c.ID);
                    if (fpoffer.PostedFiles.Count < 5)
                    {
                        foreach (HttpPostedFile Img in fpoffer.PostedFiles)
                        {
                            string Imagesfolder1 = Server.MapPath("~/OfferImage/");
                            string strImagesFolder = "OfferImage";
                            if (VasImgCnt > 0)
                            {
                                VasImgCnt = VasImgCnt + 1;
                            }
                            else
                            {
                                VasImgCnt = 1;
                            }
                            string strRandomName = j + Path.GetFileName(Img.FileName);
                            VasImgCollection.ID = VasImgCnt;
                            VasImgCollection.ImageName = hfGUID.Value + "~" + strRandomName;
                            VasImgCollection.ImageLocation = "pack://siteoforigin:,,,/" + strImagesFolder + "/" + hfGUID.Value + "~" + strRandomName;
                            VasImgCollection.WebImageLocation = Imagesfolder1 + hfGUID.Value + "~" + strRandomName;
                            VasImgCollection.LastUpdatedTime = DateTime.Now;
                            Img.SaveAs((Path.Combine(Imagesfolder1, hfGUID.Value + "~" + strRandomName)));
                            j++;
                            Apps.lotContext.VASImgCollection.Add(VasImgCollection);
                            Apps.lotContext.SaveChanges();
                            GetImages();
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Please Select lessthan 5 Images ');", true);
                    }
                    LogHelper.Logger.Info("Ended Saving in VasImg Collection");
                }
            }
        }
        catch (Exception ex)
        {
            LogHelper.Logger.ErrorException("Exception: Saving  : VasImageCollection: " + ex.Message, ex);
        }
        
    }
}