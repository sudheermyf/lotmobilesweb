﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AddModel : System.Web.UI.Page
{
    LotDBEntity.Models.Brands brand;
    LotDBEntity.Models.Models model;
    LotDBEntity.Models.Category Categ;
    object lockTarget = new object();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LotAdminUser"] == null)
        {
            Response.Redirect("Default.aspx");
        }
        else
        {
            if (!IsPostBack)
            {
                LogHelper.Logger.Info("Starts PageLoad in AddModel");
                lock (lockTarget)
                {
                    ddlCategory.Items.Add("Select");
                    var brands = Apps.lotContext.Brands.ToList();
                    ddlBrand.DataTextField = "Name";
                    ddlBrand.DataValueField = "SNo";
                    ddlBrand.DataSource = brands;
                    ddlBrand.DataBind();
                }
                LogHelper.Logger.Info("Starts PageLoad in AddModel");
            }
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtmodelname.Text) && txtexist.Text != "Already Exists" && ddlCategory.SelectedItem.Text != "Select" && ddlBrand.SelectedItem.Text != "Select")
        {
            try
            {
                lock (lockTarget)
                {
                    string strName = txtmodelname.Text.ToUpper().Trim();
                    long val1 = Convert.ToInt64(ddlBrand.SelectedValue);
                    lock (lockTarget)
                    {
                        brand = Apps.lotContext.Brands.Where(c => c.SNo == val1).FirstOrDefault();
                    }
                    long val = Convert.ToInt64(ddlCategory.SelectedValue);
                    lock (lockTarget)
                    {
                        Categ = Apps.lotContext.Categories.Where(c => c.SNo == val).FirstOrDefault();
                        if (model != null && Categ.LstModels != null)
                        {
                            lock (lockTarget)
                            {
                                model = Categ.LstModels.Where(c => c.Name == strName).FirstOrDefault();
                            }
                        }
                    }
                    long id = 0;
                    if (model == null)
                        model = new LotDBEntity.Models.Models();
                    lock (lockTarget)
                    {
                        if (Apps.lotContext.Models.ToList().Count > 0)
                            id = Apps.lotContext.Models.Max(c => c.ID);
                        if (id > 0)
                        {
                            model.ID = id + 1;
                        }
                        else
                        {
                            model.ID = 1;
                        }
                    }
                    model.Brand = brand;
                    model.Category = Categ;
                    model.Name = txtmodelname.Text.ToUpper().Trim();
                    model.Description = txtDesc.Text;
                    model.LastUpdatedTime = DateTime.Now;
                    Apps.lotContext.Models.Add(model);
                    Apps.lotContext.SaveChanges();
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Model saved successfully ');", true);
                ClearAll();
                btnSubmit.Enabled = true;
            }
            catch (Exception ex)
            {
                
                throw;
            }
            
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "name", "SuccessMsg();", true);
        }
       else
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Please enter required fields')", true);
        }
    }

    private void ClearAll()
    {
        txtmodelname.Text = "";
        txtDesc.Text = "";
        txtexist.Text = "";
        errBrand.Text = "";
        errCateg.Text = "";
        ErrModelName.Text = "";
        ddlBrand.ClearSelection();
        ddlBrand.Items.FindByText("Select").Selected = true;
        ddlCategory.Items.Clear();
        ddlCategory.Items.Add("Select");
    }
    
    protected void ddlBrand_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlBrand.SelectedItem.Text != "Select")
        {
            ddlCategory.Items.Clear();
           
            long val = Convert.ToInt64(ddlBrand.SelectedValue);
            brand = new LotDBEntity.Models.Brands();
            brand = Apps.lotContext.Brands.Where(c => c.SNo == val).FirstOrDefault();
            if (brand.LstCategory==null)
            {
                ddlCategory.Items.Clear();
                ddlCategory.Items.Add("Select");
              
                txtDesc.Text = "";
                txtmodelname.Text = "";
                txtexist.Text = "";
            } 
            else
            {
                ddlCategory.Items.Add("Select");
                var category = brand.LstCategory;
                ddlCategory.DataTextField = "Name";
                ddlCategory.DataValueField = "SNo";
                ddlCategory.DataSource = category;
                ddlCategory.DataBind();
                txtDesc.Text = "";
                txtmodelname.Text = "";
            }
        }
        else
        {
            ddlCategory.Items.Clear();
            ddlCategory.Items.Add("Select");
            ClearAll();
        }
    }

    protected void txtItemName_TextChanged(object sender, EventArgs e)
    {
        string strName = txtmodelname.Text.ToUpper().Trim();
        if (ddlCategory.SelectedValue != "Select" && ddlCategory.SelectedValue != "" && !string.IsNullOrEmpty(strName))
        {
            long val = Convert.ToInt64(ddlCategory.SelectedValue);
            Categ = new LotDBEntity.Models.Category();
            Categ = Apps.lotContext.Categories.Where(c => c.SNo == val).FirstOrDefault();
            if (Categ != null)
            {
                if (Categ.LstModels != null)
                {
                    LogHelper.Logger.Info("Model Name:" + strName);
                    model = Categ.LstModels.Where(c => c.Name == strName).FirstOrDefault();
                }
                else
                {
                    model = null;
                }
            }
            if (model != null)
            {
                txtexist.Text = "Already Exists";
                txtexist.Focus();
            }
            else
            {
                model = new LotDBEntity.Models.Models();
                txtexist.Text = "";
            }
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Please enter required fields')", true);
        }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        ClearAll();
        ddlBrand.ClearSelection();
        ddlBrand.Items.FindByText("Select").Selected = true;
        ddlCategory.Items.Clear();
        ddlCategory.Items.Add("Select");
    }
}