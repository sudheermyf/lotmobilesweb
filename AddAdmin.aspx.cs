﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AddAdmin : System.Web.UI.Page
{
    object lockTarget = new object();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LotAdminUser"] == null)
        {
            Response.Redirect("Default.aspx");
        }
       
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        lock (lockTarget)
        {
            Apps.lotContext.SuperAdmins.Add(new LotDBEntity.Models.SuperAdmin { UserName = txtUserName.Text.Trim(), Password = txtPassword.Text.Trim() });
            Apps.lotContext.SaveChanges();
        }
    }
}