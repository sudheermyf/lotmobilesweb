﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ViewModels : System.Web.UI.Page
{
    LotDBEntity.Models.Brands brand;
    public LotDBEntity.Models.Models model;
    LotDBEntity.Models.Category Categ;
    object lockTarget = new object();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LotAdminUser"] == null)
        {
            Response.Redirect("Default.aspx");
        }
        else
        {
            if (!IsPostBack)
            {
                LogHelper.Logger.Info("Starts PageLoad in ViewModels");
                lock (lockTarget)
                {
                    ddlCategory.Items.Add("Select");
                    var brands = Apps.lotContext.Brands.ToList();
                    ddlBrand.DataTextField = "Name";
                    ddlBrand.DataValueField = "SNo";
                    ddlBrand.DataSource = brands;
                    ddlBrand.DataBind();
                }
                LogHelper.Logger.Info("Ends PageLoad in ViewModels");
            }
        }
    }
    protected void ddlBrand_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlBrand.SelectedItem.Text != "Select")
            {
            ddlCategory.Items.Clear();
            lock (lockTarget)
            {
                long val = Convert.ToInt64(ddlBrand.SelectedValue);
                brand = new LotDBEntity.Models.Brands();
                brand = Apps.lotContext.Brands.Where(c => c.SNo == val).FirstOrDefault();
                if (brand.LstCategory == null)
                {
                    ddlCategory.Items.Clear();
                    ddlCategory.Items.Add("Select");
                    txtname.Text = "";
                    txtexist.Text = "";
                    txtdesc.Text = "";

                }
                else
                {
                    ddlCategory.Items.Add("Select");
                    var category = brand.LstCategory;
                    ddlCategory.DataTextField = "Name";
                    ddlCategory.DataValueField = "SNo";
                    ddlCategory.DataSource = category;
                    ddlCategory.DataBind();
                    lstmodels.Items.Clear();
                    txtname.Text = "";
                    txtexist.Text = "";
                    txtdesc.Text = "";
                }
            }
        }
        else
        {
            ddlCategory.Items.Clear();
            ddlCategory.Items.Add("Select");
            txtname.Text = "";
            txtexist.Text = "";
            txtdesc.Text = "";
            lstmodels.Items.Clear();
            ClearAll();
        }
    }
    protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlCategory.SelectedItem.Text != "Select")
        {
            lock (lockTarget)
            {
                long val = Convert.ToInt64(ddlCategory.SelectedValue);
                Categ = new LotDBEntity.Models.Category();
                Categ = Apps.lotContext.Categories.Where(c => c.SNo == val).FirstOrDefault();
                var model = Categ.LstModels;
                if (model != null)
                {
                    lstmodels.DataSource = model;
                    lstmodels.DataTextField = "Name";
                    lstmodels.DataValueField = "SNo";
                    lstmodels.DataBind();
                    txtname.Text = "";
                    txtdesc.Text = "";
                }
                else
                {
                    txtname.Text = "";
                    txtexist.Text = "";
                    txtdesc.Text = "";
                    lstmodels.Items.Clear();
                    ClearAll();
                }
            }
        }
        else
        {
            lstmodels.Items.Clear();
            txtname.Text = "";
            txtexist.Text = "";
            txtdesc.Text = "";
            //ClearAll();
        }
    }

    protected void txtname_TextChanged(object sender, EventArgs e)
    {
        CheckModelName();
    }

    private void CheckModelName()
    {
        string strName = txtname.Text.ToUpper();
        if (ddlCategory.SelectedItem.Text != "Select" && txtname.Text != null)
        {
            lock (lockTarget)
            {
                long val = Convert.ToInt64(ddlCategory.SelectedValue);
                Categ = new LotDBEntity.Models.Category();
                Categ = Apps.lotContext.Categories.Where(c => c.SNo == val).FirstOrDefault();

                model = new LotDBEntity.Models.Models();
                if (model != null && Categ.LstModels != null)
                {
                    lock (lockTarget)
                    {
                        model = Categ.LstModels.Where(c => c.Name == strName).FirstOrDefault();
                    }
                }
                if (model != null)
                {
                    txtexist.Text = "Already Exists";
                }
                else
                {
                    txtexist.Text = "";
                }
            }
        }
        else
        {
            //Response.Write("Please Enter Required Fields");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Please Enter Required Fields')", true);
        }
    }
    protected void lstmodels_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (lstmodels.SelectedItem.Text != "Select")
        {
            lock (lockTarget)
            {
                long val = Convert.ToInt64(lstmodels.SelectedValue);
                LotDBEntity.Models.Models SelectedModel = Apps.lotContext.Models.Where(c => c.SNo == val).FirstOrDefault();
                txtname.Text = SelectedModel.Name;
                txtdesc.Text = SelectedModel.Description;
                lblId.Text = SelectedModel.SNo.ToString();
                txtname.Focus();
            }
        }
        
    }
    protected void btnedit_Click(object sender, EventArgs e)
    {
        if (btnedit.Text == "Edit")
        {
            if (lstmodels.Items.Count>0)
            {
                txtname.ReadOnly = false;
                txtdesc.ReadOnly = false;
                lbladd.Text = "Edit Model";
                btnedit.Text = "Update";
                btndelete.Text = "Reset";
                ddlBrand.Enabled = false;
                ddlCategory.Enabled = false;
                lstmodels.Enabled = false;                
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "alert('Please Select One Item in the List...');", true);              
                
            }
        }
        else if (btnedit.Text == "Update")
        {
            if (txtexist.Text != "AlreadyExists")
            {
                lock (lockTarget)
                {
                    long val = Convert.ToInt64(lstmodels.SelectedValue);
                    LotDBEntity.Models.Models ModelUpdate = Apps.lotContext.Models.Where(c => c.SNo == val).FirstOrDefault();
                    if (ModelUpdate.Name != txtname.Text)
                    {
                        LogHelper.Logger.Info("starts checking modelname in ViewModels Page");
                        CheckModelName();
                        LogHelper.Logger.Info("Ends checking modelname in ViewModels Page");
                    }
                    if (txtexist.Text == "Already Exists")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Item name already exists');", true);
                    }
                    else
                    {
                        btnedit.Enabled = false;
                        ModelUpdate.Name = txtname.Text.ToUpper();
                        ModelUpdate.Description = txtdesc.Text.ToUpper();
                        ModelUpdate.LastUpdatedTime = DateTime.Now;
                        Apps.lotContext.SaveChanges();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "alert('Model Updated Successfully...');window.location='ViewModels.aspx';", true);
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "name", "UpdateMsg();", true);
                    }
                }
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Please Enter Required Fields')", true);
            }
        }
    }

    private void ClearAll()
    {
        txtdesc.Text = "";
        txtname.Text = "";
    }
    protected void btndelete_Click(object sender, EventArgs e)
    {
        if (btndelete.Text == "Delete")
        {
            if (lstmodels.Items.Count > 0)
            {
                lock (lockTarget)
                {
                    long val = Convert.ToInt64(lstmodels.SelectedValue);
                    model = Apps.lotContext.Models.Where(c => c.SNo == val).FirstOrDefault();
                    if (model.LstBasicItems != null && model.LstBasicItems.Count > 0)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Models Have different Items Please Delete Items Under the Selected Category');", true);
                    }
                    else
                    {
                        Apps.lotContext.Models.Remove(model);
                        Apps.lotContext.SaveChanges();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "alert('Model Deleted Successfully...');window.location='ViewModels.aspx';", true);
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "name", "ErrorMsg();", true);
                    }
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Please Select One Item in the List ');", true);
            }
        }
        else if (btndelete.Text == "Cancel")
        {
            Response.Redirect("ViewModels.aspx");
        }
        else if (btndelete.Text == "Reset")
        {
            lock (lockTarget)
            {
                long val = Convert.ToInt64(lblId.Text);
                LotDBEntity.Models.Models SelectedModel = Apps.lotContext.Models.Where(c => c.SNo == val).FirstOrDefault();
                txtname.Text = SelectedModel.Name;
                txtdesc.Text = SelectedModel.Description;
                txtexist.Text = "";

            }
        }

    }

    private void ReloadList()
    {
        if (lstmodels.SelectedItem.Text != "Select")
        {
            lock (lockTarget)
            {
                long val = Convert.ToInt64(lstmodels.SelectedValue);
                LotDBEntity.Models.Models SelectedModel = Apps.lotContext.Models.Where(c => c.SNo == val).FirstOrDefault();
                txtname.Text = SelectedModel.Name;
                txtdesc.Text = SelectedModel.Description;
                lblId.Text = SelectedModel.SNo.ToString();
                var model = Categ.LstModels;
                lstmodels.DataSource = model;
                lstmodels.DataTextField = "Name";
                lstmodels.DataValueField = "SNo";
                lstmodels.DataBind();
                txtname.Focus();
            }
        }
    }
}