﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ViewJukeBox : System.Web.UI.Page
{
    object lockTarget = new object();
    protected void Page_Load(object sender, EventArgs e)
    {
          if (Session["LotAdminUser"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            else
            {
                if (!IsPostBack)
                {
                    LogHelper.Logger.Info("Starts PageLoad in ViewModels");
                    lock (lockTarget)
                    {
                        //ddlfiletype.Items.Add("Select");
                        //ddlcategory.Items.Add("Select");
                        //ddlartist.Items.Add("Select");s
                        //ddlCateg4.Items.Add("Select");
                        //ddlCateg5.Items.Add("Select");
                           
                        InitiateLoad();

                    }
                    LogHelper.Logger.Info("Ends PageLoad in ViewModels");
                }
            }
    }

    private void InitiateLoad()
    {
        lock (lockTarget)
        {
            ddlfiletype.DataSource = Apps.lotContext.VASTabOne.ToList();
            ddlfiletype.DataBind();
        }
        lock (lockTarget)
        {
            ddlcategory.DataSource = Apps.lotContext.VASTabTwo.ToList();
            ddlcategory.DataBind();
        }
        lock (lockTarget)
        {
            ddlartist.DataSource = Apps.lotContext.VASTabThree.ToList();
            ddlartist.DataBind();
        }
        lock (lockTarget)
        {
            ddlCateg4.DataSource = Apps.lotContext.VASTabFour.ToList();
            ddlCateg4.DataBind();
        }
        lock (lockTarget)
        {
            ddlCateg5.DataSource = Apps.lotContext.VASTabFive.ToList();
            ddlCateg5.DataBind();
        }
        ddlfiletype.Enabled = false;
        ddlcategory.Enabled = false;
        ddlartist.Enabled = false;
        ddlCateg4.Enabled = false;
        ddlCateg5.Enabled = false;
    }
    protected void lstSongs_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetData();
    }

    private void GetData()
    {
        if (lstSongs.SelectedItem.Text != null)
        {
            long val = Convert.ToInt64(lstSongs.SelectedValue);
           
           
                LotDBEntity.Models.ValueAddedServicesData vascheck = Apps.lotContext.ValueAddedServicesData.Where(c => c.SNo == val).FirstOrDefault();

                    LotDBEntity.Models.VASTabOne vasOne = Apps.lotContext.VASTabOne.Where(c => c.SNo == vascheck.VASTabOne.SNo).FirstOrDefault();
                    if (vasOne != null)
                    {

                        ddlfiletype.ClearSelection();
                        ddlfiletype.Items.FindByText(vasOne.Name).Selected = true;
                    }
                    LotDBEntity.Models.VASTabTwo vas2 = Apps.lotContext.VASTabTwo.Where(c => c.SNo == vascheck.VASTabTwo.SNo).FirstOrDefault();
                    if (vas2 != null)
                    {

                        ddlcategory.ClearSelection();
                        ddlcategory.Items.FindByText(vas2.Name).Selected = true;
                    }
                    LotDBEntity.Models.VASTabThree vas3 = Apps.lotContext.VASTabThree.Where(c => c.SNo == vascheck.VASTabThree.SNo).FirstOrDefault();
                    if (vas3 != null)
                    {

                        ddlartist.ClearSelection();
                        ddlartist.Items.FindByText(vas3.Name).Selected = true;
                    }
                    LotDBEntity.Models.VASTabFour vas4 = Apps.lotContext.VASTabFour.Where(c => c.SNo == vascheck.VASTabFour.SNo).FirstOrDefault();
                    if (vas4 != null)
                    {

                        ddlCateg4.ClearSelection();
                        ddlCateg4.Items.FindByText(vas4.Name).Selected = true;
                    }
                    LotDBEntity.Models.VASTabFive vas5 = Apps.lotContext.VASTabFive.Where(c => c.SNo == vascheck.VASTabFive.SNo).FirstOrDefault();
                    if (vas5 != null)
                    {

                        ddlCateg5.ClearSelection();
                        ddlCateg5.Items.FindByText(vas5.Name).Selected = true;
                    }

        }
    }
    protected void btnedit_Click(object sender, EventArgs e)
    {
        if (btnedit.Text == "Edit")
        {
            if (lstSongs.Items.Count > 0)
            {
                btnedit.Text = "Update";
                btndelete.Text = "Reset";
                if (lstSongs.SelectedItem.Selected == true)
                {
                    lstSongs.Enabled = false;
                    ddlfiletype.Enabled = true;
                    ddlcategory.Enabled = true;
                    ddlartist.Enabled = true;
                    ddlCateg4.Enabled = true;
                    ddlCateg5.Enabled = true;
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Please Select atleast one item to Edit ');", true);
            }
        }
        else if (btnedit.Text == "Update")
        {
            lock (lockTarget)
            {
                long select = Convert.ToInt64(lstSongs.SelectedValue);
                LotDBEntity.Models.ValueAddedServicesData vasdata = Apps.lotContext.ValueAddedServicesData.Where(c => c.SNo == select).FirstOrDefault();
                LotDBEntity.Models.VASTabOne vastabone = null;
                LotDBEntity.Models.VASTabTwo vastabtwo = null;
                LotDBEntity.Models.VASTabThree vastabthree = null;
                LotDBEntity.Models.VASTabFour vastabfour = null;
                LotDBEntity.Models.VASTabFive vastabfive = null;
                if (!string.IsNullOrEmpty(ddlfiletype.SelectedItem.Text.Trim()))
                    vastabone = Apps.lotContext.VASTabOne.Where(c => c.Name.Equals(ddlfiletype.SelectedItem.Text)).FirstOrDefault();
                if (!string.IsNullOrEmpty(ddlcategory.SelectedItem.Text.Trim()))
                    vastabtwo = Apps.lotContext.VASTabTwo.Where(c => c.Name.Equals(ddlcategory.SelectedItem.Text)).FirstOrDefault();
                if (!string.IsNullOrEmpty(ddlartist.SelectedItem.Text.Trim()))
                    vastabthree = Apps.lotContext.VASTabThree.Where(c => c.Name.Equals(ddlartist.SelectedItem.Text)).FirstOrDefault();
                if (!string.IsNullOrEmpty(ddlCateg4.SelectedItem.Text.Trim()))
                    vastabfour = Apps.lotContext.VASTabFour.Where(c => c.Name.Equals(ddlCateg4.SelectedItem.Text)).FirstOrDefault();
                if (!string.IsNullOrEmpty(ddlCateg5.SelectedItem.Text.Trim()))
                    vastabfive = Apps.lotContext.VASTabFive.Where(c => c.Name.Equals(ddlCateg5.SelectedItem.Text)).FirstOrDefault();
                vasdata.VASTabOne = vastabone;
                vasdata.VASTabTwo = vastabtwo;
                vasdata.VASTabThree = vastabthree;
                vasdata.VASTabFour = vastabfour;
                vasdata.VASTabFive = vastabfive;
                Apps.lotContext.SaveChanges();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "alert('Juke Updated Successfully...');window.location='ViewJukeBox.aspx';", true);
            }
        }

    }
    protected void btndelete_Click(object sender, EventArgs e)
    {
        if (btndelete.Text == "Delete")
        {
            if (lstSongs.Items.Count > 0)
            {
                long select = Convert.ToInt64(lstSongs.SelectedValue);
                LotDBEntity.Models.ValueAddedServicesData vasdata = Apps.lotContext.ValueAddedServicesData.Where(c => c.SNo == select).FirstOrDefault();
                Apps.lotContext.ValueAddedServicesData.Remove(vasdata);
                Apps.lotContext.SaveChanges();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Record Deleted Successfully ');window.location='ViewJukeBox.aspx';", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Please Select atleast one item to Delete ');", true);
            }

                //InitiateLoad();
            
        }
        else if (btndelete.Text == "Reset") 
        {
            GetData();
        }
    }
    protected void ddlsearch_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddl1.Items.Clear();
        lstSongs.Items.Clear();
        if (ddlsearch.SelectedItem.Text == "Select")
        {
            ddl1.Items.Add("Select");
            ddl1.Items.Clear();
            lstSongs.Items.Clear();
        }
        if (ddlsearch.SelectedItem.Text == "FileType")
        {
            if (Apps.lotContext.VASTabOne.ToList().Count > 0)
            {
                ddl1.Items.Add("Select");
                ddl1.DataSource = Apps.lotContext.VASTabOne.ToList();
                ddl1.DataBind();
            }
            else
            {
                ddl1.Items.Add("Select");
            }
        }
        else if (ddlsearch.SelectedItem.Text == "Category")
        {
            if (Apps.lotContext.VASTabTwo.ToList().Count > 0)
            {
                ddl1.Items.Add("Select");
                ddl1.DataSource = Apps.lotContext.VASTabTwo.ToList();
                ddl1.DataBind();
            }
            else
            {
                ddl1.Items.Add("Select");
            }
        }
        else if (ddlsearch.SelectedItem.Text == "Artist")
        {
            if (Apps.lotContext.VASTabThree.ToList().Count > 0)
            {
                ddl1.Items.Add("Select");
                ddl1.DataSource = Apps.lotContext.VASTabThree.ToList();
                ddl1.DataBind();
            }
            else
            {
                ddl1.Items.Add("Select");
            }
        }
        else if (ddlsearch.SelectedItem.Text == "Music Director")
        {
            if (Apps.lotContext.VASTabFour.ToList().Count > 0)
            {
                ddl1.Items.Add("Select");
                ddl1.DataSource = Apps.lotContext.VASTabFour.ToList();
                ddl1.DataBind();
            }
            else
            {
                ddl1.Items.Add("Select");
            }
            
        }
        else if (ddlsearch.SelectedItem.Text == "Album")
        {
            if (Apps.lotContext.VASTabFive.ToList().Count > 0)
            {
                ddl1.Items.Add("Select");
                ddl1.DataSource = Apps.lotContext.VASTabFive.ToList();
                ddl1.DataBind();
            }
            else
            {
                ddl1.Items.Add("Select");
            }
        }
    }
    protected void ddl1_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddl1.SelectedItem.Text != "Select")
        {
            long selval = Convert.ToInt64(ddl1.SelectedValue);

            LotDBEntity.Models.VASTabOne vastabone = null;
            LotDBEntity.Models.VASTabTwo vastabtwo =null;
            LotDBEntity.Models.VASTabThree vastabthree =null;
            LotDBEntity.Models.VASTabFour vastabfour = null;
            LotDBEntity.Models.VASTabFive vastabfive = null;

            if (ddlsearch.SelectedItem.Text.Equals("FileType"))
                vastabone = Apps.lotContext.VASTabOne.Where(c => c.SNo == selval).FirstOrDefault();
            else if (ddlsearch.SelectedItem.Text.Equals("Category"))
                vastabtwo = Apps.lotContext.VASTabTwo.Where(c => c.SNo == selval).FirstOrDefault();
            else if (ddlsearch.SelectedItem.Text.Equals("Artist"))
                vastabthree = Apps.lotContext.VASTabThree.Where(c => c.SNo == selval).FirstOrDefault();
            else if (ddlsearch.SelectedItem.Text.Equals("Music Director"))
                vastabfour = Apps.lotContext.VASTabFour.Where(c => c.SNo == selval).FirstOrDefault();
            else if (ddlsearch.SelectedItem.Text.Equals("Album"))
                vastabfive = Apps.lotContext.VASTabFive.Where(c => c.SNo == selval).FirstOrDefault();

            if (vastabone != null)
            {
                lstSongs.DataSource = Apps.lotContext.ValueAddedServicesData.Where(c => c.VASTabOne.SNo == vastabone.SNo).ToList();
                lstSongs.DataTextField = "Name";
                lstSongs.DataValueField = "SNo";
                lstSongs.DataBind();
            }
            else if (vastabtwo != null)
            {
                lstSongs.DataSource = Apps.lotContext.ValueAddedServicesData.Where(c => c.VASTabTwo.SNo == vastabtwo.SNo).ToList();
                lstSongs.DataTextField = "Name";
                lstSongs.DataValueField = "SNo";
                lstSongs.DataBind();
            }
            else if (vastabthree != null)
            {
                lstSongs.DataSource = Apps.lotContext.ValueAddedServicesData.Where(c => c.VASTabThree.SNo == vastabthree.SNo).ToList();
                lstSongs.DataTextField = "Name";
                lstSongs.DataValueField = "SNo";
                lstSongs.DataBind();
            }
            else if (vastabfour != null)
            {
                lstSongs.DataSource = Apps.lotContext.ValueAddedServicesData.Where(c => c.VASTabFour.SNo == vastabfour.SNo).ToList();
                lstSongs.DataTextField = "Name";
                lstSongs.DataValueField = "SNo";
                lstSongs.DataBind();
            }
            else if (vastabfive != null)
            {
                lstSongs.DataSource = Apps.lotContext.ValueAddedServicesData.Where(c => c.VASTabFive.SNo == vastabfive.SNo).ToList();
                lstSongs.DataTextField = "Name";
                lstSongs.DataValueField = "SNo";
                lstSongs.DataBind();
            }
        }
        else 
        {
            lstSongs.Items.Add("Select");
            ddlfiletype.ClearSelection();
            ddlfiletype.Items.FindByText("Select").Selected = true;
            ddlcategory.ClearSelection();
            ddlcategory.Items.FindByText("Select").Selected = true;
            ddlartist.ClearSelection();
            ddlartist.Items.FindByText("Select").Selected = true;
            ddlCateg4.ClearSelection();
            ddlCateg4.Items.FindByText("Select").Selected = true;
            ddlCateg5.ClearSelection();
            ddlCateg5.Items.FindByText("Select").Selected = true;
        }
    }
}