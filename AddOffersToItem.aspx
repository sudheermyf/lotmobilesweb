﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMasterPage.master" AutoEventWireup="true" CodeFile="AddOffersToItem.aspx.cs" Inherits="AddOffersToItem" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .ErrMsg {
            color: #DD4B39;
            font-family: Arial;
            font-size: 10px;
        }
        .pgHeading {
            font-weight: 600;
            font-family: sans-serif;
            color: #92278f;
        }
    </style>

    <script type="text/javascript">
        function Validate() {
            var btnName = document.getElementById('<%=btnSubmit.ClientID %>');
            var ddlbrand = document.getElementById('<%=ddlbrand.ClientID %>'); 
            var Errddlbrand = document.getElementById('<%=Errddlbrand.ClientID %>');
            var ddlcateg = document.getElementById('<%=ddlcateg.ClientID %>');
            var Errddlcateg = document.getElementById('<%=Errddlcateg.ClientID %>');
            var ddlmodel = document.getElementById('<%=ddlmodel.ClientID %>');
            var Errddlmodel = document.getElementById('<%=Errddlmodel.ClientID %>'); 

            var lstItems = document.getElementById('<%=lstItems.ClientID %>');
            var ErrlstItems = document.getElementById('<%=ErrlstItems.ClientID %>');
            var lblId = document.getElementById('<%=lblId.ClientID %>');

            var ddlbrand1 = $(ddlbrand).find('option').filter(':selected').text();
            var ddlcateg1 = $(ddlcateg).find('option').filter(':selected').text();
            var ddlmodel1 = $(ddlmodel).find('option').filter(':selected').text();
            var lstItems1 = $(lstItems).find('option').filter(':selected').text();

            if (ddlbrand1 == "Select" || ddlbrand1 == "") {
                Errddlbrand.innerHTML = "Select Brand";
            }
            else {
                Errddlbrand.innerHTML = "";
            }

            if (ddlcateg1 == "Select" || ddlcateg1 == "") {
                Errddlcateg.innerHTML = "Select Category";
            }
            else {
                Errddlcateg.innerHTML = "";
            }

            if (ddlmodel1 == "Select" || ddlmodel1 == "") {
                Errddlmodel.innerHTML = "Select Model";
            }
            else {
                Errddlmodel.innerHTML = "";
            }

            if (lstItems.options.length == 0 || lstItems1 == "") {
                ErrlstItems.innerHTML = "Select an Item";
            }
            else {
                ErrlstItems.innerHTML = "";
            }
            if ( lstItems1 != "" && lstItems.options.length != 0 && ddlbrand1 != "Select" && ddlbrand1 != "" && ddlcateg1 != "Select" && ddlcateg1 != "" && ddlmodel1 != "Select" && ddlmodel1 != "") {
                return true;
            }
            return false;
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <style type="text/css">
         #footer {
             margin: -0% 0 0 0;
         }
         .tablecls span label {
             width: 56%;
         }
         form input[type="text"] {
             margin: 2% 0 0 0;
         }
         form label {
             /* display: block; */
             float: left;
             text-align: center;
         }
         form input[type="radio"] {
             float: left;
         }
         .radlabel {
             font-size: 20px;
             float: left;
             padding: 0 0 4% 0;
         }
         .droplabel {
             color: #690996;
             font-size: 20px;
         }
         .auto-style1 {
             height: 56px;
         }
    </style>
      <div style="margin: 5% 0 0 0; text-align: left; padding: 2% 0 0% 11.5%;">
        <h2 style="font-size: 28px;">
            <asp:Label ID="lbladd" runat="server" Text="Add Offers To Item" class="pgHeading" /></h2>
    </div>
     
    <div>
        <asp:UpdatePanel ID="upItem" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <div style="margin: 2% 0 3% 0;">
                            <asp:Label ID="lblId" runat="server" Visible="false" Text=""/>
                    <ul class="select_main" style="padding:0 0 2% 10.5%;">
                        <li>
                            <asp:Label ID="lblselectbrand" runat="server" Text="Select Brand" CssClass="droplabel"/>
                            <asp:Label ID="Errddlbrand" runat="server" class="ErrMsg" />
                            <asp:DropDownList ID="ddlbrand" CssClass="logroll1" onblur="javascript: return Validate()" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlbrand_SelectedIndexChanged">
                                <asp:ListItem Value="-1">Select</asp:ListItem>
                            </asp:DropDownList>
                        </li>
                        <li>
                            <asp:Label ID="lblselectcateg" runat="server" Text="Select Category" CssClass="droplabel"/>
                            <asp:Label ID="Errddlcateg" runat="server" class="ErrMsg" />
                            <asp:DropDownList ID="ddlcateg" CssClass="logroll1"  onblur="javascript: return Validate()" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlcateg_SelectedIndexChanged">
                                <asp:ListItem Value="-1">Select</asp:ListItem>
                            </asp:DropDownList>
                        </li>
                        <li>
                            <asp:Label ID="lblselectmodel" runat="server" Text="Select Model" CssClass="droplabel">
                            <asp:Label ID="Errddlmodel" runat="server" class="ErrMsg"/>
                            </asp:Label><asp:DropDownList ID="ddlmodel" CssClass="logroll1" onblur="javascript: return Validate()" runat="server" OnSelectedIndexChanged="ddlmodel_SelectedIndexChanged" AppendDataBoundItems="true" AutoPostBack="true">
                                <asp:ListItem Value="-1">Select</asp:ListItem></asp:DropDownList></li></ul></div><div style="margin: 0% 0 4% 0; float: left;width:30%;">
                    <ul class="select_main" style="padding: 0 0 3% 35%;">
                        <li style="width: 100%;">
                            <asp:Label ID="Label7" runat="server" Text="Select Item"/>
                            <asp:Label ID="ErrlstItems" runat="server" class="ErrMsg" />
                            <asp:ListBox ID="lstItems" onblur="javascript: return Validate()" runat="server" Width="255" Height="300" AutoPostBack="true" OnSelectedIndexChanged="lstItems_SelectedIndexChanged"></asp:ListBox>
                        </li>
                    </ul>
                </div>

                <table class="tablecls" style="float: left; width: 35%; margin: 0 0 0 8.5%;">
                    <tr>
                        
                        <td>
                            <asp:Label ID="lblimp" runat="server" ForeColor="Red" Text="*" Font-Size="Medium"></asp:Label><asp:CheckBox ID="chkFinancing" runat="server" Text="Financing Option" Style="float: left;text-align:left; margin: 0 3% 0 0;width:100%;" />
                        </td>
                        <td>
                            <asp:TextBox ID="txtfinancing" runat="server" onblur="javascript: return Validate()" Width="100%" Style="margin: 3% 0 0 0;" AutoPostBack="true"></asp:TextBox></td></tr><tr>
                    
                        <td>
                           <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="*" Font-Size="Medium"></asp:Label><asp:CheckBox ID="chlexchange" runat="server" Text="Today's Offer" Style="float: left; margin: 0 9% 0 0;width:100%;text-align:left;" />
                        </td>
                        <td>
                            <asp:TextBox ID="txtexchange" runat="server" Width="100%" Style="margin: 3% 0 0 0;" AutoPostBack="true"></asp:TextBox></td></tr><tr>
                        
                        <td>
                            <asp:Label ID="Label2" runat="server" ForeColor="Red" Text="*" Font-Size="Medium"></asp:Label><asp:CheckBox ID="chkOffers" runat="server" Text="Offers Available" Style="float: left; margin: 0 4% 0 0;width:100%;text-align:left;" />
                        </td>
                        <td>
                            <asp:TextBox ID="txtoffers" runat="server" Width="100%" Style="margin: 3% 0 0 0;" AutoPostBack="true"></asp:TextBox></td></tr><tr>
                       
                        <td>
                            <asp:Label ID="Label3" runat="server" ForeColor="Red" Text="*" Font-Size="Medium"></asp:Label><asp:CheckBox ID="chkEmi" runat="server" Text="EMI" Style="float: left; margin: 0 14% 0 0;width:100%;text-align:left;" />
                        </td>
                        <td>
                            <asp:TextBox ID="txtemi" runat="server" Width="100%" Style="margin: 3% 0 0 0;" AutoPostBack="true"></asp:TextBox></td></tr><tr>
                       
                        <td>
                            <asp:Label ID="Label4" runat="server" ForeColor="Red" Text="*" Font-Size="Medium"></asp:Label><asp:CheckBox ID="chkBuy1Get1" runat="server" Text="Buy1 Get1" Style="float: left; margin: 0 14% 0 0;width:100%;text-align:left;" />
                        </td>
                        <td>
                            <asp:TextBox ID="txtBuy1Get1" runat="server" Width="100%" Style="margin: 3% 0 0 0;" AutoPostBack="true"></asp:TextBox></td></tr><tr>
                       
                        <td>
                            <asp:Label ID="Label5" runat="server" ForeColor="Red" Text="*" Font-Size="Medium"></asp:Label><asp:CheckBox ID="chkBuy1Get2" runat="server" Text="Buy1 Get2" Style="float: left; margin: 0 14% 0 0;width:100%;text-align:left;" />
                        </td>
                        <td>
                            <asp:TextBox ID="txtBuy1Get2" runat="server" Width="100%" Style="margin: 3% 0 0 0;" AutoPostBack="true"></asp:TextBox></td></tr><tr>
                       
                        <td>
                            <asp:Label ID="Label6" runat="server" ForeColor="Red" Text="*" Font-Size="Medium"></asp:Label><asp:CheckBox ID="chkFlat50" runat="server" Text="Flat 50%" Style="float: left; margin: 0 14% 0 0;width:100%;text-align:left;" />
                        </td>
                        <td>
                            <asp:TextBox ID="txtFlat50" runat="server" Width="100%" Style="margin: 3% 0 0 0;" AutoPostBack="true"></asp:TextBox></td></tr><tr>
                       
                        <td>
                            <asp:Label ID="Label8" runat="server" ForeColor="Red" Text="*" Font-Size="Medium"></asp:Label><asp:CheckBox ID="chkPowerBank" runat="server" Text="Power Bank" Style="float: left; margin: 0 14% 0 0;width:100%;text-align:left;" />
                        </td>
                        <td>
                            <asp:TextBox ID="txtPowerBank" runat="server" Width="100%" Style="margin: 3% 0 0 0;" AutoPostBack="true"></asp:TextBox></td></tr><tr>
                       
                        <td>
                            <asp:Label ID="Label9" runat="server" ForeColor="Red" Text="*" Font-Size="Medium"></asp:Label><asp:CheckBox ID="chkTrail" runat="server" Text="Trail Period" Style="float: left; margin: 0 14% 0 0;width:100%;text-align:left;" />
                        </td>
                        <td>
                            <asp:TextBox ID="txtTrail" runat="server" Width="100%" Style="margin: 3% 0 0 0;" AutoPostBack="true"></asp:TextBox></td></tr><tr>
                        <td><asp:Label ID="lblerr" runat="server" ForeColor="Red"  Font-Size="Medium" Text="*
                            
                            
                            Only 4 Offers will be displayed"></asp:Label></td></tr></table></ContentTemplate></asp:UpdatePanel><table>
            
                <tr>
                <td></td>
                <td style="padding: 1% 0 0 57%;">
                    <asp:Button ID="btnSubmit" Text="Submit" OnClientClick="javascript: return Validate()" runat="server" CssClass="lotbtn" OnClick="btnSubmit_Click" />
                    <asp:Button ID="btnCancel" runat="server" Text="Reset" CssClass="lotbtn" OnClick="btnCancel_Click" /></td>
            </tr>
        </table>

         
    </div>
</asp:Content>
