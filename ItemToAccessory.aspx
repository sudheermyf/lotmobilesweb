﻿<%@ Page Title="ItemToAccessory" Language="C#" MasterPageFile="~/AdminMasterPage.master" AutoEventWireup="true" CodeFile="ItemToAccessory.aspx.cs" Inherits="ItemToAccessory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .ErrMsg {
            color: #DD4B39;
            font-family: Arial;
            font-size: 10px;
        }
        .pgHeading {
            font-weight: 600;
            font-family: sans-serif;
            color: #92278f;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server"> 
    <div style="margin: 5% 0 0 0; text-align: left; padding: 2% 0 0% 11.5%;">
        <h2 style="font-size: 28px;">
            <asp:Label ID="lbladd" runat="server" Text="Item To Accessory" class="pgHeading" /></h2>
    </div>
   
        <asp:UpdatePanel ID="upaccess" runat="server" UpdateMode="Always">
            <ContentTemplate>
    <div style="margin:2% 0 0 9.8%;">
        
        <asp:Label ID="Label1" runat="server" Text="Brand"></asp:Label>

        <asp:DropDownList ID="ddlbrands" runat="server" Width="42%" AppendDataBoundItems="true" CssClass="ddl" AutoPostBack="true" OnSelectedIndexChanged="ddlbrands_SelectedIndexChanged">
         <asp:ListItem Value="-1">Select</asp:ListItem>
                                        </asp:DropDownList></div>
    <div style="margin:2% 0 0 10%;float:left;width:38%;">
    
        <table>
            <tr>
                <td style="width: 50%;">
                    <asp:Label ID="Label2" runat="server" Text="Item"></asp:Label><br />
                    <asp:ListBox ID="lstAvalilableItems" runat="server" Width="98%" Height="353px" AutoPostBack="true" OnSelectedIndexChanged="lstAvalilableItems_SelectedIndexChanged"></asp:ListBox>
                    <br />
                </td>
                <td style="width: 50%;">

                    <asp:Label ID="Label3" runat="server" Text="Accessories"></asp:Label>
                    <asp:ListBox ID="lstAvalableAccessories" runat="server" Width="98%" SelectionMode="Multiple" AutoPostBack="true" Height="353px"></asp:ListBox>

                    <asp:Label ID="lblerror1" runat="server" Text="Please Select 4 Accessories Only" CssClass="ErrMsg"></asp:Label>
                </td>
            </tr>
        </table>
              
    </div>
                 <div style="margin:2% 0 0 0%;float:left;width:10%;padding:13% 0 0 2%">
                    <asp:Button ID="btnSelect" runat="server"  Text="Add >" Width="126px" CssClass="lotbtn" OnClick="btnSelect_Click" /><br />
                    <asp:Button ID="btnUnselect" runat="server"  Text="< Remove" Width="126px" style="margin-top:2px;margin-bottom:18px" OnClick="btnUnselect_Click" CssClass="lotbtn"/><br />
                 
                </div>
                <div style="margin:4% 0 0 0;float:left;width:42%;">
           
                             <asp:ListBox ID="lstSelectedItems" runat="server"  SelectionMode="Multiple" Width="80%"  Height="353px" AutoPostBack="true" ></asp:ListBox>
                   
                  <asp:Label ID="lblerror" runat="server"  CssClass="ErrMsg"></asp:Label>
    </div>
       </ContentTemplate>
                 </asp:UpdatePanel> 
    <br /><br />
</asp:Content>

