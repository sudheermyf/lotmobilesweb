﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ViewAccessory : System.Web.UI.Page
{
    LotDBEntity.Models.Brands brand;
    //LotDBEntity.Models.Models model;
    LotDBEntity.Models.Category Categ;
    LotDBEntity.Models.BasicAccessories Accessory;
    object lockTarget = new object();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LotAdminUser"] == null)
        {
            Response.Redirect("Default.aspx");
        }
        else
        {
            if (!IsPostBack)
            {
                LogHelper.Logger.Info("Starts When PageLoad in ViewAccessory");
                lock (lockTarget)
                {
                    ddlCategory.Items.Add("Select");
                    var brands = Apps.lotContext.Brands.ToList();
                    ddlBrand.DataTextField = "Name";
                    ddlBrand.DataValueField = "SNo";
                    ddlBrand.DataSource = brands;
                    ddlBrand.DataBind();
                }
                LogHelper.Logger.Info("Starts When PageLoad in ViewAccessory");
            }
        }
    }
    protected void ddlBrand_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlBrand.SelectedItem.Text != "Select")
        {
            lock (lockTarget)
            {
                ddlCategory.Items.Clear();
                long val = Convert.ToInt64(ddlBrand.SelectedValue);
                brand = new LotDBEntity.Models.Brands();
                brand = Apps.lotContext.Brands.Where(c => c.SNo == val).FirstOrDefault();
                if (brand.LstCategory.Count == 0 && brand.LstCategory == null)
                {
                    ddlCategory.Items.Clear();
                    ddlCategory.Items.Add("Select");
                }
                else
                {
                    ddlCategory.Items.Add("Select");
                    var category = brand.LstCategory;
                    ddlCategory.DataTextField = "Name";
                    ddlCategory.DataValueField = "SNo";
                    ddlCategory.DataSource = category;
                    ddlCategory.DataBind();
                }
            }
        }
        else
        {
            ddlCategory.Items.Clear();
            ddlCategory.Items.Add("Select");
            ClearAll();
        }
    }
    protected void txtname_TextChanged(object sender, EventArgs e)
    {
        string strName = txtname.Text.ToUpper().Trim();
        if (ddlCategory.SelectedItem.Text != "Select" && txtname.Text != null)
        {
            lock (lockTarget)
            {
                long val = Convert.ToInt64(ddlCategory.SelectedValue);
                Categ = new LotDBEntity.Models.Category();
                Categ = Apps.lotContext.Categories.Where(c => c.SNo == val).FirstOrDefault();

                Accessory = new LotDBEntity.Models.BasicAccessories();
                if (Accessory != null && Categ.LstAccessories.Count >= 0)
                {
                    lock (lockTarget)
                    {
                        Accessory = Categ.LstAccessories.Where(c => c.Name == strName).FirstOrDefault();
                    }
                }
                if (Accessory != null)
                {
                    txtexist.Text = "Already Exists";
                }
                else
                {
                    txtexist.Text = "";
                }
            }
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Please Enter Required Fields')", true);
        }
    }
    protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlCategory.SelectedItem.Text != "Select")
        {
            lock (lockTarget)
            {
                long val = Convert.ToInt64(ddlCategory.SelectedValue);
                Categ = new LotDBEntity.Models.Category();
                Categ = Apps.lotContext.Categories.Where(c => c.SNo == val).FirstOrDefault();
                var accessory = Categ.LstAccessories;
                lstaccessories.DataSource = accessory;
                lstaccessories.DataTextField = "Name";
                lstaccessories.DataValueField = "SNo";
                lstaccessories.DataBind();
            }
        }
        else
        {
            ClearAll();
        }
    }
    protected void lstaccessories_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (lstaccessories.SelectedItem.Text != "Select")
        {
            lock (lockTarget)
            {
                long val = Convert.ToInt64(lstaccessories.SelectedValue);
                LotDBEntity.Models.BasicAccessories SelectedAccessories = Apps.lotContext.BasicAccessories.Where(c => c.SNo == val).FirstOrDefault();
                lblId.Text = SelectedAccessories.SNo.ToString();
                txtname.Text = SelectedAccessories.Name;
                imgaccessorylogo.ImageUrl = "~/AccessoryImages/" + SelectedAccessories.ImageName;
                txtkey1.Text = SelectedAccessories.KeyFeature1;
                txtkey2.Text = SelectedAccessories.KeyFeature2;
                txtkey3.Text = SelectedAccessories.KeyFeature3;
                txtkey4.Text = SelectedAccessories.KeyFeature4;

                txtprice.Text = Convert.ToString(SelectedAccessories.Price);
                txtname.Focus();
            }
        }
    }
    protected void btnedit_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtname.Text) && !string.IsNullOrEmpty(ddlBrand.SelectedItem.Text) && !string.IsNullOrEmpty(ddlCategory.SelectedItem.Text) && !string.IsNullOrEmpty(lstaccessories.SelectedItem.Text))
        {
            Response.Redirect("AddAccessories.aspx?SNo=" + lblId.Text + "&Brand=" + ddlBrand.SelectedItem.Text + "&Categ=" + ddlCategory.SelectedItem.Text + "");
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Please Enter Manadatory Fields ');", true);
        }
    }

    private void ClearAll()
    {
        lstaccessories.DataSource = null;
        lstaccessories.Items.Clear();
        txtkey1.Text = "";
        txtkey2.Text = "";
        txtkey3.Text = "";
        txtkey4.Text = "";
        txtname.Text = "";
        txtprice.Text = "";
        imgaccessorylogo.ImageUrl = null;
    }

    protected void btndelete_Click(object sender, EventArgs e)
    {
        if (lstaccessories.Items.Count>0)
        {
            LotDBEntity.Models.BasicAccessories BAccess = Apps.lotContext.BasicAccessories.Where(c => c.Name == txtname.Text).FirstOrDefault();
            if (BAccess.LstBasicItems != null && BAccess.LstBasicItems.Count == 0)
            {
                lock (lockTarget)
                {
                    long val = Convert.ToInt64(lstaccessories.SelectedValue);
                    LotDBEntity.Models.BasicAccessories lstAcc = Apps.lotContext.BasicAccessories.FirstOrDefault();
                    LotDBEntity.Models.BasicAccessories accessory = Apps.lotContext.BasicAccessories.Where(c => c.SNo == val).FirstOrDefault();
                    if (accessory != null)
                    {
                        Apps.lotContext.BasicAccessories.Remove(accessory);
                        Apps.lotContext.SaveChanges();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Accessory Deleted Successfully ');window.location='ViewAccessory.aspx'", true);
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "name", "ErrorMsg();", true);                
                    }
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Please First Remove Linking In Item To Accessory ');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Please Select One Item in the List ');", true);
        }
    }

    private void ReloadList()
    {
        if (lstaccessories.SelectedItem.Text != "Select")
        {
            lock (lockTarget)
            {
                long val = Convert.ToInt64(ddlCategory.SelectedValue);
                Categ = new LotDBEntity.Models.Category();
                Categ = Apps.lotContext.Categories.Where(c => c.SNo == val).FirstOrDefault();
                var accessory = Categ.LstAccessories;
                lstaccessories.DataSource = accessory;
                lstaccessories.DataTextField = "Name";
                lstaccessories.DataValueField = "SNo";
                lstaccessories.DataBind();
            }
        }
    }
}