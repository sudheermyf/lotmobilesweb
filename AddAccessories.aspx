﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMasterPage.master" AutoEventWireup="true" CodeFile="AddAccessories.aspx.cs" Inherits="AddAccessories" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    
    <style type="text/css">
        .ErrMsg {
            color: #DD4B39;
            font-family: Arial;
            font-size: 10px;
        }
        .pgHeading {
            font-weight: 600;
            font-family: sans-serif;
            color: #92278f;
        }
    </style>
    
     <script type="text/javascript">
         function Validate() {
             var btnSubmit = document.getElementById('<%=btnSubmit.ClientID %>');
             var ddlBrand = document.getElementById('<%=ddlBrand.ClientID %>');
             var ErrddlBrand = document.getElementById('<%=ErrddlBrand.ClientID %>'); 
             var ddlCategory = document.getElementById('<%=ddlCategory.ClientID %>');
             var ErrddlCateg = document.getElementById('<%=ErrddlCateg.ClientID %>');
             var txtaccesname = document.getElementById('<%=txtaccesname.ClientID %>');
             var Errtxtaccesname = document.getElementById('<%=Errtxtaccesname.ClientID %>'); 
             var Errfplogo = document.getElementById('<%=Errfplogo.ClientID %>');
             var myLabel = document.getElementById('<%=myLabel.ClientID %>');
             var txtprice = document.getElementById('<%=txtprice.ClientID %>');
             var Errtxtprice = document.getElementById('<%=Errtxtprice.ClientID %>');
             var fplogo = document.getElementById('<%=fplogo.ClientID %>').value; 

             var ddlBrand1 = $(ddlBrand).find('option').filter(':selected').text();
             var ddlCategory1 = $(ddlCategory).find('option').filter(':selected').text();

             
             if (ddlBrand1 == "Select" || ddlBrand1 == "") {
                 ErrddlBrand.innerHTML = "Select Brand";               
             }
             else {
                 ErrddlBrand.innerHTML = "";
             }

             if (ddlCategory1 == "Select" || ddlCategory1 == "") {
                 ErrddlCateg.innerHTML = "Select Brand";                              
             }
             else {
                 ErrddlCateg.innerHTML = "";
             }

             if (txtaccesname.value.length == 0) {
                 Errtxtaccesname.innerHTML = "Enter Accessory Name";                
             }
             else {
                 Errtxtaccesname.innerHTML = "";
             }

             var uploadcontrol = document.getElementById('<%=fplogo.ClientID%>').value;
             //Regular Expression for fileupload control.
             var reg = /^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+(.jpeg|.JPEG|.gif|.GIF|.png|.PNG|.jpg|.JPG)$/;
             if (uploadcontrol.length > 0) {
                 //Checks with the control value.
                 if (reg.test(uploadcontrol)) {
                     Errfplogo.innerHTML = "";
                 }
                 else {
                     //If the condition not satisfied shows error message.
                     Errfplogo.innerHTML = "Only png, jpg and gif files are allowed!";
                 }
             }

             if (uploadcontrol.length == 0) {
                 Errfplogo.innerHTML = "";
             }

             if (btnSubmit.value == "Submit") {
                 if (fplogo.length == 0) {
                     Errfplogo.innerHTML = "Select Image";
                 }

                 if (txtprice.value <= 0) {
                     Errtxtprice.innerHTML = "Enter Valid Price";
                    
                 }
                 else {
                     Errtxtprice.innerHTML = "";
                 }

                 if (Errfplogo.innerHTML == "" && Errtxtprice.innerHTML == "" &&
                     Errtxtaccesname.innerHTML == "" && Errfplogo.innerHTML == "" && 
                     ErrddlBrand.innerHTML == "" &&
                     ErrddlCateg.innerHTML == "") {
                     return true;
                 }
                 return false;
             }

             if (btnSubmit.value == "Update") {
                 if (txtprice.value <= 0) {
                     Errtxtprice.innerHTML = "Enter Valid Price";
                 }
                 else {
                     Errtxtprice.innerHTML = "";
                 }

                 if (Errfplogo.innerHTML == "" && Errtxtprice.innerHTML == "" && Errtxtaccesname.innerHTML == "" &&
                     ErrddlBrand.innerHTML == "" &&
                     ErrddlCateg.innerHTML == "") {
                     return true;
                 }
                 return false;
             }
             return false;
         }       
     </script>
   
    <script type="text/javascript">
        function DisableCopyPaste(e) {
            // Message to display
            var isRight = (e.button) ? (e.button == 2) : (e.which == 3);
            var charCode = (e.which) ? e.which : e.keyCode;
            var Errtxtprice = document.getElementById('<%=Errtxtprice.ClientID %>');

            var message = "Cntrl key/ Right Click Option disabled";
            // check mouse right click or Ctrl key press
            var kCode = event.keyCode || e.charCode;
            //FF and Safari use e.charCode, while IE use e.keyCode
            if (kCode == 17 || kCode == 2 || isRight) {
                return false;
            }
        }
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            var Errtxtprice = document.getElementById('<%=Errtxtprice.ClientID %>');
            var message = "Only Numeric values allowed";
            if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
                Errtxtprice.innerHTML = "Only Numeric values allowed";
                return false;
            }
            Errtxtprice.innerHTML = "";
            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div style="margin: 5% 0 0 0; text-align: left; padding: 2% 0 0% 11.5%;">
        <h2 style="font-size: 28px;">
            <asp:Label ID="lbladd" runat="server" Text="Add Accessories" class="pgHeading" />
        </h2>
    </div>
    <div style="width: 100%; padding: 3% 0 0 23%; margin: -7% 0 0 3%;">

        <asp:Label ID="lblId" runat="server" Visible="false"></asp:Label>
        
        <asp:UpdatePanel ID="upItem" runat="server" UpdateMode="Always">
            <ContentTemplate>
       
        <table style="margin: 6% 0 4% 2.2%; width: 100%">
            <tr>
                <td style="width:15%;">
                    <asp:Label ID="Label2" runat="server" Text="Select Brand" /></td>
                <td>
                    <asp:UpdatePanel ID="upBrand" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                            <asp:DropDownList ID="ddlBrand" Width="45%" runat="server" onblur="javascript: return Validate()"  OnSelectedIndexChanged="ddlBrand_SelectedIndexChanged" AutoPostBack="true" AppendDataBoundItems="true" CssClass="ddl">
                                <asp:ListItem Value="-1">Select</asp:ListItem>
                            </asp:DropDownList>
                            <asp:Label ID="ErrddlBrand" runat="server" class="ErrMsg" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label3" runat="server" Text="Select Category" /></td>
                <td>
                    <asp:UpdatePanel ID="upCategAccess" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="ddlCategory" onblur="javascript: return Validate()" runat="server" Width="45%" CssClass="ddl"></asp:DropDownList>
                            <asp:Label ID="ErrddlCateg" runat="server" CssClass="ErrMsg" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblaccesname" runat="server" Text="Name" /></td>
                <td>

                    <asp:UpdatePanel ID="accname" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                            <asp:TextBox ID="txtaccesname" style="text-transform:uppercase" onblur="javascript: return Validate()" placeholder="Accessory Name" runat="server" AutoPostBack="true" OnTextChanged="txtaccesname_TextChanged" Width="45%"/>

                            <asp:Label ID="txtexist" runat="server" CssClass="ErrMsg" />
                            <asp:Label ID="Errtxtaccesname" runat="server" CssClass="ErrMsg" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblaccessimg" runat="server" Text="Image" /></td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                            <asp:FileUpload ID="fplogo" onblur="javascript: return Validate()" onchange="javascript: return Validate()" runat="server" Style="float: left" Width="45%" />
                            <asp:Label ID="myLabel" runat="server" CssClass="ErrMsg" />
                            <asp:Label ID="Errfplogo" runat="server" CssClass="ErrMsg" />

                            <asp:Label ID="lblImgLocation" runat="server" Font-Size="12px" ForeColor="#333" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblprice" runat="server" Text="Price" /></td>
                <td>
                    <asp:TextBox ID="txtprice" onblur="javascript: return Validate()" runat="server" placeholder="Price" Width="45%"  onkeypress="return isNumberKey(event)" onKeyDown="return DisableCopyPaste(event)" onMouseDown="return DisableCopyPaste (event)" MaxLength="6" Text="0" />
                    <asp:Label ID="Errtxtprice" runat="server" CssClass="ErrMsg" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblkey1" runat="server" Text="Key Feature1" /></td>
                <td>
                    <asp:TextBox ID="txtkey1" runat="server" Width="45%" /></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label1" runat="server" Text="Key Feature2" /></td>
                <td>
                    <asp:TextBox ID="txtkey2" runat="server" Width="45%" /></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label4" runat="server" Text="Key Feature3" /></td>
                <td>
                    <asp:TextBox ID="txtkey3" runat="server" Width="45%" /></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label5" runat="server" Text="Key Feature4" /></td>
                <td>
                    <asp:TextBox ID="txtkey4" runat="server" Width="45%" /></td>
            </tr>
        </table>
           </ContentTemplate>
            </asp:UpdatePanel>
        <table style="margin: 6% 0 0 0%;">
            <tr>
                <td></td>
                <td style="padding: 0 0 0 33.4%;">
                    <asp:Button ID="btnSubmit"  Text="Submit" OnClientClick="javascript: return Validate()" runat="server" OnClick="btnSubmit_Click" CssClass="lotbtn"/>
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="lotbtn" OnClick="btnCancel_Click" /></td>
            </tr>
        </table>
    </div>
</asp:Content>

