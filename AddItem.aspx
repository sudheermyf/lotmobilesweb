﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMasterPage.master" AutoEventWireup="true" CodeFile="AddItem.aspx.cs" Inherits="AddItem" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <style type="text/css">
        .ErrMsg {
            color: #DD4B39;
            font-family: Arial;
            font-size: 10px;
        }
        .pgHeading {
            font-weight: 600;
            font-family: sans-serif;
            color: #92278f;
        }
    </style>

      <style type="text/css">
           #ContentPlaceHolder1_DlstItemGallery {  width:auto;
        }
    form input[type="text"] {margin: 2% 0 0 0;
    }
    form label {
/* display: block; */

float: left;
text-align:center;
}
        form input[type="radio"] {
        
        float:left;
        }
        .radlabel {
            font-size: 20px;
        float:left;
        padding:0 0 4% 0;
        }
        .droplabel {color: #690996;
font-size: 20px;
           }
        .auto-style1 {
            height: 56px;
        }
     </style>
    
   <script type="text/javascript">    

       function Validate() {
           var ErrMsg = document.getElementById('<%=ErrMsg.ClientID %>');            
           var errtxtPrice = document.getElementById('<%=errtxtPrice.ClientID %>');
           var txtprice = document.getElementById('<%=txtprice.ClientID %>');
           var btnSubmit = document.getElementById('<%=btnSubmit.ClientID %>');
           var BrandName = document.getElementById('<%=ddlbrand.ClientID %>');
           var ErrBrandName = document.getElementById('<%=errBrand.ClientID %>');
           var CategName = document.getElementById('<%=ddlcateg.ClientID %>');
           var ErrCategeName = document.getElementById('<%=errCateg.ClientID %>');
           var ModelName = document.getElementById('<%=ddlmodel.ClientID %>');
           var ErrModelName = document.getElementById('<%=errModel.ClientID %>');
           var txtitemname = document.getElementById('<%=txtitemname.ClientID %>');
           var ErrItemExist = document.getElementById('<%=txtexist.ClientID %>');
           var ErrItemNull = document.getElementById('<%=ErrItemNull.ClientID %>'); 
           var fpimg = document.getElementById('<%=fpimg.ClientID %>').value; 
           var fperror = document.getElementById('<%=fperror.ClientID %>');
           var myLabel = document.getElementById('<%=myLabel.ClientID %>');
           var myLabel1 = document.getElementById('<%=myLabel1.ClientID %>');
           var myLabel2 = document.getElementById('<%=myLabel2.ClientID %>');

           var fpUploadVideo = document.getElementById('<%=fpUploadVideo.ClientID %>').value;
           var fpUploadImg = document.getElementById('<%=fpUploadImg.ClientID %>').value; 
           var ErrFUvideo = document.getElementById('<%=ErrFUvideo.ClientID %>');
           var ErrFUimages = document.getElementById('<%=ErrFUimages.ClientID %>');
           var fpOfferImage = document.getElementById('<%=fpOfferImage.ClientID %>').value; 
           var fpOffer1 = document.getElementById('<%=fpOffer1.ClientID %>');
           


           var BrandName1 = $(BrandName).find('option').filter(':selected').text();
           var CategName1 = $(CategName).find('option').filter(':selected').text();
           var ModelName1 = $(ModelName).find('option').filter(':selected').text();


           if (BrandName1 == "Select" || BrandName1 == "") {
               ErrBrandName.innerHTML = "Select Brand";
           }
           else {
               ErrBrandName.innerHTML = "";
           }

           if (CategName1 == "Select" || CategName1 == "") {
               ErrCategeName.innerHTML = "Select Category";
           }
           else {
               ErrCategeName.innerHTML = "";
           }

           if (ModelName1 == "Select" || ModelName1 == "") {
               ErrModelName.innerHTML = "Select Model";
           }
           else {
               ErrModelName.innerHTML = "";
           }

           if (ErrItemExist.textContent == "Already Exists") {
               ErrItemNull.innerHTML = "Already Exists";                        
           }
           else {
               ErrItemExist.innerHTML = "";
           }

           if (txtitemname.value.length == 0) {
               ErrItemNull.innerHTML = "Enter Item Name";
           }
           else {
               ErrItemNull.innerHTML = "";
           }

           if (fpUploadImg.length == 0) {
               myLabel.innerHTML = "";
           }

           if (fpUploadVideo.length == 0) {
               myLabel1.innerHTML = "";
           }


           var uploadcontrol4 = document.getElementById('<%=fpOfferImage.ClientID%>').value;
           //Regular Expression for fileupload control.
           var regOfferImgs = /^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+(.jpeg|.JPEG|.gif|.GIF|.png|.PNG|.jpg|.JPG)$/;
           if (uploadcontrol4.length > 0) {
               //Checks with the control value.
               if (regOfferImgs.test(uploadcontrol4)) {
                   fpOffer1.innerHTML = "";
               }
               else {
                   //If the condition not satisfied shows error message.
                   fpOffer1.innerHTML = "Only png, jpg and gif files are allowed!";
               }
           }

           var uploadcontrol3 = document.getElementById('<%=fpUploadVideo.ClientID%>').value;
           //Regular Expression for fileupload control.
           var regVdos = /^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+(.avi|.AVI|.mpeg|.MPEG|.mp4|.MP4)$/;
           if (uploadcontrol3.length > 0) {
               //Checks with the control value.
               if (regVdos.test(uploadcontrol3)) {
                   ErrFUvideo.innerHTML = "";
               }
               else {
                   //If the condition not satisfied shows error message.
                   ErrFUvideo.innerHTML = "Only avi, mpeg and mp4 files are allowed!";
               }
           }

           var uploadcontrol1 = document.getElementById('<%=fpimg.ClientID%>').value;
           //Regular Expression for fileupload control.
           var regItemImgs = /^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+(.jpeg|.JPEG|.gif|.GIF|.png|.PNG|.jpg|.JPG)$/;
           if (uploadcontrol1.length > 0) {
               //Checks with the control value.
               if (regItemImgs.test(uploadcontrol1)) {
                   fperror.innerHTML = "";
               }
               else {
                   //If the condition not satisfied shows error message.
                   fperror.innerHTML = "Only png, jpg and gif files are allowed!";
               }
           }

           var uploadcontrol2 = document.getElementById('<%=fpUploadImg.ClientID%>').value;
           //Regular Expression for fileupload control.
           var regCmpImgs = /^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+(.jpeg|.JPEG|.gif|.GIF|.png|.PNG|.jpg|.JPG)$/;      
           if (uploadcontrol2.length > 0) {
               //Checks with the control value.               
               if (regCmpImgs.test(uploadcontrol2)) {
                   ErrFUimages.innerHTML = "";
               }
               else {
                   //If the condition not satisfied shows error message.
                   ErrFUimages.innerHTML = "Only png, jpg and gif files are allowed!";
               }
           }

           if (uploadcontrol2.length == 0) {
               ErrFUimages.innerHTML = "";
           }
           if (uploadcontrol3.length == 0) {
               ErrFUvideo.innerHTML = "";
           }

           if (btnSubmit.value == "Submit") {
               if (fpimg.length == 0) {
                   fperror.innerHTML = "Please Select Images";
               }                        

               if (txtprice.value <= 0) {
                   errtxtPrice.innerHTML = "Enter Valid Price";
               }
               else {
                   errtxtPrice.innerHTML = "";
               }

               if (errtxtPrice.innerHTML == "" &&
                   ErrBrandName.innerHTML == "" &&
                   ErrCategeName.innerHTML == "" &&
                   ErrModelName.innerHTML == "" &&
                   fperror.innerHTML == "" && ErrFUimages.innerHTML == "" && ErrFUvideo.innerHTML == "" &&
                   ErrItemNull.innerHTML == "" && ErrItemExist.textContent != "Already Exists" && fpOffer1.innerHTML == "") {
                   ErrMsg.innerHTML = "";
                   return true;
               }
               else {
                   ErrMsg.innerHTML = "Enter Required Fields";
                   return false;
               }
           }
           if (btnSubmit.value == "Update") {

               if (txtprice.value <= 0) {
                   errtxtPrice.innerHTML = "Enter Valid Price";
               }
               else {
                   errtxtPrice.innerHTML = "";
               }

               if (ErrMsg.innerHTML != "Enter Required Fields" &&
                   errtxtPrice.innerHTML == "" &&
                   fperror.innerHTML == "" && ErrFUimages.innerHTML == "" && ErrFUvideo.innerHTML == "" &&
                   ErrBrandName.innerHTML == "" &&
                   ErrCategeName.innerHTML == "" &&
                   ErrModelName.innerHTML == "" && ErrItemNull.innerHTML == "" &&
                   ErrItemExist.textContent != "Already Exists" && fpOffer1.innerHTML == "") {

                   ErrMsg.innerHTML = "";
                   return true;
               }

               return false;
           }
           ErrMsg.innerHTML = "Please Enter Required Fields";
           return false;
       }
     </script>
 
    <script type="text/javascript">
        function DisableCopyPaste(e) {
            // Message to display
            var isRight = (e.button) ? (e.button == 2) : (e.which == 3);
            var charCode = (e.which) ? e.which : e.keyCode;
            var errtxtPrice = document.getElementById('<%=errtxtPrice.ClientID %>');
            var message = "Cntrl key/ Right Click Option disabled";
            // check mouse right click or Ctrl key press
            var kCode = event.keyCode || e.charCode;
            //FF and Safari use e.charCode, while IE use e.keyCode
            if (kCode == 17 || kCode == 2 || isRight) {
                alert(message);
                return false;
            }
        }
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            var errtxtPrice = document.getElementById('<%=errtxtPrice.ClientID %>');
            var message = "Only Numeric values allowed";
            if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
                errtxtPrice.innerHTML = "Only Numeric values allowed";
                return false;
            }
            errtxtPrice.innerHTML = "";
            return true;
        }
    </script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   <asp:Label ID="lblId" runat="server" />
     <div style="margin:5% 0 0 0;text-align:left;padding: 2% 0 0% 11.5%;"><h2 style="font-size:28px;">
         <asp:Label ID="lbladd" runat="server" Text="Add Item" class="pgHeading"/>
                                                                        </h2></div>
 
     <div style="margin: 0 0 8% 12%;" >
         <asp:HiddenField runat="server" ID="hfGUID" />
         <asp:UpdatePanel ID="upItem" runat="server" UpdateMode="Always">
             <ContentTemplate>
    <div style="margin: 0% 0 4% 0;">
        <ul class="select_main" style="padding: 0 0 3% 18%;">
            <li>
                <asp:Label ID="lblselectbrand" runat="server" Text="Select Brand" CssClass="droplabel">
                    
                </asp:Label>
                <asp:DropDownList ID="ddlbrand" CssClass="logroll1" runat="server" AppendDataBoundItems="true"  onblur="javascript:return Validate()" AutoPostBack="true" OnSelectedIndexChanged="ddlbrand_SelectedIndexChanged">
                  <asp:ListItem Value="-1">Select</asp:ListItem>
                </asp:DropDownList>
                <asp:Label ID="errBrand" runat="server" class="ErrMsg" ></asp:Label>
            </li>
            <li>
                <asp:Label ID="lblselectcateg" runat="server" Text="Select Category" CssClass="droplabel">

                </asp:Label>
                <asp:DropDownList ID="ddlcateg" CssClass="logroll1" runat="server" AppendDataBoundItems="true"  onblur="javascript:return Validate()" AutoPostBack="true" OnSelectedIndexChanged="ddlcateg_SelectedIndexChanged">
                    <asp:ListItem Value="-1">Select</asp:ListItem>
                </asp:DropDownList>
                 <asp:Label ID="errCateg" runat="server" class="ErrMsg" ></asp:Label>
            </li>
            <li>
                <asp:Label ID="lblselectmodel" runat="server" Text="Select Model" CssClass="droplabel">

                </asp:Label>
                <asp:DropDownList ID="ddlmodel" CssClass="logroll1" runat="server" AppendDataBoundItems="true"  onblur="javascript:return Validate()" AutoPostBack="true" OnSelectedIndexChanged="ddlmodel_SelectedIndexChanged">
                    <asp:ListItem Value="-1">Select</asp:ListItem>
                </asp:DropDownList>
                 <asp:Label ID="errModel" runat="server" class="ErrMsg" ></asp:Label>
            </li>
        </ul>
       
    </div>
                  </ContentTemplate>
             </asp:UpdatePanel>
         <table class="tablecls">
             <tr>
                 <td style="color: #7a1380; font-weight: bold; padding: 2% 0 0% 0; height: 48px; width: 13%;">Item Details</td>
                 <td style="width: 25%;"><span id="Span1">Name</span>
                     <asp:UpdatePanel ID="upItemDetail" runat="server" UpdateMode="Always">
                         <ContentTemplate>
                             <asp:TextBox ID="txtitemname" runat="server" style="text-transform:uppercase" CssClass="textbox" AutoPostBack="true" onblur="javascript:return Validate()"  OnTextChanged="txtitemname_TextChanged"></asp:TextBox>
                             <asp:Label ID="ErrItemNull" runat="server" class="ErrMsg" />
                             <asp:Label ID="txtexist" runat="server" class="ErrMsg"></asp:Label>
                         </ContentTemplate>
                     </asp:UpdatePanel>
                 </td>

                 <td style="width: 25%;">
                     <asp:Label ID="lblitemimage" runat="server" Text="Image"></asp:Label>
                     <asp:FileUpload ID="fpimg" runat="server" AllowMultiple="true" Font-Size="Medium" onblur="javascript:return Validate()" onchange="javascript:return Validate()" />
                     <asp:Label ID="myLabel2" runat="server" class="ErrMsg"></asp:Label>
                     <asp:Label ID="fperror" runat="server" class="ErrMsg"></asp:Label>
                 </td>
                 <td style="width: 25%;">
            </td>
             </tr>
             <tr>              
                 <td></td> 
                 <td colspan="3">
                <asp:DataList ID="DlstItemGallery" runat="server" DataKeyField="SNo" RepeatDirection="Horizontal" RepeatColumns="6" >
                           <ItemTemplate>
                               <asp:Image Width="26%" Height="20%" ID="imgItem" ImageUrl='<%# Bind("ImageName", "~/ItemImages/{0}") %>' runat="server" />
                           </ItemTemplate>
                       </asp:DataList>
                 </td>
             </tr>
             <tr>
                 <td style="color: #7a1380; font-weight: bold; padding: 2% 0 0% 0; height: 48px;">Price</td>
                 <td>
                     <asp:Label ID="lblprice" runat="server" Text="Price"></asp:Label>
                     <asp:TextBox ID="txtprice" runat="server" required="required" Text="0" onblur="javascript: return Validate()" onchange="javascript:return Validate()" CssClass="textbox" MaxLength="6"  onkeypress="return isNumberKey(event)" onKeyDown="return DisableCopyPaste(event)" onMouseDown="return DisableCopyPaste (event)"/>
                     <asp:Label ID="errtxtPrice" runat="server" class="ErrMsg"/>
                 </td>

                 <td></td>
                 <td></td>
             </tr>
             <tr>
                 <td></td>

                 <td>
                     <asp:Label ID="lbldimensions" runat="server" Text="Dimensions"></asp:Label><br />
                     <asp:TextBox ID="txtd1" runat="server" Style="width: 30%; float: left; margin: 0 0 0 1px;" Text="0"></asp:TextBox>
                     <asp:TextBox ID="txtd2" runat="server" Style="width: 30%; float: left; margin: 0 0 0 1px;" Text="0"></asp:TextBox>
                     <asp:TextBox ID="txtd3" runat="server" Style="width: 30%; float: left; margin: 0 0 0 1px;" Text="0"></asp:TextBox>
                 </td>

                 <td>
                     <asp:Label ID="lblcolor" runat="server" Text="Color"></asp:Label>
                     <asp:TextBox ID="txtcolor" runat="server" style="text-transform:uppercase" CssClass="textbox"></asp:TextBox>
                     <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtcolor" EnableClientScript="false"  ErrorMessage="Must Enter Color"></asp:RequiredFieldValidator>--%>
                 </td>

                 <td style="width: 29%">

                     <asp:Label ID="lblitemweight" runat="server" Text="Weight"></asp:Label>

                     <asp:TextBox ID="txtweight" runat="server" Text="0" CssClass="textbox"></asp:TextBox>
                 </td>
             </tr>

             <tr>
                 <td style="color: #7a1380; font-weight: bold; padding: 2% 0 0% 0; height: 48px;">Storage</td>
                 <td>

                     <asp:Label ID="lblinternalmemory" runat="server" Text="Internal Memory"></asp:Label>
                     <asp:TextBox ID="txtinternalmem" runat="server" CssClass="textbox"></asp:TextBox>

                 </td>
                 <td>
                     <asp:Label ID="lblexternalmem" runat="server" Text="External Memory"></asp:Label>
                     <asp:TextBox ID="txtexternalmem" runat="server" CssClass="textbox"></asp:TextBox></td>

                 <td></td>
             </tr>
             <tr>
                 <td style="color: #7a1380; font-weight: bold; padding: 2% 0 0% 0; height: 48px;">Camera</td>

                 <td>

                     <asp:Label ID="lblpcamera" runat="server" Text="Primary Camera"></asp:Label>
                     <asp:TextBox ID="txtpcamera" runat="server" CssClass="textbox"></asp:TextBox>
                 </td>

                 <td>
                     <asp:Label ID="lblscamera" runat="server" Text="Secondary Camera"></asp:Label>
                     <asp:TextBox ID="txtscamera" runat="server" CssClass="textbox"></asp:TextBox>
                 </td>

                 <td>
                     <asp:Label ID="lblcamspec" runat="server" Text="Camera Specifications"></asp:Label>
                     <asp:TextBox ID="txtcamspec" runat="server" CssClass="textbox"></asp:TextBox>
                 </td>
             </tr>
             <tr>
                 <td style="color: #7a1380; font-weight: bold; padding: 2% 0 0% 0; height: 48px;">Processor</td>
                 <td>
                     <asp:Label ID="lblptype" runat="server" Text="Processor Type"></asp:Label>
                     <asp:TextBox ID="txtptype" runat="server" CssClass="textbox"></asp:TextBox>
                 </td>
                 <td>
                     <asp:Label ID="lblspeed" runat="server" Text="Processor Speed"></asp:Label>
                     <asp:TextBox ID="txtpspeed" runat="server" CssClass="textbox"></asp:TextBox></td>
                 <td>
                     <asp:Label ID="lblram" runat="server" Text="Ram"></asp:Label>
                     <asp:TextBox ID="txtram" runat="server" CssClass="textbox"></asp:TextBox></td>
             </tr>
             <tr>
                 <td style="color: #7a1380; font-weight: bold; padding: 2% 0 0% 0; height: 48px;">Battery</td>

                 <td>
                     <asp:Label ID="lblbatterytype" runat="server" Text="Battery Type"></asp:Label>
                     <asp:TextBox ID="txtbatterytype" runat="server" CssClass="textbox"></asp:TextBox>

                 </td>
                 <td>
                     <asp:Label ID="lbltalktime" runat="server" Text="Talk Time"></asp:Label>
                     <asp:TextBox ID="txttalktime" runat="server" CssClass="textbox"></asp:TextBox>

                 </td>

                 <td>
                     <asp:Label ID="lblstandby" runat="server" Text="stand By"></asp:Label>
                     <asp:TextBox ID="txtStandBy" runat="server" CssClass="textbox"></asp:TextBox>
                 </td>

                 <td></td>
             </tr>
             <tr>
                 <td style="color: #7a1380; font-weight: bold; padding: 2% 0 0% 0; height: 48px;">Display</td>


                 <td>
                     <asp:Label ID="lbldisplaytype" runat="server" Text="Display Type"></asp:Label>
                     <asp:TextBox ID="txtdisplaytype" runat="server" CssClass="textbox"></asp:TextBox></td>

                 <td>
                     <asp:Label ID="lbldisplaysize" runat="server" Text="Display Size"></asp:Label>
                     <asp:TextBox ID="txtdisplaysize" runat="server" CssClass="textbox"></asp:TextBox></td>

                 <td></td>
             </tr>
             <tr>
                 <td style="color: #7a1380; font-weight: bold; padding: 2% 0 0% 0; height: 48px;">Os</td>
                 <td>
                     <asp:Label ID="lblosname" runat="server" Text="OS Name"></asp:Label>
                     <asp:TextBox ID="txtosname" runat="server" CssClass="textbox"></asp:TextBox></td>
                 <td>
                     <asp:Label ID="lblosversion" runat="server" Text="OS Version"></asp:Label>
                     <asp:TextBox ID="txtosversion" runat="server" CssClass="textbox"></asp:TextBox></td>
             </tr>
             <tr>
                 <td style="color: #7a1380; font-weight: bold; padding: 2% 0 0% 0; height: 48px;">Sim Details</td>
                 <td>
                     <asp:Label ID="lblsim" runat="server" Text="Sim"></asp:Label>
                     <asp:DropDownList ID="ddlsim" runat="server" CssClass="textbox">
                         <asp:ListItem>Micro Sim</asp:ListItem>
                         <asp:ListItem>Macro Sim</asp:ListItem>
                         <asp:ListItem>Nano Sim</asp:ListItem>
                           <asp:ListItem>CDMA</asp:ListItem>
                         <asp:ListItem>No</asp:ListItem>
                     </asp:DropDownList>
                 </td>

                 <td>
                     <asp:Label ID="lblsimtype" runat="server" Text="Sim Type"></asp:Label>
                     <asp:TextBox ID="txtsimtype" runat="server" CssClass="textbox"></asp:TextBox></td>
             </tr>
             <tr>
                 <td style="color: #7a1380; font-weight: bold; padding: 2% 0 0% 0; height: 48px;">Offer</td>
                 
                 <td>
                     <asp:Label ID="lblOfferImage" runat="server" Text="Offer Image"></asp:Label>
                      <asp:FileUpload ID="fpOfferImage" runat="server" AllowMultiple="true" Font-Size="Medium"  onblur="javascript:return Validate()" onchange="javascript:return Validate()" />
                     <asp:Label ID="fpOffer1" runat="server"  class="ErrMsg"></asp:Label>
                     <asp:Label ID="fpOffer2" runat="server"  class="ErrMsg"></asp:Label></td>

                 <td>
                     <asp:Label ID="lblOfferDesc" runat="server" Text="Offer Description"></asp:Label>
                     <asp:TextBox ID="txtOfferDesc" runat="server" CssClass="textbox"></asp:TextBox>
                 </td>
                 <td>
                     <asp:Image ID="imgOfferDesc" runat="server" Height="80" Width="120" Visible="true" AlternateText="img.jpg" />
                 </td>
             </tr>
             <tr>
                 <td style="color: #7a1380; font-weight: bold; padding: 2% 0 0% 0; height: 48px;">Compare Images</td>


                 <td>
                     <asp:Label ID="Label1" runat="server" Text="Upload Images"></asp:Label>
                     <asp:FileUpload ID="fpUploadImg" runat="server" AllowMultiple="true" Font-Size="Medium"  onblur="javascript:return Validate()" onchange="javascript:return Validate()" />
                     <asp:Label ID="myLabel" runat="server"  class="ErrMsg"></asp:Label>
                     <asp:Label ID="ErrFUimages" runat="server"  class="ErrMsg"></asp:Label>
                 </td>
                 <td  colspan="2">
                             <asp:DataList ID="ddlCompareImages" runat="server" DataKeyField="SNo" RepeatDirection="Horizontal" RepeatColumns="6">
                                 <ItemTemplate>
                                     <asp:Image Width="65%" ID="Image1" ImageUrl='<%# Bind("Name", "~/CompareImagesAndVideos/{0}") %>' Height="13%"  runat="server" />
                                 </ItemTemplate>
                             </asp:DataList>

                             &nbsp;</td>
                

                 
             </tr>
             <tr>
                         <td style="color: #7a1380; font-weight: bold; padding: 0% 0 0% 0; height: 80px;">Compare Video</td>
                             <td>
                     <asp:Label ID="Label2" runat="server" Text="Upload Video"></asp:Label>
                     <asp:FileUpload ID="fpUploadVideo" runat="server" Font-Size="Medium"  onblur="javascript:return Validate()" onchange="javascript:return Validate()"/>
                     <asp:Label ID="Label3" runat="server" Text="File size should not exceed 70MB" class="ErrMsg" ForeColor="#FF4E4E"/>
                     <asp:Label ID="myLabel1" runat="server" class="ErrMsg"/>
                     <asp:Label ID="ErrFUvideo" runat="server" class="ErrMsg"/>
                 </td>
                 <td colspan="2">
                     <asp:Label ID="lblcomparevideo" runat="server" Style="text-wrap:normal"></asp:Label>
                 </td>
                     </tr>
            
             <tr>
                 <td style="color: #7a1380; font-weight: bold; padding: 2% 0 0% 0; height: 48px;">Lot Verdict/Pro's & Con's</td>


                 <td>
                     <asp:Label ID="Label4" runat="server" Text="Lot Verdict"></asp:Label>
                     <asp:TextBox ID="txtverdict" runat="server" CssClass="textbox"></asp:TextBox>                    
                 </td>

                 <td>
                     <asp:Label ID="Label5" runat="server" Text="Pro's"></asp:Label>
                     <asp:TextBox ID="txtpros" runat="server" CssClass="textbox"></asp:TextBox>
                 </td>

                 <td>
                     <asp:Label ID="Label6" runat="server" Text="Con's"></asp:Label>
                     <asp:TextBox ID="txtcons" runat="server" CssClass="textbox"></asp:TextBox>
                 </td>
             </tr>
             <tr>
                 <td style="color: #7a1380; font-weight: bold; padding: 2% 0 0% 0; height: 48px;">Others</td>
                 <td>
                     <asp:Label ID="lblwifi" runat="server" Text="WIFI" CssClass="radlabel" Width="40%"></asp:Label>
                     <asp:RadioButtonList ID="radwifi" runat="server" RepeatDirection="Horizontal" Width="35%">
                         <asp:ListItem>Yes</asp:ListItem>
                         <asp:ListItem>No</asp:ListItem>
                     </asp:RadioButtonList>
                 </td>
                 <td>
                     <asp:Label ID="lbledge" runat="server" Text="EDGE" CssClass="radlabel" Width="32%"></asp:Label>
                     <asp:RadioButtonList ID="radedge" runat="server" RepeatDirection="Horizontal" TextAlign="Right" Width="35%">
                         <asp:ListItem>Yes</asp:ListItem>
                         <asp:ListItem>No</asp:ListItem>
                     </asp:RadioButtonList>
                 </td>

                 <td>
                     <asp:Label ID="lblgprs" runat="server" Text="GPRS" CssClass="radlabel" Width="36%"></asp:Label>
                     <asp:RadioButtonList ID="radgprs" runat="server" RepeatDirection="Horizontal" TextAlign="Right" Width="35%">
                         <asp:ListItem>Yes</asp:ListItem>
                         <asp:ListItem>No</asp:ListItem>
                     </asp:RadioButtonList>
                 </td>
             </tr>
             <tr>
                 <td></td>
                 <td>
                     <asp:Label ID="lbl3g" runat="server" Text="3G &nbsp; &nbsp;&nbsp;  " CssClass="radlabel" Width="40%"></asp:Label>
                     <asp:RadioButtonList ID="rad3g" runat="server" RepeatDirection="Horizontal" TextAlign="Right" Width="35%">
                         <asp:ListItem>Yes</asp:ListItem>
                         <asp:ListItem>No</asp:ListItem>
                     </asp:RadioButtonList></td>

                 <td>
                     <asp:Label ID="lblBluetooth" runat="server" Text="Bluetooth" CssClass="radlabel" Width="32%"></asp:Label>
                     <asp:RadioButtonList ID="radBluetooth" runat="server" RepeatDirection="Horizontal" TextAlign="Right" Width="35%">
                         <asp:ListItem>Yes</asp:ListItem>
                         <asp:ListItem>No</asp:ListItem>
                     </asp:RadioButtonList></td>
                 <td>
                     <asp:Label ID="lblflash" runat="server" Text="FLASH" CssClass="radlabel" Width="36%"></asp:Label>
                     <asp:RadioButtonList ID="radflash" runat="server" RepeatDirection="Horizontal" TextAlign="Right" Width="35%">
                         <asp:ListItem>Yes</asp:ListItem>
                         <asp:ListItem>No</asp:ListItem>
                     </asp:RadioButtonList></td>
             </tr>
             <tr>
                 <td></td>
                 <td>
                     <asp:Label ID="lbltouch" runat="server" Text="Touch Screen" CssClass="radlabel" Width="40%"></asp:Label>
                     <asp:RadioButtonList ID="radtouch" runat="server" RepeatDirection="Horizontal" TextAlign="Right" Width="35%">
                         <asp:ListItem>Yes</asp:ListItem>
                         <asp:ListItem>No</asp:ListItem>
                     </asp:RadioButtonList></td>

                 <td>
                     <asp:Label ID="lblmusic" runat="server" Text="Music Player" CssClass="radlabel" Width="33%"></asp:Label>
                     <asp:RadioButtonList ID="radmusic" runat="server" RepeatDirection="Horizontal" TextAlign="Right" Width="35%">
                         <asp:ListItem>Yes</asp:ListItem>
                         <asp:ListItem>No</asp:ListItem>
                     </asp:RadioButtonList></td>

                 <td>
                     <asp:Label ID="lblQwerty" runat="server" Text="Qwerty" CssClass="radlabel" Width="36%"></asp:Label>
                     <asp:RadioButtonList ID="radQwerty" runat="server" RepeatDirection="Horizontal" TextAlign="Right" Width="35%">
                         <asp:ListItem>Yes</asp:ListItem>
                         <asp:ListItem>No</asp:ListItem>
                     </asp:RadioButtonList></td>
             </tr>
         </table>
             
    <table>
          <tr>
            <td></td>
          
            <td></td>
            <td style="padding:3% 0 0 51%;"><asp:Button ID="btnSubmit" runat="server"   OnClientClick="javascript:return Validate()" Text="Submit" CssClass="lotbtn" OnClick="btnSubmit_Click"/>
                <asp:Button ID="btnCancel" runat="server" Text="Cancel"  CssClass="lotbtn" OnClick="btnCancel_Click"/>
                          
                <asp:Label ID="ErrMsg" runat="server" class="ErrMsg" />

            </td>
        </tr>
    </table>
    </div>
   

</asp:Content>

