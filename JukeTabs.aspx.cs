﻿using AjaxControlToolkit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class JukeTabs : System.Web.UI.Page
{
    object lockTarget = new object();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LotAdminUser"] == null)
        {
            Response.Redirect("Default.aspx");
        }
        else if (!IsPostBack)
        {
           // TabContainer1_ActiveTabChanged(TabContainer, null);
            //TabPanelFile.Attributes.Add("onClick", "ClickMe(); return false;");
        }
        
    }
    protected void btnFileSave_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtfilename.Text))
        {
            LotDBEntity.Models.VASTabOne VAS1 = new LotDBEntity.Models.VASTabOne();
            VAS1.Name = txtfilename.Text.ToUpper().Trim();
            lock (lockTarget)
            {
                long id1 = 0;
                if (Apps.lotContext.VASTabOne.ToList().Count > 0)
                    id1 = Apps.lotContext.VASTabOne.Max(c => c.ID);
                if (id1 > 0)
                {
                    VAS1.ID = id1 + 1;
                }
                else
                {
                    VAS1.ID = 1;
                }
                VAS1.LastUpdatedTime = DateTime.Now;
                Apps.lotContext.VASTabOne.Add(VAS1);
                Apps.lotContext.SaveChanges();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Saved successfully ');", true);
                txtfilename.Text = "";
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Please Enter the Name ');", true);
        }
    }
    protected void btnCategSave_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtCategName.Text))
        {
            LotDBEntity.Models.VASTabTwo VAS2 = new LotDBEntity.Models.VASTabTwo();
            VAS2.Name = txtCategName.Text.ToUpper().Trim();
            lock (lockTarget)
            {
                long id1 = 0;
                if (Apps.lotContext.VASTabTwo.ToList().Count > 0)
                    id1 = Apps.lotContext.VASTabTwo.Max(c => c.ID);
                if (id1 > 0)
                {
                    VAS2.ID = id1 + 1;
                }
                else
                {
                    VAS2.ID = 1;
                }
                VAS2.LastUpdatedTime = DateTime.Now;
                Apps.lotContext.VASTabTwo.Add(VAS2);
                Apps.lotContext.SaveChanges();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Saved successfully ');", true);
                txtCategName.Text = "";
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Please Enter the Name ');", true);
        }
    }
    protected void btnArtistSave_Click(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(txtArtistName.Text))
        {
            LotDBEntity.Models.VASTabThree VAS3 = new LotDBEntity.Models.VASTabThree();
            VAS3.Name = txtArtistName.Text.ToUpper().Trim();
            lock (lockTarget)
            {
                long id1 = 0;
                if (Apps.lotContext.VASTabThree.ToList().Count > 0)
                    id1 = Apps.lotContext.VASTabThree.Max(c => c.ID);
                if (id1 > 0)
                {
                    VAS3.ID = id1 + 1;
                }
                else
                {
                    VAS3.ID = 1;
                }
                VAS3.LastUpdatedTime = DateTime.Now;
                Apps.lotContext.VASTabThree.Add(VAS3);
                Apps.lotContext.SaveChanges();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Saved successfully ');", true);
                txtArtistName.Text = "";
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Please Enter the Name ');", true);
        }
    }
    protected void btnCateg4Save_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(TextBox1.Text))
        {
            LotDBEntity.Models.VASTabFour VAS4 = new LotDBEntity.Models.VASTabFour();
            VAS4.Name = TextBox1.Text.ToUpper().Trim();
            lock (lockTarget)
            {
                long id1 = 0;
                if (Apps.lotContext.VASTabFour.ToList().Count > 0)
                    id1 = Apps.lotContext.VASTabFour.Max(c => c.ID);
                if (id1 > 0)
                {
                    VAS4.ID = id1 + 1;
                }
                else
                {
                    VAS4.ID = 1;
                }
                VAS4.LastUpdatedTime = DateTime.Now;
                Apps.lotContext.VASTabFour.Add(VAS4);
                Apps.lotContext.SaveChanges();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Saved successfully ');", true);
                TextBox1.Text = "";
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Please Enter the Name ');", true);
        }
    }
    protected void btnCateg5Save_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(TextBox2.Text))
        {
            LotDBEntity.Models.VASTabFive VAS5 = new LotDBEntity.Models.VASTabFive();
            VAS5.Name = TextBox2.Text.ToUpper().Trim();
            lock (lockTarget)
            {
                long id1 = 0;
                if (Apps.lotContext.VASTabFive.ToList().Count > 0)
                    id1 = Apps.lotContext.VASTabFive.Max(c => c.ID);
                if (id1 > 0)
                {
                    VAS5.ID = id1 + 1;
                }
                else
                {
                    VAS5.ID = 1;
                }
                VAS5.LastUpdatedTime = DateTime.Now;
                Apps.lotContext.VASTabFive.Add(VAS5);
                Apps.lotContext.SaveChanges();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Saved successfully ');", true);
                TextBox2.Text = "";
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Please Enter the Name ');", true);
        }


    }
    protected void btnCateg5Cancel_Click(object sender, EventArgs e)
    {
        TextBox2.Text = "";
    }
    protected void btnCateg4Cancel_Click(object sender, EventArgs e)
    {
        TextBox1.Text = "";
    }
    protected void btnArtistCancel_Click(object sender, EventArgs e)
    {
        txtArtistName.Text = "";
    }
    protected void btnCategCancel_Click(object sender, EventArgs e)
    {
        txtCategName.Text = "";
    }
    protected void btnFileCancel_Click(object sender, EventArgs e)
    {
        txtfilename.Text = "";
    }
    protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
    {
        //try
        //{
        //    if (TabContainer1.ActiveTabIndex == 1)
        //    {
        //        ctrlActionControl.ActionPanelMode = true;
        //        ctrlStatusControl.StatusPanelMode = false;
        //    }

        //    if (tbMain.ActiveTabIndex == 2)
        //    {
        //        ctrlStatusControl.StatusPanelMode = true;
        //        ctrlActionControl.ActionPanelMode = false;
        //    }
        //}
        //catch (Exception ex)
        //{
        //    Support.ExceptionHandler.HandleException(ex);
        //}
    }
}