﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AddCategories : System.Web.UI.Page
{
    LotDBEntity.Models.Brands brand;
    LotDBEntity.Models.Category Categ;
    object lockTarget = new object();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LotAdminUser"] == null)
        {
            Response.Redirect("Default.aspx");
        }
        else
        {
            if (!IsPostBack)
            {
                LogHelper.Logger.Info("Starts When PageLoad in AddCategories");
                lock (lockTarget)
                {
                    var brands = Apps.lotContext.Brands.ToList();
                    ddlBrand.DataSource = brands;
                    ddlBrand.DataTextField = "Name";
                    ddlBrand.DataValueField = "SNo";
                    ddlBrand.DataBind();
                }
                LogHelper.Logger.Info("Ends When PageLoad in AddCategories");
                if (Request.QueryString["SNo"] != null)
                {
                    btnSubmit.Text = "Update";
                    Button1.Text = "Reset";
                    lbladd.Text = "Edit Category";
                    getUpdatedData();
                }
            }
        }
    }

    private void getUpdatedData()
    {
        LogHelper.Logger.Info("Starts getUpdatedData() in AddCategories");
        lock (lockTarget)
        {
            long Id = Convert.ToInt64(Request.QueryString["SNo"]);
            LotDBEntity.Models.Category UpdateCateg = Apps.lotContext.Categories.Where(c => c.SNo == Id).FirstOrDefault();
            txtCategoryName.Text = UpdateCateg.Name;
            ddlBrand.Items.FindByText(UpdateCateg.Brand.Name).Selected = true;
            ddlBrand.Enabled = false;
            txtDesc.Text = UpdateCateg.Description;
            txtexist.Text = "";
        }
        LogHelper.Logger.Info("Starts getUpdatedData() in AddCategories");
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (btnSubmit.Text == "Submit")
        {
            if (txtexist.Text != "Already Exists" && ddlBrand.SelectedItem.Text != "Select")
            {
                
                    LogHelper.Logger.Info("Starts Submit Button Clicks in AddCategories");
                    long val = Convert.ToInt64(ddlBrand.SelectedValue);
                    string strName = txtCategoryName.Text.ToUpper().Trim();
                    lock (lockTarget)
                    {
                        brand = Apps.lotContext.Brands.Where(c => c.SNo == val).FirstOrDefault();
                        if (brand.LstCategory != null)
                        {
                            Categ = brand.LstCategory.Where(c => c.Name == strName).FirstOrDefault();
                        }
                        else
                        {
                            Categ = null;
                        }
                        if (Categ == null)
                        {
                            Categ = new LotDBEntity.Models.Category();
                            long id = 0; ;
                            Categ.Name = txtCategoryName.Text.ToUpper().Trim();
                            lock (lockTarget)
                            {
                                if (Apps.lotContext.Categories.ToList().Count > 0)
                                    id = Apps.lotContext.Categories.Max(c => c.ID);
                                if (id > 0)
                                {
                                    Categ.ID = id + 1;
                                }
                                else
                                {
                                    Categ.ID = 1;
                                }
                            }
                            Categ.Description = txtDesc.Text;
                            Categ.Brand = brand;
                            Categ.LastUpdatedTime = DateTime.Now;
                            LogHelper.Logger.Info("Save Changes Starts in Categories");
                            Apps.lotContext.Categories.Add(Categ);
                            Apps.lotContext.SaveChanges();
                            LogHelper.Logger.Info("Save Changes ends in Categories");
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Category Saved Successfully ');", true);
                            //ScriptManager.RegisterStartupScript(this, this.GetType(), "name", "SuccessMsg();", true);
                            btnSubmit.Enabled = true;
                            ClearAll();
                }
                
                }
                LogHelper.Logger.Info("Ends Submit Button Clicks in AddCategories");
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Please Enter Required Fields')", true);
            }
        }
            else if (btnSubmit.Text == "Update")
            {
                if (txtexist.Text != "Already Exists" && ddlBrand.SelectedItem.Text != "Select")
                {

                    long Id = Convert.ToInt64(Request.QueryString["SNo"]);
                    LogHelper.Logger.Info("Starts Update Clicks in AddCategories Button");
                    lock (lockTarget)
                    {
                        LotDBEntity.Models.Category CategUpdate = Apps.lotContext.Categories.Where(c => c.SNo == Id).FirstOrDefault();
                        if (CategUpdate != null)
                        {
                            if (CategUpdate.Name != txtCategoryName.Text.Trim())
                            {
                                checkCategoryName();
                            }
                            if (txtexist.Text == "Already Exists")
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Item name already exists');", true);
                            }
                            else
                            {
                                btnSubmit.Enabled = false;
                                CategUpdate.Name = txtCategoryName.Text.ToUpper().Trim();
                                CategUpdate.Description = txtDesc.Text.ToUpper();
                                CategUpdate.LastUpdatedTime = DateTime.Now;
                                Apps.lotContext.SaveChanges();
                                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Hello", "alert('Category updated successfully');window.location='ViewCategories.aspx'", true);
                                //ScriptManager.RegisterStartupScript(this, this.GetType(), "name", "window.location='ViewCategories.aspx'", true);
                                //ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Category updated successfully ');window.location='ViewCategories.aspx'", true);                   
                                //Response.Redirect("ViewCategories.aspx");
                                btnSubmit.Enabled = true;
                            }
                            LogHelper.Logger.Info("Starts Update Clicks in AddCategories Button");
                        }
                    }
                }
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Please Enter Required Fields')", true);
            }
        }
        catch (Exception ex)
        {
            
        }
        
       
    }

    private void ClearAll()
    {
        ddlBrand.ClearSelection();
        ddlBrand.Items.FindByText("Select").Selected = true;
        txtCategoryName.Text = "";
        txtDesc.Text = "";
        errBrand.Text = "";
        txtexist.Text = "";
        ErrCategNull.Text = "";   
    }

    protected void txtCategoryName_TextChanged(object sender, EventArgs e)
    {
        LogHelper.Logger.Info("Starts txtCategoryName_TextChanged in AddCategories");
        checkCategoryName();
        LogHelper.Logger.Info("Ends txtCategoryName_TextChanged in AddCategories");
    }

    private void checkCategoryName()
    {
        if (ddlBrand.SelectedItem.Text != "Select")
        {
            string strName = txtCategoryName.Text.ToUpper().Trim();
            long val = Convert.ToInt64(ddlBrand.SelectedValue);
            lock (lockTarget)
            {
                brand = Apps.lotContext.Brands.Where(c => c.SNo == val).FirstOrDefault();
                if (brand != null)
                {
                    if (brand.LstCategory != null)
                    {
                        lock (lockTarget)
                        {
                            LogHelper.Logger.Info("CategoryName" + strName);
                            Categ = brand.LstCategory.Where(c => c.Name == strName).FirstOrDefault();
                        }
                    }
                    else
                    {
                        Categ = null;
                    }

                }
                if (Categ != null)
                {
                    txtexist.Text = "Already Exists";
                    txtCategoryName.Focus();
                }
                else
                {
                    Categ = new LotDBEntity.Models.Category();
                    txtexist.Text = "";
                    txtDesc.Focus();
                }
            }
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        if (Button1.Text == "Cancel")
        {
            LogHelper.Logger.Info("Starts When Cancel Button in AddCatgeories");
            ClearAll();
            ddlBrand.ClearSelection();
            ddlBrand.Items.FindByText("Select").Selected=true;
            LogHelper.Logger.Info("Ends When Cancel Button in AddCatgeories");
        }
        else
        {
            LogHelper.Logger.Info("Starts When reset Button in AddCatgeories");
            getUpdatedData();
            LogHelper.Logger.Info("Ends When reset Button in AddCatgeories");
        }
    }
    protected void ddlBrand_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlBrand.SelectedItem.Text == "Select")
        {
            ClearAll();
        }
    }
}