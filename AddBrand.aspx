﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMasterPage.master" AutoEventWireup="true" CodeFile="AddBrand.aspx.cs" Inherits="AddBrand" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .ErrMsg {
            color: #DD4B39;
            font-family: Arial;
            font-size: 10px;
        }

        .pgHeading {
            font-weight: 600;
            font-family: sans-serif;
            color: #92278f;
        }
    </style>
  
 
    <script type="text/javascript">
        function Validate() {
            var btnSubmit = document.getElementById('<%=btnSubmit.ClientID %>');
            var txtBrandName = document.getElementById('<%=txtBrandName.ClientID %>');
            var FUBrandLogo = document.getElementById('<%=FUBrandLogo.ClientID %>').value;
            var txtexist = document.getElementById('<%=txtexist.ClientID %>');
            var myLabel = document.getElementById('<%=myLabel.ClientID %>');
            var ErrBrandName = document.getElementById('<%=ErrBrandName.ClientID %>');

            if (txtBrandName.value == "") {
                ErrBrandName.innerHTML = "Brand name should not be empty";
            }
            else {
                ErrBrandName.innerHTML = "";
            }

            if (txtexist.textContent == "Already Exists") {
                ErrBrandName.innerHTML = "";
            }
            else {
                txtexist.innerHTML = "";
            }
            var uploadcontrol = document.getElementById('<%=FUBrandLogo.ClientID%>').value;
            //Regular Expression for fileupload control.
            var reg = /^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+(.jpeg|.JPEG|.gif|.GIF|.png|.PNG|.jpg|.JPG)$/;

            if (uploadcontrol.length > 0) {
                if (reg.test(uploadcontrol)) {
                    myLabel.innerHTML = "";
                }
                else {
                    myLabel.innerHTML = "Only png, jpg and gif files are allowed!";
                }
            }

            if (uploadcontrol.length == 0) {
                myLabel.innerHTML = "";
            }

            if (btnSubmit.value == "Submit") {

                if (FUBrandLogo.length == 0) {
                    myLabel.innerHTML = "Please select brand logo";
                }
                if (ErrBrandName.innerHTML == "" && myLabel.innerHTML == "" && txtexist.textContent != "Already Exists") {                 
                    return true;
                }
            }
            else if (btnSubmit.value == "Update") {

                if (ErrBrandName.innerHTML == "" && txtexist.textContent != "Already Exists" && myLabel.innerHTML == "") {
                    return true;
                }
            }
            return false;
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div style="margin: 5% 0 0 0; text-align: left; padding: 2% 0 0% 11.4%;">
        <h2 style="font-size: 28px;">
            <asp:Label ID="lbladd" runat="server" Text="Add Brand" class="pgHeading" />
        </h2>
    </div>
    <div style="width: 100%; padding: 3% 0 0 23%; margin: -8% 0 0 0;">
        <asp:UpdatePanel ID="upBrandPage" runat="server" UpdateMode="Always">
            <ContentTemplate>

            
        <table style="width: 100%; margin: 6% 0 4% 6%;">
            <tr>
                <td style="width: 15%;">
                    <asp:Label ID="Label1" runat="server" Text="Brand Name" /></td>
                <td>
                    <asp:UpdatePanel ID="upbrand" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                            <asp:TextBox ID="txtBrandName" runat="server" style="text-transform:uppercase" placeholder="Brand Name" onblur="javascript: return Validate();" Width="45%" AutoPostBack="true" OnTextChanged="txtBrandName_TextChanged" />
                            <asp:Label ID="txtexist" runat="server" class="ErrMsg" />
                            <asp:Label ID="ErrBrandName" runat="server" class="ErrMsg" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label2" runat="server" Text="Brand Description" /><br />
                    <asp:Label ID="Label7" Style="Font-Size: 12px;" runat="server" Text="(Optional)" /></td>
                <td>
                    <asp:TextBox ID="txtBrandDesc" runat="server" placeholder="Brand Description" Width="45%" TextMode="MultiLine"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label6" runat="server" Text="Brand Logo" /></td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                    <asp:FileUpload ID="FUBrandLogo" runat="server" Style="float: left; top: 0px; left: 0px;" Width="45%" onblur="javascript: return Validate();" onchange="javascript: return Validate();" />
                    </ContentTemplate>
                        </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td>

                </td>
                <td>
                    <asp:Label ID="myLabel" runat="server" class="ErrMsg" />
                    <asp:Label ID="lblFUtemp" runat="server" />
                    <asp:Label ID="lblImgLocation" runat="server" Font-Size="12px" ForeColor="#333" />
                </td>
            </tr>
            </table>
                </ContentTemplate>
        </asp:UpdatePanel>
        <table>
            <tr>
                <td></td>
                <td style="padding: 4% 0 0 36.5%;">
                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClientClick="javascript: return Validate();" OnClick="btnSubmit_Click" CssClass="lotbtn" />
                    <asp:Button ID="Button1" runat="server" Text="Cancel" OnClick="Button1_Click" CssClass="lotbtn" />                  
                
                </td>
            </tr>
        </table>


    </div>
</asp:Content>

