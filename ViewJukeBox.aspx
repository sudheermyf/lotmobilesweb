﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMasterPage.master" AutoEventWireup="true" CodeFile="ViewJukeBox.aspx.cs" Inherits="ViewJukeBox" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
      <style type="text/css">
        .ErrMsg {
            color: #DD4B39;
            font-family: Arial;
            font-size: 10px;
        }
        .pgHeading {
            font-weight: 600;
            font-family: sans-serif;
            color: #92278f;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
      <div style="margin: 5% 0 0 0; text-align: left; padding: 2% 0 0% 11.5%;">
        <h2 style="font-size: 28px;">
            <asp:Label ID="lbladd" runat="server" Text="View JukeBox" class="pgHeading" /></h2>
    </div>

        <asp:UpdatePanel ID="upmodel" runat="server">
            <ContentTemplate>
                
            <div style="width: 100%; padding: 3% 0 0 27.5%; margin: -2% 0 0 0;">
                
                <div style="width: 35%; float: left;">
                    <asp:Label ID="lblsearch" runat="server" Text="Search By Category"></asp:Label>
                    <br /><br />                        
                    <asp:DropDownList ID="ddlsearch" runat="server" AutoPostBack="true" AppendDataBoundItems="true" Width="85%" OnSelectedIndexChanged="ddlsearch_SelectedIndexChanged" >
                        <asp:ListItem Value="0">Select</asp:ListItem>
                        <asp:ListItem Value="1">FileType</asp:ListItem>
                        <asp:ListItem Value="2">Category</asp:ListItem>
                        <asp:ListItem Value="3">Artist</asp:ListItem>
                        <asp:ListItem Value="4">Music Director</asp:ListItem>
                        <asp:ListItem Value="5">Album</asp:ListItem>
                    </asp:DropDownList>
                    <br /><br />
                    <asp:DropDownList ID="ddl1" runat="server" DataTextField="Name" DataValueField="SNo" AutoPostBack="true" Width="85%" OnSelectedIndexChanged="ddl1_SelectedIndexChanged" AppendDataBoundItems="true">
                        <asp:ListItem Value="0">Select</asp:ListItem>
                    </asp:DropDownList>
                    <br /><br />
                    <asp:ListBox ID="lstSongs" runat="server" AutoPostBack="true" onblur="javascript: return Validate()" OnSelectedIndexChanged="lstSongs_SelectedIndexChanged" Width="85%" Height="310px"></asp:ListBox>

                </div>

                <div style="width: 60%; float: left; margin: 0% 0 3% 0%;">
                    <div>
                        <asp:Label ID="lblName" runat="server" Text="Name"></asp:Label>
                        <br />
                        <asp:DropDownList ID="ddlfiletype" runat="server" Width="300px" DataTextField="Name" DataValueField="SNo" AutoPostBack="true" AppendDataBoundItems="true" CssClass="ddl" >
                            <asp:ListItem Value="-1">Select</asp:ListItem>
                        </asp:DropDownList>
                        <br />
                        <asp:DropDownList ID="ddlcategory" runat="server" Width="300px" DataTextField="Name" DataValueField="SNo" AutoPostBack="true" AppendDataBoundItems="true" CssClass="ddl">
                            <asp:ListItem Value="-1">Select</asp:ListItem>
                        </asp:DropDownList>
                        <br />
                        <asp:DropDownList ID="ddlartist" runat="server" Width="300px" DataTextField="Name" DataValueField="SNo" AutoPostBack="true" AppendDataBoundItems="true" CssClass="ddl">
                            <asp:ListItem Value="-1">Select</asp:ListItem>
                        </asp:DropDownList>
                        <br />
                        <asp:DropDownList ID="ddlCateg4" runat="server" Width="300px" DataTextField="Name" DataValueField="SNo" AutoPostBack="true" AppendDataBoundItems="true" CssClass="ddl">
                            <asp:ListItem Value="-1">Select</asp:ListItem>
                        </asp:DropDownList>
                        <br />
                        <asp:DropDownList ID="ddlCateg5" runat="server" Width="300px" DataTextField="Name" DataValueField="SNo" AutoPostBack="true"  AppendDataBoundItems="true" CssClass="ddl">
                            <asp:ListItem Value="-1">Select</asp:ListItem>
                        </asp:DropDownList>
                        <br />
                        <asp:Label ID="lblId" runat="server" Visible="false"></asp:Label>
                    </div>
                </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    <div style="margin:16% 0 0 62%;">
        <asp:Button ID="btnedit" runat="server" Text="Edit" CssClass="lotbtn" OnClientClick="javascript: return Validate()" OnClick="btnedit_Click"/>
        <asp:Button ID="btndelete" runat="server" Text="Delete" CssClass="lotbtn" OnClick="btndelete_Click" />
    </div>
</asp:Content>

