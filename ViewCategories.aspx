﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMasterPage.master" AutoEventWireup="true" CodeFile="ViewCategories.aspx.cs" Inherits="ViewCategories" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .ErrMsg {
            color: #DD4B39;
            font-family: Arial;
            font-size: 10px;
        }
        .pgHeading {
            font-weight: 600;
            font-family: sans-serif;
            color: #92278f;
        }
    </style>
    <script type="text/javascript">        
        function Validate() {
            var ddlBrand = document.getElementById('<%=ddlBrand.ClientID %>'); 
            var lblErrddlBrand = document.getElementById('<%=lblErrddlBrand.ClientID %>'); 
            var lblCategoryName = document.getElementById('<%=lblCategoryName.ClientID %>'); 
            var lstCategory = document.getElementById('<%=lstCategory.ClientID %>'); 
            var ErrEmptyBrandName = document.getElementById('<%=ErrEmptyBrandName.ClientID %>');
            var lblDesc = document.getElementById('<%=lblDesc.ClientID %>');

            if (ddlBrand.options[ddlBrand.selectedIndex].text == "Select Brand") {
                lblErrddlBrand.innerHTML = "Select Brand";
                lblCategoryName.innerHTML = "";
                lblDesc.innerHTML = "";
            }
            else {
                lblErrddlBrand.innerHTML = "";
            }

            if (lblCategoryName.innerHTML.length == 0 && lstCategory.innerHTML.length >0) {
                ErrEmptyBrandName.innerHTML = "Select List Item";
            }
            else {
                ErrEmptyBrandName.innerHTML = "";
            }
            if (ddlBrand.options[ddlBrand.selectedIndex].text != "Select Brand" && lblCategoryName.innerHTML.length != 0) {
                return true;
            }
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div style="margin: 5% 0 0 0; text-align: left; padding: 2% 0 0% 11.5%;">
        <h2 style="font-size: 28px;">
            <asp:Label ID="lbladd" runat="server" Text="View Categories" class="pgHeading" /></h2>
    </div>
    <asp:UpdatePanel ID="uPanel" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <div style="width: 100%; padding: 3% 0 0 27.5%; margin: -2% 0 0 0;">

                <div style="width: 40%; float: left;">

                    <asp:Label ID="Label2" runat="server" Text="Select Brand"></asp:Label>

                    <asp:Label ID="lblErrddlBrand" runat="server" CssClass="ErrMsg" />

                    <asp:DropDownList ID="ddlBrand" runat="server" DataTextField="Name" DataValueField="SNo" AutoPostBack="true" onblur="javascript: return Validate()"
                        AppendDataBoundItems="true" OnSelectedIndexChanged="ddlBrand_SelectedIndexChanged" Style="margin: 1% 0 4% 0; width: 90%;">
                        <asp:ListItem Value="-1">Select Brand</asp:ListItem>
                    </asp:DropDownList>

                    <asp:Label ID="ErrEmptyBrandName" runat="server" CssClass="ErrMsg" />

                    <asp:ListBox ID="lstCategory" runat="server" Height="350px" AutoPostBack="true"
                        Width="90%" DataTextField="Name" DataValueField="SNo" onblur="javascript: return Validate()"
                        OnSelectedIndexChanged="lstCategory_SelectedIndexChanged" />
                </div>

                <div style="width: 60%; float: left; margin: 4% 0 3% 0%;">
                    <table style="width: 80%;">
                        <tr>
                            <td>
                                <asp:Label ID="lblId" runat="server" Visible="false" /></td>
                        </tr>
                        <tr>
                            <td style="width: 19%; text-align: left;">
                                <h3>
                                    <asp:Label ID="lblCategoryName" runat="server" Style="font-weight: 600; font-family: sans-serif; font-size: 22px; color: #6e6e6e;" /></h3>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" style="padding: 5% 0 0% 0%;">
                                <h4>
                                    <asp:Label ID="Label4" runat="server" Text="Description" Style="float: left; font-weight: 600; font-family: sans-serif; color: #6e6e6e;" /></h4>
                                <br />
                                <br />
                                <asp:TextBox ID="lblDesc" runat="server" Style="line-height: 1.2em;" />
                                <br />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div style="margin:1% 0 0 65%;padding:3% 0 0 0%;">
        <asp:Button ID="btnEdit" runat="server" Text="Edit" CssClass="lotbtn" OnClientClick="javascript: return Validate()" OnClick="btnEdit_Click" />
        <asp:Button ID="brnDelete" runat="server" Text="Delete" CssClass="lotbtn" OnClick="brnDelete_Click" />
    </div>
</asp:Content>

