﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AddPriceRange : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LotAdminUser"] == null)
        {
            Response.Redirect("Default.aspx");
        }
        else
        {
            if (!IsPostBack)
            {
                gdview.DataSource = Apps.lotContext.PriceFilters.ToList();
                gdview.DataBind();
            }
        }
    }
    protected void gdview_RowEditing(object sender, GridViewEditEventArgs e)
    {
        Label lbl = (Label)gdview.Rows[e.NewEditIndex].FindControl("lblorderid");
        Session["firstvalue"] = Convert.ToInt32(lbl.Text);
    }

    protected void gdview_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
       
    }
    protected void gdview_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {

    }
}