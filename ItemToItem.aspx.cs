﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class ItemToItem : System.Web.UI.Page
{
    object lockTarget = new object();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LotAdminUser"] == null)
        {
            Response.Redirect("Default.aspx");
        }
        else
        {
            if (!IsPostBack)
            {

                if (Apps.lotContext.BasicItems != null)
                {
                    lock (lockTarget)
                    {
                        LogHelper.Logger.Info("Starts When Checking BasicItems Not Null");
                        lstAvalilableItems.DataSource = Apps.lotContext.BasicItems.ToList();
                        lstAvalilableItems.DataTextField = "ItemName";
                        lstAvalilableItems.DataValueField = "SNo";
                        lstAvalilableItems.DataBind();
                        LogHelper.Logger.Info("Ends When Checking BasicItems Not Null");
                    }
                   
                }
            }
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            LogHelper.Logger.Info("Starts When Save Clicks");
            LotDBEntity.Models.BasicItem lstSimilarProducts;
            if (lstAvalilableItems.SelectedValue != "" && lstSelectedSimilarItems.SelectedValue != "")
            {
                if (lstSelectedSimilarItems.GetSelectedIndices().Count() < 4)
                {
                    long val = Convert.ToInt64(lstAvalilableItems.SelectedValue);
                    LotDBEntity.Models.BasicItem selectedItem = Apps.lotContext.BasicItems.Where(c => c.SNo == val).FirstOrDefault();
                    if (selectedItem != null && selectedItem.LstSimilarProducts != null && selectedItem.LstSimilarProducts.Count > 0)
                    {

                        //List<LotDBEntity.Models.SimilarProducts> lstsim = Apps.lotContext.SimilarProducts.Where(c => c.BaseItemID == selectedItem.ID).ToList();
                        //foreach (var item in lstsim)
                        //{
                        //    Apps.lotContext.SimilarProducts.Remove(item);
                        //    Apps.lotContext.SaveChanges();
                        //}

                        Apps.lotContext.SimilarProducts.RemoveRange(selectedItem.LstSimilarProducts);
                        Apps.lotContext.SaveChanges();
                    }
                    foreach (ListItem li in lstSelectedSimilarItems.Items)
                    {
                        if (li.Selected)
                        {
                            LogHelper.Logger.Info("Starts When Item Selected in Selected Similar Items");
                            lock (lockTarget)
                            {
                                //lstSimilarProducts = Apps.lotContext.BasicItems.Where(c => c.ItemName == li.Text).FirstOrDefault();
                                long id = Convert.ToInt64(li.Value);
                                lstSimilarProducts = Apps.lotContext.BasicItems.Where(c => c.SNo.Equals(id)).FirstOrDefault();
                                //long val = Convert.ToInt64(lstAvalilableItems.SelectedValue);

                                //LotDBEntity.Models.BasicItem selectedItem = Apps.lotContext.BasicItems.Where(c => c.SNo == val).FirstOrDefault();

                                if (lstSimilarProducts != null)
                                {
                                    LotDBEntity.Models.SimilarProducts SimProd = new LotDBEntity.Models.SimilarProducts(lstSimilarProducts);
                                    SimProd.LastUpdatedTime = DateTime.Now;
                                    SimProd.BaseItemID = selectedItem.ID;
                                    SimProd.ItemSKU = lstSimilarProducts.ID;
                                    lock (lockTarget)
                                    {
                                        if (Apps.lotContext.SimilarProducts.ToList().Count > 0)
                                            SimProd.ID = (long)(Apps.lotContext.SimilarProducts.Max(m => m.ID) + 1);
                                        else
                                            SimProd.ID = 1;
                                        lock (lockTarget)
                                        {
                                            if (selectedItem.LstSimilarProducts==null)
                                            {
                                                List<LotDBEntity.Models.SimilarProducts> similarProd = new List<LotDBEntity.Models.SimilarProducts>();
                                                similarProd.Add(SimProd);
                                                selectedItem.LstSimilarProducts = similarProd;
                                            }
                                            else
                                            {
                                                selectedItem.LstSimilarProducts.Add(SimProd);
                                            }
                                            //Apps.lotContext.SaveChanges();
                                        }
                                    }
                                }

                                #region Commented
                                //LotDBEntity.Models.SimilarProducts SimProd = new LotDBEntity.Models.SimilarProducts();
                                //long id = 0;
                                //if (selectedItem.LstSimilarProducts != null && selectedItem.LstSimilarProducts.Count > 0)
                                //{
                                //    if (selectedItem.LstSimilarProducts.Count < 4)
                                //    {
                                //        lstSimilarProducts.ForEach(c =>
                                //               {
                                //                   lock (lockTarget)
                                //                   {
                                //                       LotDBEntity.Models.SimilarProducts similarProd = selectedItem.LstSimilarProducts.Where(d => d.ItemSKU == c.SNo).FirstOrDefault();
                                //                       if (similarProd == null)
                                //                       {
                                //                           similarProd = new LotDBEntity.Models.SimilarProducts(c);
                                //                           similarProd.BaseItemID = selectedItem.SNo;
                                //                           similarProd.LastUpdatedTime = DateTime.Now;
                                //                           lock (lockTarget)
                                //                           {
                                //                               if (Apps.lotContext.SimilarProducts.ToList().Count > 0)
                                //                                   similarProd.ID = (long)(Apps.lotContext.SimilarProducts.Max(m => m.ID) + 1);
                                //                               else
                                //                                   similarProd.ID = 1;
                                //                               selectedItem.LstSimilarProducts.Add(similarProd);
                                //                           }
                                //                       }
                                //                   }
                                //               });
                                //    }
                                //    else
                                //    {
                                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Similar Items Saved Successfully...');", true);
                                //    }
                                //}

                                //else
                                //{
                                //    long val1 = Convert.ToInt64(lstAvalilableItems.SelectedValue);

                                //    lstSimilarProducts.ForEach(c =>
                                //    {
                                //        LotDBEntity.Models.SimilarProducts SimProd = new LotDBEntity.Models.SimilarProducts(c);
                                //        SimProd.LastUpdatedTime = DateTime.Now;
                                //        SimProd.BaseItemID = selectedItem.SNo;
                                //        lock (lockTarget)
                                //        {
                                //            if (Apps.lotContext.SimilarProducts.ToList().Count > 0)
                                //                SimProd.ID = (long)(Apps.lotContext.SimilarProducts.Max(m => m.ID) + 1);
                                //            else
                                //                SimProd.ID = 1;
                                //            lock (lockTarget)
                                //            {
                                //                selectedItem.LstSimilarProducts = Apps.lotContext.SimilarProducts.Where(d => d.SNo == val).ToList();
                                //                selectedItem.LstSimilarProducts.Add(SimProd);
                                //            }
                                //        }
                                //    });

                                //} 
                                #endregion
                                LogHelper.Logger.Info("Ends When Item Selected in Selected Similar Items");
                            }
                            Apps.lotContext.SaveChanges();
                        }

                    }
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Linking between selected item and similar items are done...');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Please Select Atleast One Item..');", true);
                }
                lstAvalilableItems.ClearSelection();
                lstSelectedSimilarItems.ClearSelection();
            }
           

        }
        catch (Exception ex)
        {
            LogHelper.Logger.ErrorException("Exception: SaveButton : ItemToItemPage: " + ex.Message, ex);
        }
        
    }

    protected void lstAvalilableItems_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (lstAvalilableItems.Items.Count > 0)
            {
                lock (lockTarget)
                {
                    string strSelectedName = lstAvalilableItems.SelectedItem.Text;
                    lstSelectedSimilarItems.DataSource = Apps.lotContext.BasicItems.ToList().Where(c => c.ItemName != strSelectedName);
                    lstSelectedSimilarItems.DataTextField = "ItemName";
                    lstSelectedSimilarItems.DataValueField = "SNo";
                    lstSelectedSimilarItems.DataBind();

                    long val = Convert.ToInt64(lstAvalilableItems.SelectedValue);
                    lock (lockTarget)
                    {
                        LotDBEntity.Models.BasicItem BItem = Apps.lotContext.BasicItems.Where(c => c.SNo == val).FirstOrDefault();
                        lstSelectedSimilarItems.ClearSelection();

                        if (BItem.LstSimilarProducts != null)
                        {
                            foreach (var Item in BItem.LstSimilarProducts)
                            {
                                string SPName = Item.ItemName;

                                if (SPName != BItem.ItemName)
                                {
                                    lstSelectedSimilarItems.Items.FindByText(SPName.ToUpper()).Selected = true;
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Please select one item in the list...');", true);
            }
        }
        catch (Exception ex)
        {
            LogHelper.Logger.ErrorException("Exception: lstAvalilableItems_SelectedIndexChanged  : ItemToItem: " + ex.Message, ex);
        }
        
        
    }
    protected void btnRemoveSimilarPrd_Click(object sender, EventArgs e)
    {
        try
        {
            if (lstAvalilableItems.SelectedValue != "" && lstSelectedSimilarItems.SelectedValue != null)
            {
                List<LotDBEntity.Models.BasicItem> lstSimilarProducts = new List<LotDBEntity.Models.BasicItem>();

                foreach (ListItem li in lstSelectedSimilarItems.Items)
                {
                    if (li.Selected)
                    {
                        lock (lockTarget)
                        {
                            lstSimilarProducts = Apps.lotContext.BasicItems.Where(c => c.ItemName == li.Text).ToList();

                            lock (lockTarget)
                            {
                                long val = Convert.ToInt64(lstAvalilableItems.SelectedValue);
                                LotDBEntity.Models.BasicItem selectedItem = Apps.lotContext.BasicItems.Where(c => c.SNo == val).FirstOrDefault();

                                if (selectedItem.LstSimilarProducts.Count > 0)
                                {
                                    lstSimilarProducts.ForEach(c =>
                                    {
                                        lock (lockTarget)
                                        {

                                            LotDBEntity.Models.SimilarProducts similarProd = Apps.lotContext.SimilarProducts.Where(d => d.BaseItemID == selectedItem.ID).FirstOrDefault();
                                            //LotDBEntity.Models.SimilarProducts similarProd = selectedItem.LstSimilarProducts.Where(d => d.ItemSKU == c.SNo).FirstOrDefault();
                                            if (similarProd != null)
                                            {
                                                Apps.lotContext.SimilarProducts.Remove(similarProd);
                                                Apps.lotContext.SaveChanges();
                                            }
                                        }
                                    });
                                }
                            }
                        }
                    }
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Remove linking between selected item and similar items are done');", true);
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "name", "ErrorMsg();", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Please select atleast one similar item ';", true);
            }
            lstAvalilableItems.ClearSelection();
            lstSelectedSimilarItems.ClearSelection();
        }
        catch (Exception ex)
        {
          LogHelper.Logger.ErrorException("Exception: btnRemoveSimilarPrd_Click  : ItemToItem: " + ex.Message, ex);
        }
        
    }
}