﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMasterPage.master" AutoEventWireup="true" CodeFile="VasData.aspx.cs" Inherits="VasData" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     <style type="text/css">
        .ErrMsg {
            color: #DD4B39;
            font-family: Arial;
            font-size: 10px;
        }

        .pgHeading {
            font-weight: 600;
            font-family: sans-serif;
            color: #92278f;
        }
    </style>
     
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div style="margin: 5% 0 0 0; text-align: left; padding: 2% 0 0% 11.4%;">
        <h2 style="font-size: 28px;">
            <asp:Label ID="lbladd" runat="server" Text="Add Juke Box" class="pgHeading" />
            <asp:HiddenField runat="server" ID="hfGUID" />
        </h2>
    </div>
    <div style="width: 90%; padding: 3% 0 0 23%; margin: -8% 0 0 0;">
        <asp:UpdatePanel ID="upBrandPage" runat="server" UpdateMode="Always">
            <ContentTemplate>

            
        <table style="width: 80%; margin: 6% 0 4% 6%;">
            <tr>
                <td style="width: 15%;">
                    <asp:Label ID="Label1" runat="server" Text="Album Name" /></td>
                <td>
                  <asp:DropDownList ID="ddl5" runat="server" DataTextField="Name" DataValueField="SNo" AppendDataBoundItems="true" Width="25%">
                                <asp:ListItem Value="-1">Select</asp:ListItem>
                            </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td><asp:Label ID="lblmusic" runat="server" Text="Music Director"></asp:Label></td>
                <td>
                     <asp:DropDownList ID="ddl4" runat="server" DataTextField="Name" DataValueField="SNo" AppendDataBoundItems="true"  Width="25%">
                                <asp:ListItem Value="-1">Select</asp:ListItem>
                            </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td><asp:Label ID="lblartist" runat="server" Text="Artist"></asp:Label></td>
                <td>
                     <asp:DropDownList ID="ddlauthor" runat="server" DataTextField="Name" AppendDataBoundItems="true" DataValueField="SNo"  Width="25%">
                                <asp:ListItem Value="-1">Select</asp:ListItem>
                            </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td><asp:Label ID="lblcategory" runat="server" Text="Category"></asp:Label></td>
                <td>
                     <asp:DropDownList ID="ddlcateg" runat="server" DataTextField="Name" DataValueField="SNo" AppendDataBoundItems="true" Width="25%">
                                <asp:ListItem Value="-1">Select</asp:ListItem>
                            </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td><asp:Label ID="lblfiletype" runat="server" Text="FileType"></asp:Label></td>
                <td>
                     <asp:DropDownList ID="ddlfiletype" DataTextField="Name" DataValueField="SNo" AppendDataBoundItems="true" runat="server"  Width="25%">
                                <asp:ListItem Value="-1">Select</asp:ListItem>
                            </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label6" runat="server" Text="Files" /></td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                    <asp:FileUpload ID="FUBrandLogo" runat="server"  AllowMultiple="true" Style="float: left; top: 0px; left: 0px;" Width="35%"  />
                    </ContentTemplate>
                        </asp:UpdatePanel>
                </td>
            </tr>
            </table>
                </ContentTemplate>
        </asp:UpdatePanel>
        <table>
            <tr>
                <td></td>
                <td style="padding: 4% 0 0 36.5%;">
                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClientClick="javascript: return Validate();"  OnClick="btnSubmit_Click" CssClass="lotbtn" />
                    <asp:Button ID="Button1" runat="server" Text="Cancel" OnClick="Button1_Click" CssClass="lotbtn" />                  
                
                </td>
            </tr>
        </table>


    </div>
</asp:Content>

