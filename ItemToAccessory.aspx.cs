﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ItemToAccessory : System.Web.UI.Page
{
    object lockTarget = new object();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LotAdminUser"] == null)
        {
            Response.Redirect("Default.aspx");
        }
        else
        {
            if (!IsPostBack)
            {
                lock (lockTarget)
                {
                    var brands = Apps.lotContext.Brands.ToList();
                    lstAvalilableItems.SelectionMode = ListSelectionMode.Single;
                    ddlbrands.DataSource = brands;
                    ddlbrands.DataTextField = "Name";
                    ddlbrands.DataValueField = "SNo";
                    ddlbrands.DataBind();
                }
            }
        }
    }
    LotDBEntity.Models.Admin admin = new LotDBEntity.Models.Admin();
    List<LotDBEntity.Models.BasicAccessories> lstBasicAccessory;

    protected void btnSelect_Click(object sender, EventArgs e)
    {
        try
        {
            if (ddlbrands.SelectedItem.Text != "Select" && lstAvalableAccessories.SelectedItem != null && lstAvalilableItems.SelectedItem != null)
            {
                if (lstBasicAccessory != null)
                {
                    lstBasicAccessory = new List<LotDBEntity.Models.BasicAccessories>();
                }
                else
                {

                    foreach (ListItem li in lstAvalableAccessories.Items)
                    {

                        if (li.Selected)
                        {
                            if (lstSelectedItems.Items.Contains(li))
                            {
                                lblerror.Visible = false;
                                //lblerror.Text = "Accessory already added to this item";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Your Selected accessories are already in list:Please choose another accessory');", true);
                                //lstAvalableAccessories.ClearSelection();
                                //lstSelectedItems.ClearSelection();
                            }
                            else
                            {
                                lblerror.Text = "";
                            }
                            if (lstSelectedItems.Items.Count >= 4)
                            {
                                lblerror.Visible = false;
                                lblerror.Text = "Please select only 4 Accessories to Item";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Already 4 Items Saved Successfully');", true);
                                //lstAvalableAccessories.ClearSelection();
                                //lstSelectedItems.ClearSelection();
                            }
                            else
                            {
                                lblerror.Text = "";
                            }
                            if (lblerror.Text == "")
                            {
                                if (!lstSelectedItems.Items.Contains(li) && lstSelectedItems.Items.Count < 4)
                                {
                                    lstSelectedItems.Items.Add(li);

                                }
                                if (!lstSelectedItems.Items.Contains(li))
                                {
                                    lstSelectedItems.Items.Add(li);

                                }
                                lock (lockTarget)
                                {
                                    long val = Convert.ToInt64(lstAvalilableItems.SelectedValue);
                                    LotDBEntity.Models.BasicItem basic = Apps.lotContext.BasicItems.Where(c => c.SNo == val).FirstOrDefault();

                                    List<LotDBEntity.Models.BasicAccessories> lstAcccessories = new List<LotDBEntity.Models.BasicAccessories>();

                                    foreach (ListItem item in lstSelectedItems.Items)
                                    {
                                        long val1 = Convert.ToInt64(item.Value);
                                        lock (lockTarget)
                                        {
                                            LotDBEntity.Models.BasicAccessories accessory = Apps.lotContext.BasicAccessories.Where(c => c.SNo == val1).FirstOrDefault();
                                            if (accessory != null)
                                                lstAcccessories.Add(accessory);

                                        }
                                    }
                                    basic.LstAccessories = lstAcccessories;
                                    Apps.lotContext.SaveChanges();

                                }
                                Apps.lotContext.SaveChanges();
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Please select only 4 Accessories to Item');", true);
                            }
                        }

                    }

                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Linking between selected item and selected accessories are done...');", true);


            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Select anyone of the brand || Select anyone of the Item..');", true);
            }

        }
        catch (Exception ex)
        {
            LogHelper.Logger.ErrorException("Exception: btnSelect_Click  : ItemToAccessory: " + ex.Message, ex);
        }
        
    }

    private void LoadSelectedAccessories()
    {
        lstSelectedItems.DataSource = lstBasicAccessory.ToList();
        lstSelectedItems.DataTextField = "Name";
        lstSelectedItems.DataValueField = "SNo";
       lstSelectedItems.DataBind();
    }

    protected void btnUnselect_Click(object sender, EventArgs e)
    {
        try
        {
            if (ddlbrands.SelectedItem.Text != "Select")
            {
                List<LotDBEntity.Models.BasicAccessories> lstSelectedAccessory = new List<LotDBEntity.Models.BasicAccessories>();
                if (lstSelectedItems.SelectedItem != null)
                {
                    foreach (ListItem checkItemSelected in lstSelectedItems.Items)
                    {
                        if (checkItemSelected.Selected)
                        {
                            long selectedAccessorySNo = 0;
                            Int64.TryParse(checkItemSelected.Value, out selectedAccessorySNo);
                            if (selectedAccessorySNo > 0)
                            {
                                lock (lockTarget)
                                {
                                    LotDBEntity.Models.BasicAccessories selectedAccessory = Apps.lotContext.BasicAccessories.Where(c => c.SNo == selectedAccessorySNo).FirstOrDefault();
                                    if (selectedAccessory != null)
                                    {
                                        lstSelectedAccessory.Add(selectedAccessory);
                                    }
                                    Apps.lotContext.SaveChanges();
                                }
                            }
                        }
                    }

                    if (lstSelectedAccessory.Count > 0)
                    {
                        if (lstAvalilableItems.SelectedItem != null)
                        {
                            long selectedItemSNo = 0;
                            Int64.TryParse(lstAvalilableItems.SelectedItem.Value, out selectedItemSNo);
                            if (selectedItemSNo > 0)
                            {
                                lock (lockTarget)
                                {
                                    LotDBEntity.Models.BasicItem selectedItem = Apps.lotContext.BasicItems.Where(c => c.SNo == selectedItemSNo).FirstOrDefault();
                                    foreach (var item in lstSelectedAccessory)
                                    {
                                        selectedItem.LstAccessories.Remove(item);
                                    }
                                    //Parallel.ForEach(lstSelectedAccessory, item =>
                                    //    {
                                    //        selectedItem.LstAccessories.Remove(item);
                                    //    });
                                    Apps.lotContext.SaveChanges();
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Selected item has been removed...');", true);
                                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "name", "ErrorMsg();", true);
                                    //lstAvalilableItems.ClearSelection();
                                    //lstSelectedItems.ClearSelection();
                                    LoadItems();
                                }
                            }

                        }
                    }

                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Select Similar Items to remove..');", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Select brand..');", true);
            }

        }
        catch (Exception ex)
        {
           LogHelper.Logger.ErrorException("Exception: btnUnselect_Click  : ItemToAccessory: " + ex.Message, ex);
        }
     }
     
    protected void ddlbrands_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlbrands.SelectedItem.Text != "Select")
        {
            lock (lockTarget)
            {
                long val = Convert.ToInt64(ddlbrands.SelectedValue);
                LotDBEntity.Models.Brands Brand = Apps.lotContext.Brands.Where(c => c.SNo == val).FirstOrDefault();
                List<LotDBEntity.Models.BasicItem> lstItem = Brand.LstItems.ToList();
                if (lstItem != null)
                {
                    if (lstItem.Count > 0)
                    {
                        lstSelectedItems.Items.Clear();
                        lstAvalilableItems.DataSource = lstItem;
                        lstAvalilableItems.DataTextField = "ItemName";
                        lstAvalilableItems.DataValueField = "SNo";
                        lstAvalilableItems.DataBind();
                    }
                    else
                    {
                        lstSelectedItems.ClearSelection();
                        lstAvalableAccessories.ClearSelection();
                        lstSelectedItems.Items.Clear();

                    }
                }
                else
                {
                     
                }
            }
            lock (lockTarget)
            {
                var accessory = Apps.lotContext.BasicAccessories.ToList();
                lstAvalableAccessories.DataSource = accessory;
                lstAvalableAccessories.DataTextField = "Name";
                lstAvalableAccessories.DataValueField = "SNo";
                lstAvalableAccessories.DataBind();
            }
        }
        else
        {
            lstAvalilableItems.Items.Clear();
            lstAvalableAccessories.Items.Clear();
            lstSelectedItems.Items.Clear();
        }
    }
    protected void lstAvalilableItems_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblerror.Text = "";
        LoadItems();
    }

    private void LoadItems()
    {
        try
        {
            if (lstAvalilableItems.Items.Count > 0)
            {
                LogHelper.Logger.Info(" starts AvailableItems more than zero");
                lock (lockTarget)
                {
                    long val = Convert.ToInt64(lstAvalilableItems.SelectedValue);
                    LotDBEntity.Models.BasicItem BItem = Apps.lotContext.BasicItems.Where(c => c.SNo == val).FirstOrDefault();
                   
                        if (BItem.LstAccessories!=null)
                        {
                            LogHelper.Logger.Info("starts BItem.LstAccessories not null");
                            lstAvalableAccessories.ClearSelection();
                            lstSelectedItems.ClearSelection();

                            lock (lockTarget)
                            {
                                List<LotDBEntity.Models.BasicAccessories> lstBasicAccess = BItem.LstAccessories.ToList();
                                lstSelectedItems.DataSource = lstBasicAccess;
                                lstSelectedItems.DataTextField = "Name";
                                lstSelectedItems.DataValueField = "SNo";
                                lstSelectedItems.DataBind();
                            }
                            LogHelper.Logger.Info("ends BItem.LstAccessories not null");
                        }
                        else
                        {
                            lstAvalableAccessories.ClearSelection();
                            lstSelectedItems.ClearSelection();
                            lstSelectedItems.Items.Clear();
                        }
                    }
                   
                    LogHelper.Logger.Info(" ends AvailableItems more than zero");
                
            }
           
        }
        catch (Exception ex)
        {
            LogHelper.Logger.ErrorException("Exception: LoadItems() : ItemToAccessory: " + ex.Message, ex);
        }
        
    }
}