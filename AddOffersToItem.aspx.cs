﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AddOffersToItem : System.Web.UI.Page
{
    //LotDBEntity.Models.Models model;
    LotDBEntity.Models.BasicItem item;
    object lockTarget = new object();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LotAdminUser"] == null)
        {
            Response.Redirect("Default.aspx");
        }
        else
        {
            if (!IsPostBack)
            {
                LogHelper.Logger.Info("starts PageLoad in AddOffersToItem Page");
                lock (lockTarget)
                {
                    var brands = Apps.lotContext.Brands.ToList();
                    ddlbrand.DataSource = brands;
                    ddlbrand.DataTextField = "Name";
                    ddlbrand.DataValueField = "SNo";
                    ddlbrand.DataBind();
                }
                LogHelper.Logger.Info("starts PageLoad in AddOffersToItem Page");
            }
        }
    }
    protected void ddlbrand_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlbrand.SelectedItem.Text != "Select")
        {
            ddlcateg.Items.Clear();
            ddlmodel.Items.Clear();
            long val = Convert.ToInt64(ddlbrand.SelectedValue);
            lock (lockTarget)
            {
                LotDBEntity.Models.Brands brand = Apps.lotContext.Brands.Where(c => c.SNo == val).FirstOrDefault();
                if (brand.LstCategory == null)
                {

                    ddlcateg.Items.Clear();
                    ddlcateg.Items.Add("Select");
                    ddlmodel.Items.Clear();
                    ddlmodel.Items.Add("Select");

                    lstItems.DataSource = null;
                    lstItems.DataBind();

                }
                else
                {
                    ddlcateg.Items.Add("Select");
                    ddlmodel.Items.Add("Select");
                    lstItems.Items.Clear();
                    lstItems.DataSource = null;
                    lstItems.DataBind();
                    ddlcateg.DataSource = brand.LstCategory;
                    ddlcateg.DataTextField = "Name";
                    ddlcateg.DataValueField = "SNo";
                    ddlcateg.DataBind();
                    chkFinancing.Checked = false;
                    chkOffers.Checked = false;
                    chlexchange.Checked = false;
                    chkEmi.Checked = false;
                    txtoffers.Text = "";
                    txtfinancing.Text = "";
                    txtexchange.Text = "";
                    txtemi.Text = "";
                }
            }
        }
        else
        {
            ddlcateg.Items.Clear();
            ddlcateg.Items.Add("Select");
            ddlmodel.Items.Clear();
            ddlmodel.Items.Add("Select");
            lstItems.Items.Clear();
            lstItems.DataSource = null;
            lstItems.DataBind();
            clearAll();
        }
    }
    protected void ddlcateg_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlcateg.SelectedItem.Text != "Select")
        {
            ddlmodel.Items.Clear();
            long val = Convert.ToInt64(ddlcateg.SelectedValue);
            lock (lockTarget)
            {
                LotDBEntity.Models.Category Categ = Apps.lotContext.Categories.Where(c => c.SNo == val).FirstOrDefault();
                if (Categ.LstModels == null)
                {
                    ddlmodel.Items.Clear();
                    ddlmodel.Items.Add("Select");
                }
                else
                {
                    ddlmodel.Items.Clear();
                    ddlmodel.Items.Add("Select");
                    ddlmodel.DataSource = Categ.LstModels;
                    ddlmodel.DataTextField = "Name";
                    ddlmodel.DataValueField = "SNo";
                    ddlmodel.DataBind();
                }
            }
        }
        else
        {
            ddlmodel.Items.Clear();
            ddlmodel.Items.Add("Select");
            clearAll();
        }
    }
    protected void ddlmodel_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlmodel.SelectedItem.Text != "Select")
        {
            long val = Convert.ToInt64(ddlmodel.SelectedValue);
            lock (lockTarget)
            {
                LotDBEntity.Models.Models Model = Apps.lotContext.Models.Where(c => c.SNo == val).FirstOrDefault();
                if (Model.LstBasicItems != null)
                {
                    lstItems.DataSource = Model.LstBasicItems;
                    lstItems.DataTextField = "ItemName";
                    lstItems.DataValueField = "SNo";
                    lstItems.DataBind();
                    txtemi.Text = "";
                    txtexchange.Text = "";
                    txtfinancing.Text = "";
                    txtoffers.Text = "";
                }
            }
        }
        else
        {
            clearAll();
        }

    }
    protected void lstItems_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadItemOffer();
    }

    private void LoadItemOffer()
    {
        if (lstItems.SelectedValue != "" )// !string.IsNullOrEmpty(lstItems.SelectedItem.Text))// != null)
        {
            lock (lockTarget)
            {
                 long val = Convert.ToInt64(lstItems.SelectedValue);
                LotDBEntity.Models.BasicItem SelectedItem = Apps.lotContext.BasicItems.Where(c => c.SNo == val).FirstOrDefault();
                lblId.Text = SelectedItem.SNo.ToString();
                if (SelectedItem.OffersAvailable == true)
                {
                    chkOffers.Checked = true;
                    txtoffers.Text = SelectedItem.Offer;
                }
                else
                {
                    chkOffers.Checked = false;
                    txtoffers.Text = "";
                }
                if (SelectedItem.EMIAvailable == true)
                {
                    chkEmi.Checked = true;
                    txtemi.Text = SelectedItem.EMIMonths;
                }
                else
                {
                    chkEmi.Checked = false;
                    txtemi.Text = "";
                }
                if (SelectedItem.FinancingOptionsAvailable == true)
                {
                    chkFinancing.Checked = true;
                    txtfinancing.Text = SelectedItem.FinancerDetails;
                }
                else
                {
                    chkFinancing.Checked = false;
                    txtfinancing.Text = "";
                }
                if (SelectedItem.ExchangeAvailable == true)
                {
                    chlexchange.Checked = true;
                    txtexchange.Text = SelectedItem.ExchangeDetails;
                }
                else
                {
                    chlexchange.Checked = false;
                    txtexchange.Text = "";
                }
                if (SelectedItem.BuyOneGetOneAvailable == true)
                {
                    chkBuy1Get1.Checked = true;
                    txtBuy1Get1.Text = SelectedItem.BuyOneGetOneDescription;
                }
                else
                {
                    chkBuy1Get1.Checked = false;
                    txtBuy1Get1.Text = "";
                }
                if (SelectedItem.BuyOneGetTwoAvailable == true)
                {
                    chkBuy1Get2.Checked = true;
                    txtBuy1Get2.Text = SelectedItem.BuyOneGetTwoDescription;
                }
                else
                {
                    chkBuy1Get2.Checked = false;
                    txtBuy1Get2.Text = "";
                }
                if (SelectedItem.Flat50BuyBackAvailable == true)
                {
                    chkFlat50.Checked = true;
                    txtFlat50.Text = SelectedItem.Flat50BuyBackDescription;
                }
                else
                {
                    chkFlat50.Checked = false;
                    txtFlat50.Text = "";
                }
                if (SelectedItem.PowerBankFreeAvailable == true)
                {
                    chkPowerBank.Checked = true;
                    txtPowerBank.Text = SelectedItem.PowerBankFreeDescription;
                }
                else
                {
                    chkPowerBank.Checked = false;
                    txtPowerBank.Text = "";
                }
                if (SelectedItem.TrailPeriodAvailable == true)
                {
                    chkTrail.Checked = true;
                    txtTrail.Text = SelectedItem.TrailPeriodDescription;
                }
                else
                {
                    chkTrail.Checked = false;
                    txtTrail.Text = "";
                }

            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Please select any one of the item');", true);
        }
    }

    private void clearAll()
    {
        chkFinancing.Checked = false;
        chkOffers.Checked = false;
        chlexchange.Checked = false;
        chkEmi.Checked = false;
        chkBuy1Get1.Checked = false;
        chkBuy1Get2.Checked = false;
        chkFlat50.Checked = false;
        chkPowerBank.Checked = false;
        chkTrail.Checked = false;
        txtoffers.Text = "";
        txtfinancing.Text = "";
        txtexchange.Text = "";
        txtemi.Text = "";
        txtBuy1Get1.Text = "";
        txtBuy1Get2.Text = "";
        txtFlat50.Text = "";
        txtPowerBank.Text = "";
        txtTrail.Text = "";
        lblId.Text = "0";
        lstItems.Items.Clear();
        ddlbrand.ClearSelection();
        ddlbrand.Items.FindByText("Select").Selected = true;
        ddlcateg.ClearSelection();
        ddlcateg.Items.FindByText("Select").Selected = true;
        ddlmodel.ClearSelection();
        ddlmodel.Items.FindByText("Select").Selected = true;

    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (lblId.Text != "0" && ddlmodel.SelectedItem.Text != "Select" && ddlcateg.SelectedItem.Text != "Select" && ddlbrand.SelectedItem.Text != "Select")
        {
            long val = Convert.ToInt64(lstItems.SelectedValue);
            lock (lockTarget)
            {
                item = Apps.lotContext.BasicItems.Where(c => c.SNo == val).FirstOrDefault();
                if (chkOffers.Checked == true)
                {
                    item.OffersAvailable = true;
                }
                else
                {
                    item.OffersAvailable = false;
                }

                if (chlexchange.Checked == true)
                {
                    item.ExchangeAvailable = true;
                }
                else
                {
                    item.ExchangeAvailable = false;

                }
                if (chkEmi.Checked == true)
                {
                    item.EMIAvailable = true;
                }
                else
                {
                    item.EMIAvailable = false;
                }
                if (chkFinancing.Checked == true)
                {
                    item.FinancingOptionsAvailable = true;
                }
                else
                {
                    item.FinancingOptionsAvailable = false;
                }
                if (chkBuy1Get1.Checked == true)
                {
                    item.BuyOneGetOneAvailable = true;
                }
                else
                {
                    item.BuyOneGetOneAvailable = false;
                }
                if (chkBuy1Get2.Checked == true)
                {
                    item.BuyOneGetTwoAvailable = true;
                }
                else
                {
                    item.BuyOneGetTwoAvailable = false;
                }
                if (chkFlat50.Checked == true)
                {
                    item.Flat50BuyBackAvailable = true;
                }
                else
                {
                    item.Flat50BuyBackAvailable = false;
                }
                if (chkPowerBank.Checked == true)
                {
                    item.PowerBankFreeAvailable = true;
                }
                else
                {
                    item.PowerBankFreeAvailable = false;
                }
                if (chkTrail.Checked == true)
                {
                    item.TrailPeriodAvailable = true;
                }
                else
                {
                    item.TrailPeriodAvailable = false;
                }
                item.Offer = txtoffers.Text.ToUpper();
                item.ExchangeDetails = txtexchange.Text.ToUpper();
                item.EMIMonths = txtemi.Text.ToUpper();
                item.FinancerDetails = txtfinancing.Text.ToUpper();
                item.BuyOneGetOneDescription = txtBuy1Get1.Text.ToUpper();
                item.BuyOneGetTwoDescription = txtBuy1Get2.Text.ToUpper();
                item.Flat50BuyBackDescription = txtFlat50.Text.ToUpper();
                item.PowerBankFreeDescription = txtPowerBank.Text.ToUpper();
                item.TrailPeriodDescription = txtTrail.Text.ToUpper();
                item.LastUpdatedTime = DateTime.Now;
                Apps.lotContext.SaveChanges();
                lstItems.ClearSelection();
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "name", "UpdateMsg();", true);
                clearAll();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Offers added to item successfully ');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Please select manadatory fields');", true);
            
        }
       

    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        if (ddlbrand.SelectedItem.Text != "Select" && ddlcateg.SelectedItem.Text != "Select" && ddlmodel.SelectedItem.Text != "Select")
        {
            LoadItemOffer();
        }
        else
        {
            chkFinancing.Checked = false;
            chkOffers.Checked = false;
            chlexchange.Checked = false;
            chkEmi.Checked = false;
            txtoffers.Text = "";
            txtfinancing.Text = "";
            txtexchange.Text = "";
            txtemi.Text = "";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Brand / Category / Model should not be empty');", true);
        }
    }
}