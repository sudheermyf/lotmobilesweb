﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMasterPage.master" AutoEventWireup="true" CodeFile="ViewBrands.aspx.cs" Inherits="ViewBrands" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .ErrMsg {
            color: #DD4B39;
            font-family: Arial;
            font-size: 10px;
        }
        .pgHeading {
            font-weight: 600;
            font-family: sans-serif;
            color: #92278f;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div style="margin: 5% 0 0 0; text-align: left; padding: 2% 0 0% 11.5%;" />
    <h2 style="font-size: 28px;">
        <asp:Label ID="lbladd" runat="server" Text="View Brands" class="pgHeading" /></h2>

    <asp:UpdatePanel ID="uPanel" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <div style="width: 100%; padding: 4% 3% 0 18%; margin: -3% 0 0 0;">

                <div style="width: 40%; float: left;">
                    <asp:Label ID="lblDisplay"  runat="server" CssClass="ErrMsg" Text="Select any brand to view details"></asp:Label>
                    <asp:ListBox ID="lstBrand" runat="server" Height="400px" AutoPostBack="true"
                        Width="90%" DataTextField="Name" DataValueField="SNo"
                        OnSelectedIndexChanged="lstBrand_SelectedIndexChanged" />
                </div>
                <div style="width: 60%; float: left;">
                    <table>
                        <tr>
                            <td style="width: 20%;">
                                <asp:Label ID="lblId" runat="server" Visible="false"></asp:Label></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label6" runat="server" Text="Logo"></asp:Label></td>
                            <td>
                                <asp:Image ID="imgbrandlogo" runat="server" Height="70" Width="170" AlternateText="img.jpg" /></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label1" runat="server" Text=" Name"></asp:Label></td>
                            <td>
                                <asp:Label ID="lblbrandname" runat="server"></asp:Label></td>
                        </tr>

                        <tr>
                            <td>
                                <asp:Label ID="Label4" runat="server" Text="Description"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtdesc" runat="server" ReadOnly="true" style="width: 75%;"></asp:TextBox></td>
                        </tr>
                    </table>
                </div>


            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
   <div style="margin:21% 0 0 67%;">
                <asp:Button ID="btnEdit" runat="server" Text="Edit" CssClass="lotbtn" OnClick="btnEdit_Click" />
            
                <asp:Button ID="brnDelete" runat="server" Text="Delete" CssClass="lotbtn" OnClick="brnDelete_Click" />
        </div>



</asp:Content>

