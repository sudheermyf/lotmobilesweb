﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMasterPage.master" AutoEventWireup="true" CodeFile="VASTabs.aspx.cs" Inherits="VASTabs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script src="css/tabpanel/tabs.js"></script>
    <link href="css/tabpanel/style.css" rel="stylesheet" />
    <script src="css/tabpanel/tabs_old.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <div style="margin: 5% 0 0 0; text-align: left; padding: 2% 0 0% 11.4%;">
        <h2 style="font-size: 28px;">
            <asp:Label ID="lbladd" runat="server" Text="Add JukeBox Tab Fields" class="pgHeading" />
        </h2>
    </div>
    <div style="width: 100%; padding: 3% 0 0 23%; margin: -8% 0 0 0;">
        <asp:UpdatePanel ID="upBrandPage" runat="server" UpdateMode="Always">
            <ContentTemplate>
            
        <table style="width: 100%; margin: 6% 0 4% 6%;">
            <tr>
                <td style="width: 15%;">
                    <asp:Label ID="lblfilename" runat="server" Text="File Type Name"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtfilename" runat="server" Width="45%"></asp:TextBox>          
                </td>
                <td>
                    <asp:Button ID="btnFileSave" runat="server" Text="Save" OnClick="btnFileSave_Click" CssClass="lotbtn" />
                    <asp:Button ID="btnFileCancel" runat="server" Text="Cancel" OnClick="btnFileCancel_Click"  CssClass="lotbtn"/>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblCategName" runat="server" Text="Category Name"></asp:Label></td>
                <td>
                      <asp:TextBox  ID="txtCategName" runat="server" Width="45%"></asp:TextBox></td>
                <td>
                   <asp:Button ID="btnCategSave" runat="server" Text="Save" OnClick="btnCategSave_Click" CssClass="lotbtn" />
                    <asp:Button ID="btnCategCancel" runat="server" Text="Cancel"  OnClick="btnCategCancel_Click" CssClass="lotbtn" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblArtistName" runat="server" Text="Artist Name"></asp:Label>
                </td>
                <td>
                     <asp:TextBox ID="txtArtistName" runat="server" Width="45%"></asp:TextBox>
                </td>
                <td>
                    <asp:Button ID="btnArtistSave" runat="server" Text="Save" OnClick="btnArtistSave_Click" CssClass="lotbtn" />
                    <asp:Button ID="btnArtistCancel" runat="server" Text="Cancel"   OnClick="btnArtistCancel_Click" CssClass="lotbtn"/>
                </td>
            </tr>
            <tr>
                <td> <asp:Label ID="Label1" runat="server" Text="Music Director"></asp:Label></td>
                <td> <asp:TextBox ID="TextBox1" runat="server" Width="45%"></asp:TextBox></td>
                <td> <asp:Button ID="btnCateg4Save" runat="server" Text="Save"  OnClick="btnCateg4Save_Click"  CssClass="lotbtn"/>
                     <asp:Button ID="btnCateg4Cancel" runat="server" Text="Cancel" OnClick="btnCateg4Cancel_Click"  CssClass="lotbtn" /></td>
            </tr>
                <td><asp:Label ID="Label2" runat="server" Text="Album"></asp:Label></td>
                <td><asp:TextBox ID="TextBox2" runat="server" Width="45%"></asp:TextBox></td>
                <td><asp:Button ID="btnCateg5Save" runat="server" Text="Save" OnClick="btnCateg5Save_Click"  CssClass="lotbtn" />
                <asp:Button ID="btnCateg5Cancel" runat="server" Text="Cancel"  OnClick="btnCateg5Cancel_Click"  CssClass="lotbtn"/></td>
            </table>
                </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

