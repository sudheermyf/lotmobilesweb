﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ViewBrands : System.Web.UI.Page
{
    public LotDBEntity.Models.Brands brand;
    List<LotDBEntity.Models.Brands> SelectBrand = new List<LotDBEntity.Models.Brands>();
    object lockTarget = new object();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LotAdminUser"] == null)
        {
            Response.Redirect("Default.aspx");
        }
        else
        {
            if (!IsPostBack)
            {
                LogHelper.Logger.Info("Starts PageLoad in ViewBrands");
                lock (lockTarget)
                {
                    SelectBrand = Apps.lotContext.Brands.ToList();
                    lstBrand.DataSource = SelectBrand;
                    lstBrand.DataBind();
                    lstBrand.Focus();
                    txtdesc.ReadOnly = true;
                }
                LogHelper.Logger.Info("Ends PageLoad in ViewBrands");
            }
        }
    }
    
    protected void lstBrand_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (lstBrand.Items.Count > 0)
        {
            lock (lockTarget)
            {
                long val = Convert.ToInt64(lstBrand.SelectedValue);
                LotDBEntity.Models.Brands SelectedBrand = Apps.lotContext.Brands.Where(c => c.SNo == val).FirstOrDefault();
                imgbrandlogo.ImageUrl = "~/BrandImages/" + SelectedBrand.ImageName;
                lblId.Text = SelectedBrand.SNo.ToString();
                lblbrandname.Text = SelectedBrand.Name;
                txtdesc.Text = SelectedBrand.Description;
                lstBrand.Focus();
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Please Select One Item in the Brand List ');", true);
        }

    }
    protected void btnEdit_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(lblId.Text))
        {
            Response.Redirect("AddBrand.aspx?SNo=" + lblId.Text + "");
        }
    }
    protected void brnDelete_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(lblId.Text))
        {
            lock (lockTarget)
            {
                long Id = Convert.ToInt64(lblId.Text);
                brand = Apps.lotContext.Brands.Where(c => c.SNo == Id).FirstOrDefault();
                if (brand.LstCategory != null && brand.LstCategory.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Brands Have different Categories Please Delete Categories ');", true);
                }
                else
                {
                    Apps.lotContext.Brands.Remove(brand);
                    Apps.lotContext.SaveChanges();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Brand Deleted Successfully ');window.location='ViewBrands.aspx'", true);
                }
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Please Select One Item in the List To Delete ');window.location='ViewBrands.aspx'", true);
        }
    }
}