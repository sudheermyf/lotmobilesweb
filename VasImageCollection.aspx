﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMasterPage.master" AutoEventWireup="true" CodeFile="VasImageCollection.aspx.cs" Inherits="VasImageCollection" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     <style type="text/css">
        .ErrMsg {
            color: #DD4B39;
            font-family: Arial;
            font-size: 10px;
        }
        .pgHeading {
            font-weight: 600;
            font-family: sans-serif;
            color: #92278f;
        }
    </style>
     <script type="text/javascript">

         function Validate() {
             var Errfpoffer = document.getElementById('<%=Errfpoffer.ClientID %>');
             var myLabel = document.getElementById('<%=myLabel.ClientID %>');
             var fpoffer = document.getElementById('<%=fpoffer.ClientID %>').value;

             if (fpoffer.length < 4) {
                 Errfpoffer.innerHTML = "Select Image";
             }

             var uploadcontrol2 = document.getElementById('<%=fpoffer.ClientID%>').value;
             //Regular Expression for fileupload control.
             var reg = /^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+(.jpeg|.JPEG|.gif|.GIF|.png|.PNG|.jpg|.JPG)$/;
             if (uploadcontrol2.length > 0) {
                 //Checks with the control value.
                 if (reg.test(uploadcontrol2)) {
                     Errfpoffer.innerHTML = "";
                 }
                 else {
                     //If the condition not satisfied shows error message.
                     Errfpoffer.innerHTML = "Only png, jpg and gif files are allowed!";
                 }
             }


             if (txttitle.value.length > 4 && Errfpoffer.innerHTML == "") {
                 return true;
             }
             return false;
         }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <div style="margin: 5% 0 0 0; text-align: left; padding: 2% 0 0% 11.5%;">
        <h2 style="font-size: 28px;">
            <asp:HiddenField runat="server" ID="hfGUID" />
            <asp:Label ID="lbladd" runat="server" Text="Value Added Services" class="pgHeading" /></h2>
    </div>
    
   <div style=" width: 100%;
padding: 3% 0 0 23%;margin: -4% 0 0 0;"> 
       <asp:UpdatePanel ID="upcategory" runat="server">
           <ContentTemplate>

    <table style="width:70%; margin: 6% 0 4% 6%;">
          <tr>
            <td>
                <asp:Label ID="Label2" runat="server" Text="Add Image"></asp:Label></td>
            <td>
            <asp:FileUpload ID="fpoffer" runat="server"  AllowMultiple="true" onblur="javascript: return Validate()"/>    
                <asp:Label ID="myLabel" runat="server" ForeColor="Red"></asp:Label>
                    <asp:Label ID="Errfpoffer" runat="server" CssClass="ErrMsg"/>
            </td>
        </tr> 
        </table>
       <table style="width:70%; margin: 6% 0 4% 6%;">
        <tr>
            <td>
                <asp:DataList ID="DlstItemGallery" runat="server" DataKeyField="SNo" RepeatDirection="Horizontal" RepeatColumns="4" >
                           <ItemTemplate>
                               <asp:Image Style="margin-left:30px" Height="20%" ID="imgItem" ImageUrl='<%# Bind("ImageName", "~/OfferImage/{0}") %>' runat="server" />
                           </ItemTemplate>
                       </asp:DataList>
            </td>
        </tr>   
         </table>
           </ContentTemplate>
          
       </asp:UpdatePanel>
       <table style="margin: 6% 0 0 0%;">
        <tr>
            <td></td>
            <td style="padding:0 0 0 10.2%;">
                <asp:Button ID="btnSubmit" runat="server" style="margin-left:10%;" Text="Submit" OnClientClick="javascript: return Validate()"  CssClass="lotbtn"  OnClick="btnSubmit_Click" />
                 <asp:Button ID="btnCancel" runat="server" Text="Cancel"  CssClass="lotbtn"  OnClick="btnCancel_Click"  />
            </td>
        </tr>
  </table>
       
       </div>
</asp:Content>

