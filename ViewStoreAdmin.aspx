﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMasterPage.master" AutoEventWireup="true" CodeFile="ViewStoreAdmin.aspx.cs" Inherits="ViewStoreAdmin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div style="margin: 5% 0 0 0; text-align: left; padding: 2% 0 0% 10%;">
        <h2 style="font-size: 28px;">Store-Admin</h2>
    </div>
      <asp:UpdatePanel ID="uPanel" runat="server" UpdateMode="Always">
                <ContentTemplate>
    <div style="width: 100%; padding: 3% 0 0 23%; margin: -4% 0 0 0;">

        <div style="width:40%;float:left;">
          


             <asp:ListBox ID="lstStoreAdmin" runat="server"  Height="400px" AutoPostBack="true"
                 Width="90%" DataTextField="StoreName" DataValueField="SNo"
                 OnSelectedIndexChanged="lstStoreAdmin_SelectedIndexChanged"/>
                    
            
        </div>
        
        <div style="width:60%;float:left;">
            <table>
                 
        <tr>
            <td>
               <asp:Image ID="imgStore" runat="server" Height="50" Width="50" AlternateText="img.jpg" /></td>
            <td>
                
                <asp:Label ID="lblStoreName" runat="server" Font-Size="Larger"></asp:Label>
            </td>
        </tr>  
                  <tr>
            <td>
                <asp:Label ID="Label7" runat="server" Text="Store ID"></asp:Label></td>
            <td>
                 <asp:Label ID="lblStoreID" runat="server"></asp:Label></td>
        </tr>  
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text="User Name"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtusername" runat="server" ReadOnly="true"></asp:TextBox>
                </td>
        </tr>
     
       
        <tr>
            <td>
                <asp:Label ID="Label5" runat="server" Text="Store Address"></asp:Label></td>
            <td>
               <asp:TextBox ID="txtStoreAddress" runat="server" ReadOnly="true"></asp:TextBox></td>
        </tr> 
      
        <tr>
            <td>
                <asp:Label ID="Label8" runat="server" Text="Store Manager Name"></asp:Label></td>
            <td>
                 <asp:TextBox ID="txtmanager" runat="server" ReadOnly="true"></asp:TextBox></td>
        </tr>  
        <tr>
            <td>
                <asp:Label ID="Label9" runat="server" Text="Number Of Employees"></asp:Label></td>
            <td>
                 <asp:TextBox ID="txtemp" runat="server" ReadOnly="true"></asp:TextBox></td>
        </tr>  
        <tr>
            <td>
                <asp:Label ID="Label10" runat="server" Text="Store Contact Number"></asp:Label></td>
            <td>
                 <asp:TextBox ID="txtcontact" runat="server" ReadOnly="true"></asp:TextBox></td>
        </tr>  
               
        <tr>
             <td> <asp:Button ID="btnEdit" runat="server" Text="Edit"  CssClass="lotbtn" OnClick="btnEdit_Click"/></td>
           <td>
                <asp:Button ID="btnDelet" runat="server" Text="Delete" CssClass="lotbtn" OnClick="btnDelet_Click"/>
            </td>
        </tr>
    </table>
        </div>
    </div>
    </ContentTemplate>
      
            </asp:UpdatePanel>
</asp:Content>

