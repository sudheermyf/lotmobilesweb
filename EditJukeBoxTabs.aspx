﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMasterPage.master" AutoEventWireup="true" CodeFile="EditJukeBoxTabs.aspx.cs" Inherits="EditJukeBoxTabs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table>
      <tr>
          <td style="vertical-align: middle;padding: 6.5% 0 7.5% 26px;text-align: right;">
              <asp:RadioButtonList  ID="radtab" runat="server" Style=" margin-left:200px; text-align:right"  Width="89%" OnSelectedIndexChanged="radtab_SelectedIndexChanged" RepeatDirection="Horizontal" RepeatColumns="5" AutoPostBack="true" AppendDataBoundItems="true">
                  <asp:ListItem>FileType</asp:ListItem>
                  <asp:ListItem>Category</asp:ListItem>
                  <asp:ListItem>Artist</asp:ListItem>
                  <asp:ListItem>Music Director</asp:ListItem>
                  <asp:ListItem>Album</asp:ListItem>
              </asp:RadioButtonList>
          </td>
      </tr>
  </table>
    <table>
        <tr>
            <td>
                <asp:Label ID="lblerr" runat="server" Font-Size="Medium" ForeColor="LightGreen" ></asp:Label>
            <asp:UpdatePanel runat="server" UpdateMode="Conditional" >
                <ContentTemplate>
                    <asp:GridView ID="GridView1" runat="server" Width="700px" style="margin-left:300px" CellSpacing="10"  
                AutoGenerateColumns="False" Font-Names="Arial"
                Font-Size="12pt"   OnRowCommand="GridView1_RowCommand"
                 
                 OnRowEditing="GridView1_RowEditing"
                OnRowUpdating="GridView1_RowUpdating" OnRowCancelingEdit="GridView1_RowCancelingEdit" CellPadding="3" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" GridLines="Horizontal">
                        <AlternatingRowStyle BackColor="#F7F7F7" />
                <Columns>
                    <asp:TemplateField ItemStyle-Width="60px" HeaderText="SNo">
                        <ItemTemplate>
                            <asp:Label ID="lblSno" runat="server" Text='<%#Eval("SNo") %>' ></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="60px" />
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-Width="60px" HeaderText="Name" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:Label ID="lblName" runat="server" Width="300px"
                                Text='<%# Eval("Name")%>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtname" runat="server"  Width="300px" Text='<%#Eval("Name") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemStyle Width="60px" />
                    </asp:TemplateField>
                    <asp:CommandField ShowEditButton="True"/>
                    <asp:TemplateField HeaderText="Delete">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkDelete" runat="server" Text="Delete"  CommandArgument='<%#Bind("SNo") %>'
                                  OnClientClick="return confirm('Are you sure you want to Delete the record')" CommandName="Delete"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>


                </Columns>
                        <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                        <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
                        <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
                        <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
                        <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
                        <SortedAscendingCellStyle BackColor="#F4F4FD" />
                        <SortedAscendingHeaderStyle BackColor="#5A4C9D" />
                        <SortedDescendingCellStyle BackColor="#D8D8F0" />
                        <SortedDescendingHeaderStyle BackColor="#3E3277" />
            </asp:GridView>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID = "GridView1" />
                </Triggers>
            </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>

