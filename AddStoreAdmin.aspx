﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMasterPage.master" AutoEventWireup="true" CodeFile="AddStoreAdmin.aspx.cs" Inherits="AddStoreAdmin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <div style="margin:5% 0 0 0;text-align:left;padding: 2% 0 0% 10%;"><h2 style="font-size:28px;">Add Store-Admin</h2></div>
   <div style=" width: 100%;
padding: 3% 0 0 23%;margin: -4% 0 0 0;">
      <table style="width:70%; margin: 6% 0 10% 6%;">
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text="User Name"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtUserName" runat="server" placeholder="User Name" required="required" width="50%" AutoPostBack="true" OnTextChanged="txtUserName_TextChanged"></asp:TextBox>
                <asp:Label ID="txtexist" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label2" runat="server" Text="Password"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtPassword" runat="server" placeholder="Password" TextMode="Password" required="required" width="50%"></asp:TextBox></td>
        </tr>  
        <tr>
            <td>
                <asp:Label ID="Label3" runat="server" Text="Confirm Password"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtConfirmPwd" runat="server" placeholder="Confirm Password" TextMode="Password" required="required"></asp:TextBox></td>
        </tr>   
        <tr>
            <td>
                <asp:Label ID="Label4" runat="server" Text="Store Name"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtStrName" runat="server" placeholder="Store Name" required="required" width="50%"></asp:TextBox></td>
        </tr>  
        <tr>
            <td>
                <asp:Label ID="Label5" runat="server" Text="Store Address"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtStrAddress" runat="server" placeholder="Store Address" required="required" width="50%"></asp:TextBox></td>
        </tr>  
        <tr>
            <td>
                <asp:Label ID="Label6" runat="server" Text="Store Image"></asp:Label></td>
            <td>
                <asp:FileUpload ID="FUStoreImage" runat="server" Style="float:left;" /></td>
        </tr>  
        <tr>
            <td>
                <asp:Label ID="Label7" runat="server" Text="Store ID"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtStrId" runat="server" placeholder="Store ID" required="required" width="50%"></asp:TextBox></td>
        </tr>  
        <tr>
            <td>
                <asp:Label ID="Label8" runat="server" Text="Store Manager Name"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtStrManagerName" runat="server" placeholder="Manager Name" required="required" width="50%"></asp:TextBox></td>
        </tr>  
        <tr>
            <td>
                <asp:Label ID="Label9" runat="server" Text="Number Of Employees"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtNoOfEmployee" runat="server" placeholder="Number of Employee" required="required" width="50%"></asp:TextBox></td>
        </tr>  
        <tr>
            <td>
                <asp:Label ID="Label10" runat="server" Text="Store Contact Number"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtStrContactNo" runat="server" placeholder="Store Contact Number" required="required" width="50%"></asp:TextBox></td>
        </tr>  
               
        <tr>
             <td>&nbsp;</td>
           <td>
                <asp:Button ID="btnSubmit" runat="server" Text="Submit"  OnClick="btnSubmit_Click" CssClass="lotbtn"/>
                   <asp:Button ID="Button1" runat="server" Text="Cancel"  OnClick="btnSubmit_Click" CssClass="lotbtn"/>
            </td>
        </tr>
    </table>
       </div>
</asp:Content>

