﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AddBrand : System.Web.UI.Page
{
    object lockTarget = new object();
    protected void Page_Load(object sender, EventArgs e)
    {
       
        if (Session["LotAdminUser"] == null)
        {
            Response.Redirect("Default.aspx");
        }
        else
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["SNo"] != null)
                {
                    btnSubmit.Text = "Update";
                    Button1.Text = "Reset";
                    lbladd.Text = "Edit Brand";
                    GetUpdatedData();
                 }
            }
        }
    }   

    private void GetUpdatedData()
    {
        LogHelper.Logger.Info("Starts GetUpdatedData() in AddBrand");
        lock (lockTarget)
        {
            long Id = Convert.ToInt64(Request.QueryString["SNo"].ToString());
            LotDBEntity.Models.Brands UpdateBrands = Apps.lotContext.Brands.Where(c => c.SNo == Id).FirstOrDefault();
            txtBrandName.Text = UpdateBrands.Name;
            txtBrandDesc.Text = UpdateBrands.Description;
            lblImgLocation.Text = UpdateBrands.ImageName;
            txtexist.Text="";
        }
        LogHelper.Logger.Info("Ends GetUpdatedData() in AddBrand");
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (btnSubmit.Text == "Submit")
        {
            if (txtexist.Text != "Already Exists")
            {
                try
                {
                    LogHelper.Logger.Info("Starts when Submit Clicks in AddBrands");
                    LotDBEntity.Models.Brands brands = new LotDBEntity.Models.Brands();
                    lock (lockTarget)
                    {
                        long id = 0;
                        if (Apps.lotContext.Brands.ToList().Count > 0)
                            id = Apps.lotContext.Brands.Max(c => c.ID);
                        if (id > 0)
                        {
                            brands.ID = id + 1;
                        }
                        else
                        {
                            brands.ID = 1;
                        }
                    }
                    if (FUBrandLogo.HasFile)
                    {
                        LogHelper.Logger.Info("Starts when BrandLogo HasFile in Submit");
                        string extension = Path.GetExtension(FUBrandLogo.PostedFile.FileName);
                        string fileName = Path.GetFileName(FUBrandLogo.PostedFile.FileName);
                        string strBrandImages = new Random().Next(1000, 100000).ToString() + System.IO.Path.GetExtension(fileName);
                        brands.ImageName = strBrandImages;
                        string folder = Server.MapPath("~/BrandImages/");
                        string strFolder = "BrandImages";
                        Directory.CreateDirectory(folder);
                        FUBrandLogo.PostedFile.SaveAs(Path.Combine(folder, strBrandImages));
                        brands.WEBImageLocation = Path.Combine(folder, strBrandImages);
                        brands.ImageLocation = "pack://siteoforigin:,,,/" + strFolder + "/" + strBrandImages;
                        LogHelper.Logger.Info("Ends when BrandLogo HasFile in Submit");
                    }
                    brands.Name = txtBrandName.Text.ToUpper().Trim();
                    brands.Description = txtBrandDesc.Text.ToUpper();
                    brands.LastUpdatedTime = DateTime.Now;
                    LogHelper.Logger.Info("Starts Add SaveChanges in Brands");
                    Apps.lotContext.Brands.Add(brands);
                    Apps.lotContext.SaveChanges();
                    LogHelper.Logger.Info("Ends Add SaveChanges in Brands");
                    LogHelper.Logger.Info("Ends when Submit Clicks in AddBrands");
                }
                catch (Exception ex)
                {
                    LogHelper.Logger.ErrorException("Exception: Submit : AddBrand: " + ex.Message, ex);
                }
                

                //ScriptManager.RegisterStartupScript(this, this.GetType(), "name", "SuccessMsg();", true);
                ClearAll();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Brand saved successfully ');", true);
                
            }
        }
        else if (btnSubmit.Text == "Update")
        {
            if (!string.IsNullOrEmpty(txtBrandName.Text) && txtexist.Text != "Already Exists")
            {
                LotDBEntity.Models.Brands brands = new LotDBEntity.Models.Brands();
                long Id = Convert.ToInt64(Request.QueryString["SNo"]);
                lock(lockTarget)
                {
                LotDBEntity.Models.Brands lstBrand = Apps.lotContext.Brands.Where(c => c.SNo == Id).FirstOrDefault();
                if (lstBrand != null)
                {
                    lstBrand.Name = txtBrandName.Text.ToUpper().Trim();
                    if (lstBrand.Name != txtBrandName.Text.Trim())
                    {
                        checkBrandName();
                    }
                    if (txtexist.Text == "Already Exists")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Item name already exists');", true);
                    }
                    else
                    {
                        btnSubmit.Enabled = false;
                        if (FUBrandLogo.HasFile)
                        {
                            LogHelper.Logger.Info("Starts when BrandLogo HasFile in Update");
                            string extension = Path.GetExtension(FUBrandLogo.PostedFile.FileName);
                            string fileName = Path.GetFileName(FUBrandLogo.PostedFile.FileName);
                            string strBrandImages = new Random().Next(1000, 100000).ToString() + System.IO.Path.GetExtension(fileName);
                            lstBrand.ImageName = strBrandImages;
                            string folder = Server.MapPath("~/BrandImages/");
                            string strFolder = "BrandImages";
                            Directory.CreateDirectory(folder);

                            FUBrandLogo.PostedFile.SaveAs(Path.Combine(folder, strBrandImages));
                            lstBrand.WEBImageLocation = Path.Combine(folder, strBrandImages);
                            lstBrand.ImageLocation = "pack://siteoforigin:,,,/" + strFolder + "/" + strBrandImages;
                            LogHelper.Logger.Info("Ends when BrandLogo HasFile in Update");
                        }
                        else
                        {
                            lstBrand.WEBImageLocation = lstBrand.WEBImageLocation;
                            lstBrand.ImageLocation = lstBrand.ImageLocation;
                        }
                        lstBrand.Description = txtBrandDesc.Text.ToUpper();
                        lstBrand.LastUpdatedTime = DateTime.Now;
                        LogHelper.Logger.Info("Save Changes Starts in Brands");
                        Apps.lotContext.SaveChanges();
                        LogHelper.Logger.Info("Save Changes Ends in Brands");
                        ClearAll();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Brand updated successfully ');window.location='ViewBrands.aspx'", true);
                        btnSubmit.Enabled = true;
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "name", "UpdateMsg();", true);
                    } //ClearAll();
                   } 
                }
            }
        }        
    }

    private void ClearAll()
    {
        txtBrandDesc.Text = "";
        txtBrandName.Text = "";
        FUBrandLogo.Dispose();
        txtexist.Text = "";
        ErrBrandName.Text = "";
        myLabel.Text = "";
    }
    protected void txtBrandName_TextChanged(object sender, EventArgs e)
    {
        LogHelper.Logger.Info("txtBrandName_TextChanged Started");
        checkBrandName();
        LogHelper.Logger.Info("txtBrandName_TextChanged Ended");
    }

    private void checkBrandName()
    {
        string strVal = txtBrandName.Text.ToUpper().Trim();
        List<LotDBEntity.Models.Brands> lstBrand = new List<LotDBEntity.Models.Brands>();
        lstBrand = Apps.lotContext.Brands.Where(c => c.Name == strVal).ToList();
        if (lstBrand.Count > 0)
        {
            txtexist.Text = "Already Exists";
            txtexist.Focus();
        }
        else
        {
            txtexist.Text = "";
            txtBrandDesc.Focus();
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        if (Button1.Text == "Cancel")
        {
            ClearAll();
        }
        else
        {
            GetUpdatedData();
        }
    }
}