﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ViewItem : System.Web.UI.Page
{
    private object lockTarget = new object();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LotAdminUser"] == null)
        {
            Response.Redirect("Default.aspx");
        }
        else
        {
            if (!IsPostBack)
            {
                LogHelper.Logger.Info("Starts PageLoad in ViewItems");
                lock (lockTarget)
                {
                    LoadData();
                }
                LogHelper.Logger.Info("Ends PageLoad in ViewItems");
            }
        }
    }

    private void LoadData()
    {
        var brands = Apps.lotContext.Brands.ToList();
        ddlbrand.DataSource = brands;
        ddlbrand.DataTextField = "Name";
        ddlbrand.DataValueField = "SNo";
        ddlbrand.DataBind();
    }

    protected void ddlbrand_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlbrand.SelectedItem.Text != "Select")
        {
            lock (lockTarget)
            {
                ddlcateg.Items.Clear();
                ddlmodel.Items.Clear();
                long val = Convert.ToInt64(ddlbrand.SelectedValue);
                LotDBEntity.Models.Brands brand = Apps.lotContext.Brands.Where(c => c.SNo == val).FirstOrDefault();
                if (brand.LstCategory == null)
                {
                    ddlcateg.Items.Clear();
                    ddlcateg.Items.Add("Select");
                    ddlmodel.Items.Clear();
                    ddlmodel.Items.Add("Select");
                    lstItems.Items.Clear();
                }
                else
                {
                    ddlcateg.Items.Add("Select");
                    ddlmodel.Items.Add("Select");
                    ddlcateg.DataSource = brand.LstCategory;
                    ddlcateg.DataTextField = "Name";
                    ddlcateg.DataValueField = "SNo";
                    ddlcateg.DataBind();
                    lstItems.Items.Clear();
                }
            }
        }
        else
        {
            ddlcateg.Items.Clear();
            ddlcateg.Items.Add("Select");
            ddlmodel.Items.Clear();
            ddlmodel.Items.Add("Select");
            lstItems.Items.Clear();
            clearAll();
        }
    }

    protected void ddlcateg_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlcateg.SelectedItem.Text != "Select")
        {
            lock (lockTarget)
            {
                ddlmodel.Items.Clear();
                long val = Convert.ToInt64(ddlcateg.SelectedValue);
                LotDBEntity.Models.Category Categ = Apps.lotContext.Categories.Where(c => c.SNo == val).FirstOrDefault();
                if (Categ.LstModels == null)
                {
                    ddlmodel.Items.Clear();
                    ddlmodel.Items.Add("Select");
                }
                else
                {
                    ddlmodel.Items.Clear();
                    ddlmodel.Items.Add("Select");
                    ddlmodel.DataSource = Categ.LstModels;
                    ddlmodel.DataTextField = "Name";
                    ddlmodel.DataValueField = "SNo";
                    ddlmodel.DataBind();
                }
            }
        }
        else
        {
            ddlmodel.Items.Clear();
            ddlmodel.Items.Add("Select");
            lstItems.Items.Clear();
            clearAll();
        }
    }

    protected void ddlmodel_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlmodel.SelectedItem.Text != "Select")
        {
            lock (lockTarget)
            {
                long val = Convert.ToInt64(ddlmodel.SelectedValue);
                LotDBEntity.Models.Models Model = Apps.lotContext.Models.Where(c => c.SNo == val).FirstOrDefault();
                if (Model.LstBasicItems != null)
                {
                    lstItems.DataSource = Model.LstBasicItems;
                    lstItems.DataTextField = "ItemName";
                    lstItems.DataValueField = "SNo";
                    lstItems.DataBind();
                }
            }
        }
        else
        {
            lstItems.Items.Clear();
            clearAll();
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(lblId.Text))
        {
            Response.Redirect("AddItem.aspx?SNo=" + lblId.Text + "");
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Please Select One Item To Edit...');", true);
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        if (lstItems.Items.Count > 0)
        {
            lock (lockTarget)
            {
                LotDBEntity.Models.BasicItem BItem = Apps.lotContext.BasicItems.Where(c => c.ItemName == txtitemname.Text).FirstOrDefault();
                lock (lockTarget)
                {
                    List<LotDBEntity.Models.ImageCollection> lstImgCollection = Apps.lotContext.ImageCollection.Where(c => c.basicItem.SNo == BItem.SNo).ToList();
                    List<LotDBEntity.Models.SimilarProducts> lstSimilarProd = Apps.lotContext.SimilarProducts.Where(c => c.ItemSKU == BItem.SNo).ToList();
                    if (BItem != null && BItem.LstSimilarProducts == null && BItem.LstAccessories == null && lstSimilarProd == null)
                    {
                        foreach (var item in lstImgCollection)
                        {
                            Apps.lotContext.ImageCollection.Remove(item);
                            Apps.lotContext.SaveChanges();
                        }
                        Apps.lotContext.BasicItems.Remove(BItem);
                        Apps.lotContext.SaveChanges();
                        //clearAll();
                        Reload();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Item Deleted Successfully...');window.location='ViewItem.aspx';", true);
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "name", "ErrorMsg();", true);
                    }
                    else if (BItem != null && BItem.LstSimilarProducts.Count == 0 && BItem.LstAccessories.Count == 0 && lstSimilarProd.Count == 0)
                    {
                        foreach (var item in lstImgCollection)
                        {
                            Apps.lotContext.ImageCollection.Remove(item);
                            Apps.lotContext.SaveChanges();
                        }
                        Apps.lotContext.BasicItems.Remove(BItem);
                        Apps.lotContext.SaveChanges();
                        //clearAll();
                        Reload();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Item Deleted Successfully...');window.location='ViewItem.aspx';", true);
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "name", "ErrorMsg();", true);
                    }
                    else
                    {
                        clearAll();
                        lstItems.ClearSelection();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Please remove the item from similar products list');", true);
                    }
                }
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Please Select One Item in the List.');", true);
        }
    }

    private void clearAll()
    {
        dataItems.DataSource = null;
        dataItems.DataBind();
        lblId.Text = "";
        txtitemname.Text = "";
        txtd1.Text = "0";
        txtd2.Text = "0";
        txtd3.Text = "0";
        txtOfferDesc.Text = "";
        txtcolor.Text = "";
        txtweight.Text = "0";
        txtinternalmem.Text = "";
        txtexternalmem.Text = "";
        txtpcamera.Text = "";
        txtscamera.Text = "";
        txtcamspec.Text = "";
        txtptype.Text = "";
        txtpspeed.Text = "";
        txtram.Text = "";
        txtbatterytype.Text = "";
        txttalktime.Text = "";
        txtStandBy.Text = "";
        txtdisplaytype.Text = "";
        txtdisplaysize.Text = "";
        txtprice.Text = "0";
        txtosname.Text = "";
        txtosversion.Text = "";
        lblsim1.Text = "";
        txtsimtype.Text = "";
        txtverdict.Text = "";
        txtpros.Text = "";
        txtcons.Text = "";
        lblwifi1.Text = "";
        lbledge1.Text = "";
        lblgprs1.Text = "";
        lbl3g1.Text = "";
        lblbluetooth1.Text = "";
        lblflash1.Text = "";
        lbltouch1.Text = "";
        lblradmusic.Text = "";
        lblQwerty1.Text = "";
    }

    private void Reload()
    {
        if (ddlmodel.SelectedItem != null)
        {
            lock (lockTarget)
            {
                long val = Convert.ToInt64(ddlmodel.SelectedValue);
                LotDBEntity.Models.Models Model = Apps.lotContext.Models.Where(c => c.SNo == val).FirstOrDefault();
                if (Model.LstBasicItems != null)
                {
                    lstItems.DataSource = Model.LstBasicItems;
                    lstItems.DataTextField = "ItemName";
                    lstItems.DataValueField = "SNo";
                    lstItems.DataBind();
                }
            }
        }
    }

    protected void lstItems_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (lstItems.SelectedItem.Text != "Select")
        {
            long val = Convert.ToInt64(lstItems.SelectedValue);
            lock (lockTarget)
            {
                List<LotDBEntity.Models.ImageCollection> ImgCollect = Apps.lotContext.ImageCollection.Where(c => c.basicItem.SNo == val).ToList();
                dataItems.DataSource = null;
                dataItems.DataBind();
                dataItems.DataSource = ImgCollect.ToList();
                dataItems.DataBind();
            }
            List<LotDBEntity.Models.BasicItem> lstSelectedItem = new List<LotDBEntity.Models.BasicItem>();
            foreach (ListItem checkItemSelected in lstItems.Items)
            {
                if (checkItemSelected.Selected)
                {
                    long selectedItemSNo = 0;
                    Int64.TryParse(checkItemSelected.Value, out selectedItemSNo);
                    if (selectedItemSNo > 0)
                    {
                        lock (lockTarget)
                        {
                            LotDBEntity.Models.BasicItem selectedAccessory = Apps.lotContext.BasicItems.Where(c => c.SNo == selectedItemSNo).FirstOrDefault();
                            if (selectedAccessory != null)
                            {
                                lstSelectedItem.Add(selectedAccessory);
                            }
                        }
                    }
                }
            }
            if (lstSelectedItem.Count > 0)
            {
                long selectedItemSNo = 0;
                Int64.TryParse(lstItems.SelectedItem.Value, out selectedItemSNo);
                if (selectedItemSNo > 0)
                {
                    lock (lockTarget)
                    {
                        LotDBEntity.Models.BasicItem selectedItem = Apps.lotContext.BasicItems.Where(c => c.SNo == selectedItemSNo).FirstOrDefault();
                        ddlCompareImages.DataSource = selectedItem.LstImgsAndVideos.Where(c => c.IsVideo == false).ToList();
                        ddlCompareImages.DataBind();
                        //Parallel.ForEach(lstSelectedAccessory, item =>
                        //{
                        //    selectedItem.LstAccessories.Remove(item);
                        //});
                        Apps.lotContext.SaveChanges();
                        //LoadItems();
                    }
                    {
                    }
                }
                // ddlCompareImages.DataSource=
                lock (lockTarget)
                {
                    LotDBEntity.Models.BasicItem SelectedItem = Apps.lotContext.BasicItems.Where(c => c.SNo == val).FirstOrDefault();
                    lblId.Text = SelectedItem.SNo.ToString();
                    txtitemname.Text = SelectedItem.ItemName;
                    txtd1.Text = SelectedItem.Length.ToString();
                    txtd2.Text = SelectedItem.Breadth.ToString();
                    txtd3.Text = SelectedItem.Height.ToString();
                    txtcolor.Text = SelectedItem.ItemColor;
                    txtweight.Text = SelectedItem.Weight.ToString();
                    txtinternalmem.Text = SelectedItem.InternalStorage;
                    txtexternalmem.Text = SelectedItem.ExternalStorage;
                    txtpcamera.Text = SelectedItem.PrimaryCamera;
                    txtscamera.Text = SelectedItem.SecondaryCamera;
                    txtcamspec.Text = SelectedItem.CameraSpecifications;
                    txtptype.Text = SelectedItem.ProcessorType;
                    txtpspeed.Text = SelectedItem.ProcessorSpeed;
                    txtram.Text = SelectedItem.RAM;
                    txtbatterytype.Text = SelectedItem.BatteryType;
                    txttalktime.Text = SelectedItem.TalkTime;
                    txtStandBy.Text = SelectedItem.StandBy;
                    txtdisplaytype.Text = SelectedItem.DisplayType;
                    txtdisplaysize.Text = SelectedItem.DisplaySize;
                    txtprice.Text = SelectedItem.DefaultPrice.ToString();
                    txtosname.Text = SelectedItem.OSName;
                    txtosversion.Text = SelectedItem.OSVersion;
                    lblsim1.Text = SelectedItem.SIM;
                    txtsimtype.Text = SelectedItem.SIMType;
                    txtverdict.Text = SelectedItem.Verdict;
                    txtpros.Text = SelectedItem.Pros;
                    imgOfferDesc.ImageUrl = "~/OfferImage/" + SelectedItem.ExtraOfferImageName;
                    txtOfferDesc.Text = SelectedItem.ExtraOfferDesc;
                    txtcons.Text = SelectedItem.Cons;
                    if (SelectedItem.Wifi == true)
                    {
                        lblwifi1.Text = "Yes";
                    }
                    else
                    {
                        lblwifi1.Text = "No";
                    }
                    if (SelectedItem.EDGE == true)
                    {
                        lbledge1.Text = "Yes";
                    }
                    else
                    {
                        lbledge1.Text = "No";
                    }
                    if (SelectedItem.GPRS == true)
                    {
                        lblgprs1.Text = "Yes";
                    }
                    else
                    {
                        lblgprs1.Text = "No";
                    }
                    if (SelectedItem.Supports3G == true)
                    {
                        lbl3g1.Text = "Yes";
                    }
                    else
                    {
                        lbl3g1.Text = "No";
                    }
                    if (SelectedItem.Bluetooth == true)
                    {
                        lblbluetooth1.Text = "Yes";
                    }
                    else
                    {
                        lblbluetooth1.Text = "No";
                    }
                    if (SelectedItem.FlashAvailable == true)
                    {
                        lblflash1.Text = "Yes";
                    }
                    else
                    {
                        lblflash1.Text = "No";
                    }
                    if (SelectedItem.TouchScreen == true)
                    {
                        lbltouch1.Text = "Yes";
                    }
                    else
                    {
                        lbltouch1.Text = "No";
                    }
                    if (SelectedItem.MusicPlayer == true)
                    {
                        lblradmusic.Text = "Yes";
                    }
                    else
                    {
                        lblradmusic.Text = "No";
                    }
                    if (SelectedItem.Quert == true)
                    {
                        lblQwerty1.Text = "Yes";
                    }
                    else
                    {
                        lblQwerty1.Text = "No";
                    }
                }
            }
        }
    }
}