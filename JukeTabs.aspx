﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMasterPage.master" AutoEventWireup="true" CodeFile="JukeTabs.aspx.cs" Inherits="JukeTabs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div style=" width:40%;visibility: visible;margin: 70px;">
        <asp:TabContainer ID="TabContainer1" runat="server" CssClass="fancy fancy-green"  OnDemand="true" OnActiveTabChanged="TabContainer1_ActiveTabChanged"  AutoPostBack="true" style="display:block;visibility:visible;"  ActiveTabIndex="0">
            <asp:TabPanel ID="TabPanelFile" runat="server" HeaderText="File Type" Enabled="true" OnDemandMode="Once">
                <HeaderTemplate>File Type</HeaderTemplate>
                <ContentTemplate>
                    <asp:Panel ID="pnlFileType" runat="server">
                        <asp:Label ID="lblfilename" runat="server" Text="File Type Name"></asp:Label>
                        <asp:TextBox ID="txtfilename" runat="server"></asp:TextBox>
                        <asp:Button ID="btnFileSave" runat="server" Text="Save" OnClick="btnFileSave_Click" />
                        <asp:Button ID="btnFileCancel" runat="server" Text="Cancel" OnClick="btnFileCancel_Click" />
                    </asp:Panel>
                </ContentTemplate>
            </asp:TabPanel>
                <asp:TabPanel ID="TabPanelCateg" runat="server" HeaderText="Category"   OnDemandMode="Once" Enabled="true">
                <HeaderTemplate>Category</HeaderTemplate>
                <ContentTemplate>
                    <asp:Panel ID="pnlCategory" runat="server">
                        <asp:Label ID="lblCategName" runat="server" Text="Category Name"></asp:Label>
                        <asp:TextBox ID="txtCategName" runat="server"></asp:TextBox>
                        <asp:Button ID="btnCategSave" runat="server" Text="Save" OnClick="btnCategSave_Click" />
                        <asp:Button ID="btnCategCancel" runat="server" Text="Cancel" OnClick="btnCategCancel_Click" />
                    </asp:Panel>
                </ContentTemplate>
            </asp:TabPanel>
             <asp:TabPanel ID="TabPanel1" runat="server" HeaderText="Artist Name">
                <HeaderTemplate>Artist</HeaderTemplate>
                <ContentTemplate>
                    <asp:Panel ID="pnlArtist" runat="server">
                        <asp:Label ID="lblArtistName" runat="server" Text="Artist Name"></asp:Label>
                        <asp:TextBox ID="txtArtistName" runat="server"></asp:TextBox>
                        <asp:Button ID="btnArtistSave" runat="server" Text="Save" OnClick="btnArtistSave_Click" />
                        <asp:Button ID="btnArtistCancel" runat="server" Text="Cancel" OnClick="btnArtistCancel_Click" />
                    </asp:Panel>
                </ContentTemplate>
            </asp:TabPanel>
               <asp:TabPanel ID="TabPanel2" runat="server"  HeaderText="Category 4">
                <HeaderTemplate>Category4</HeaderTemplate>
                <ContentTemplate>
                    <asp:Panel ID="Panel3" runat="server">
                        <asp:Label ID="Label1" runat="server" Text="Category 4"></asp:Label>
                        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                        <asp:Button ID="btnCateg4Save" runat="server" Text="Save" OnClick="btnCateg4Save_Click" />
                        <asp:Button ID="btnCateg4Cancel" runat="server" Text="Cancel" OnClick="btnCateg4Cancel_Click" />
                    </asp:Panel>
                </ContentTemplate>
            </asp:TabPanel>
              <asp:TabPanel ID="TabPanel3" runat="server" HeaderText="Category 5">
                <HeaderTemplate>Category5</HeaderTemplate>
                <ContentTemplate>
                    <asp:Panel ID="Panel4" runat="server">
                        <asp:Label ID="Label2" runat="server" Text="Category 5"></asp:Label>
                        <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                        <asp:Button ID="btnCateg5Save" runat="server" Text="Save" OnClick="btnCateg5Save_Click" />
                        <asp:Button ID="btnCateg5Cancel" runat="server" Text="Cancel" OnClick="btnCateg5Cancel_Click" />
                    </asp:Panel>
                </ContentTemplate>
            </asp:TabPanel>
        </asp:TabContainer>
        </div>
</asp:Content>

