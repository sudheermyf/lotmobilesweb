﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class EditJukeBoxTabs : System.Web.UI.Page
{
    object lockTarget = new object();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            lblerr.Text = "";
        }
    }
    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        lblerr.Text="";
        GridView1.EditIndex = e.NewEditIndex;
        //GridView1.DataBind();
    }
    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        lblerr.Text = "";
        string Id = ((Label)GridView1.Rows[e.RowIndex].FindControl("lblSno")).Text;
        long uid = Convert.ToInt64(Id);
        if (radtab.SelectedItem.Text == "FileType")
        {
            lock (lockTarget)
            {
                LotDBEntity.Models.VASTabOne vasone = Apps.lotContext.VASTabOne.Where(c => c.SNo == uid).FirstOrDefault();
                vasone.Name = ((TextBox)GridView1.Rows[e.RowIndex].FindControl("txtname")).Text;
                Apps.lotContext.SaveChanges();
            }
            lock(lockTarget)
            {
                GridView1.DataSource = Apps.lotContext.VASTabOne.ToList();
                GridView1.DataBind();
            }
        }
        else if (radtab.SelectedItem.Text == "Category")
        {
            lock (lockTarget)
            {
                LotDBEntity.Models.VASTabTwo vastwo = Apps.lotContext.VASTabTwo.Where(c => c.SNo == uid).FirstOrDefault();
                vastwo.Name = ((TextBox)GridView1.Rows[e.RowIndex].FindControl("txtname")).Text;
                Apps.lotContext.SaveChanges();
            }
            lock (lockTarget)
            {
                GridView1.DataSource = Apps.lotContext.VASTabTwo.ToList();
                GridView1.DataBind();
            }
        }
        else if (radtab.SelectedItem.Text == "Artist")
        {
            lock (lockTarget)
            {
                LotDBEntity.Models.VASTabThree vasthree = Apps.lotContext.VASTabThree.Where(c => c.SNo == uid).FirstOrDefault();
                vasthree.Name = ((TextBox)GridView1.Rows[e.RowIndex].FindControl("txtname")).Text;
                Apps.lotContext.SaveChanges();
            }
            lock (lockTarget)
            {
                GridView1.DataSource = Apps.lotContext.VASTabThree.ToList();
                GridView1.DataBind();
            }
        }
        else if (radtab.SelectedItem.Text == "Music Director")
        {
            lock (lockTarget)
            {
                LotDBEntity.Models.VASTabFour vasfour = Apps.lotContext.VASTabFour.Where(c => c.SNo == uid).FirstOrDefault();
                vasfour.Name = ((TextBox)GridView1.Rows[e.RowIndex].FindControl("txtname")).Text;
                Apps.lotContext.SaveChanges();
            }
            lock (lockTarget)
            {
                GridView1.DataSource = Apps.lotContext.VASTabFour.ToList();
                GridView1.DataBind();
            }
        }

        else if (radtab.SelectedItem.Text == "Album")
        {
            lock (lockTarget)
            {
                LotDBEntity.Models.VASTabFive vasfive = Apps.lotContext.VASTabFive.Where(c => c.SNo == uid).FirstOrDefault();
                vasfive.Name = ((TextBox)GridView1.Rows[e.RowIndex].FindControl("txtname")).Text;
                Apps.lotContext.SaveChanges();
            }
            lock (lockTarget)
            {
                GridView1.DataSource = Apps.lotContext.VASTabFive.ToList();
                GridView1.DataBind();
            }
        }
     
        GridView1.EditIndex = -1;
        GridView1.DataBind();
    }
    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        lblerr.Text = "";
        GridView1.EditIndex = -1;
        GridView1.DataBind();
    }
    protected void radtab_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblerr.Text = "";
        if (radtab.SelectedItem.Text == "FileType")
        {
            lock (lockTarget)
            {
                GridView1.DataSource = Apps.lotContext.VASTabOne.ToList();
                GridView1.DataBind();
            }
        }
        else if (radtab.SelectedItem.Text == "Category")
        {
            lock (lockTarget)
            {
                GridView1.DataSource = Apps.lotContext.VASTabTwo.ToList();
                GridView1.DataBind();
            }
        }
        else if (radtab.SelectedItem.Text == "Artist")
        {
            lock (lockTarget)
            {
                GridView1.DataSource = Apps.lotContext.VASTabThree.ToList();
                GridView1.DataBind();
            }
        }
        else if (radtab.SelectedItem.Text == "Music Director")
        {
            lock (lockTarget)
            {
                GridView1.DataSource = Apps.lotContext.VASTabFour.ToList();
                GridView1.DataBind();
            }
        }
        else if (radtab.SelectedItem.Text == "Album")
        {
            lock (lockTarget)
            {
                GridView1.DataSource = Apps.lotContext.VASTabFive.ToList();
                GridView1.DataBind();
            }
        }
    }
    
    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        long id =Convert.ToInt64((e.CommandArgument.ToString()));
        if (e.CommandName == "Delete")
        {
            if (radtab.SelectedItem.Text == "FileType")
            {
                if (Apps.lotContext.ValueAddedServicesData.Where(c => c.VASTabOne.ID == id).ToList().Count == 0)
                {
                    lock (lockTarget)
                    {
                        LotDBEntity.Models.VASTabOne vasone = Apps.lotContext.VASTabOne.Where(c => c.SNo == id).FirstOrDefault();
                        if (vasone != null)
                        {
                            Apps.lotContext.VASTabOne.Remove(vasone);
                            Apps.lotContext.SaveChanges();
                        }
                    }
                    lock (lockTarget)
                    {
                        GridView1.DataSource = Apps.lotContext.VASTabOne.ToList();
                        GridView1.DataBind();
                    }
                    lblerr.Text = "";
                    lblerr.Text = "FileType  Delete Successfully";
                    Response.Redirect("EditJukeBoxTabs.aspx");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('FileType  Delete Successfully ');", true);
                }
                else
                {
                    lblerr.Text = "";
                    lblerr.Text = "FileType Contains in JukeBox Please Delete that record";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('FileType Contains in JukeBox Please Delete that record ');", true);
                }
            }
            else if (radtab.SelectedItem.Text == "Category")
            {
                if (Apps.lotContext.ValueAddedServicesData.Where(c => c.VASTabTwo.ID == id).ToList().Count == 0)
                {
                    lock (lockTarget)
                    {
                        LotDBEntity.Models.VASTabTwo vastwo = Apps.lotContext.VASTabTwo.Where(c => c.SNo == id).FirstOrDefault();
                        if (vastwo != null)
                        {
                            Apps.lotContext.VASTabTwo.Remove(vastwo);
                            Apps.lotContext.SaveChanges();
                        }
                    }
                    lock (lockTarget)
                    {
                        GridView1.DataSource = Apps.lotContext.VASTabTwo.ToList();
                        GridView1.DataBind();
                    }
                    lblerr.Text = "";
                    lblerr.Text = "Category Name  Delete Successfully";
                    Response.Redirect("EditJukeBoxTabs.aspx");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Category Name  Delete Successfully ');", true);
                }
                else
                {
                    lblerr.Text = "";
                    lblerr.Text = "Category Name Contains in JukeBox Please Delete that record";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Category Name Contains in JukeBox Please Delete that record ');", true);
                }
            }
            else if (radtab.SelectedItem.Text == "Artist")
            {
                
                if (Apps.lotContext.ValueAddedServicesData.Where(c => c.VASTabThree.ID == id).ToList().Count == 0)
                {
                    lock (lockTarget)
                    {
                        LotDBEntity.Models.VASTabThree vasthree = Apps.lotContext.VASTabThree.Where(c => c.SNo== id).FirstOrDefault();
                        if (vasthree != null)
                        {
                            Apps.lotContext.VASTabThree.Remove(vasthree);
                            Apps.lotContext.SaveChanges();
                        }
                    }
                    lock (lockTarget)
                    {
                        GridView1.DataSource = Apps.lotContext.VASTabThree.ToList();
                        GridView1.DataBind();
                    }
                    lblerr.Text = "";
                    lblerr.Text = "Artist Name  Delete Successfully";
                    Response.Redirect("EditJukeBoxTabs.aspx");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Artist Name  Delete Successfully ');", true);
                }
                else
                {
                    lblerr.Text = "";
                    lblerr.Text = "Artist Name Contains in JukeBox Please Delete that record";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Artist Name Contains in JukeBox Please Delete that record ');", true);
                }
            }
            else if (radtab.SelectedItem.Text == "Music Director")
            {
                if (Apps.lotContext.ValueAddedServicesData.Where(c => c.VASTabFour.ID == id).ToList().Count == 0)
                {
                    lock (lockTarget)
                    {
                        LotDBEntity.Models.VASTabFour vasfour = Apps.lotContext.VASTabFour.Where(c => c.SNo == id).FirstOrDefault();
                        if (vasfour != null)
                        {
                            Apps.lotContext.VASTabFour.Remove(vasfour);
                            Apps.lotContext.SaveChanges();
                        }
                    }
                    lock (lockTarget)
                    {
                        GridView1.DataSource = Apps.lotContext.VASTabFour.ToList();
                        GridView1.DataBind();
                    }
                    lblerr.Text = "";
                    lblerr.Text = "Music Director Name  Delete Successfully";
                    Response.Redirect("EditJukeBoxTabs.aspx");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Genre Name  Delete Successfully ');", true);
                }
                else
                {
                    lblerr.Text = "";
                    lblerr.Text = "Genre Name Contains in JukeBox Please Delete that record";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Genre Name Contains in JukeBox Please Delete that record ');", true);
                }
            }
            else if (radtab.SelectedItem.Text == "Album")
            {
                if (Apps.lotContext.ValueAddedServicesData.Where(c => c.VASTabFive.ID == id).ToList().Count == 0)
                {
                    lock (lockTarget)
                    {
                        LotDBEntity.Models.VASTabFive vasfive = Apps.lotContext.VASTabFive.Where(c => c.SNo == id).FirstOrDefault();
                        if (vasfive != null)
                        {
                            Apps.lotContext.VASTabFive.Remove(vasfive);
                            Apps.lotContext.SaveChanges();
                        }
                    }
                    lock (lockTarget)
                    {
                        GridView1.DataSource = Apps.lotContext.VASTabFive.ToList();
                        GridView1.DataBind();
                       
                    }
                    lblerr.Text = "";
                    lblerr.Text = "Era  Name  Delete Successfully";
                    Response.Redirect("EditJukeBoxTabs.aspx");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Era  Name  Delete Successfully ');", true);
                }
                else
                {
                    lblerr.Text = "";
                    lblerr.Text = "Album Name Contains in JukeBox Please Delete that record";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Era Name Contains in JukeBox Please Delete that record ');", true);
                }
            }
          
        }
    }
}