﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AddOffersPage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        LotDBEntity.Models.OffersOfTheDay Offers = new LotDBEntity.Models.OffersOfTheDay();
        List<LotDBEntity.Models.OffersOfTheDay> OffersOfTheDay = Apps.lotContext.OffersOfTheDay.ToList();
        if (OffersOfTheDay.Count == 0)
        {
            Offers.ImageName = txttitle.Text.ToUpper().Trim();
            if (fpoffer.HasFile)
            {
                string extension = Path.GetExtension(fpoffer.PostedFile.FileName);
                string fileName = Path.GetFileName(fpoffer.PostedFile.FileName);
                string strOffersPage = new Random().Next(1000, 100000).ToString() + System.IO.Path.GetExtension(fileName);
                string folder = Server.MapPath("~/OfferImage/");
                string strFolder = "OfferImage";
                Directory.CreateDirectory(folder);
                fpoffer.PostedFile.SaveAs(Path.Combine(folder, strOffersPage));

                Offers.ImageLocation = "pack://siteoforigin:,,,/" + strFolder + "/" + strOffersPage;

                Offers.LastUpdatedTime = DateTime.Now;
                Apps.lotContext.OffersOfTheDay.Add(Offers);
                Apps.lotContext.SaveChanges();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Offer Added Successfully ');window.location='AddOffersPage.aspx'", true);
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "name", "SuccessMsg();", true);
                txttitle.Text = "";                
            }
        }
        else if (OffersOfTheDay.Count > 0)
        {

            LotDBEntity.Models.OffersOfTheDay OffersUpdate = Apps.lotContext.OffersOfTheDay.Where(c => c.SNo == 1).FirstOrDefault();

            OffersUpdate.ImageName = txttitle.Text.ToUpper().Trim();
            if (fpoffer.HasFile)
            {
                string extension = Path.GetExtension(fpoffer.PostedFile.FileName);
                string fileName = Path.GetFileName(fpoffer.PostedFile.FileName);
                string strOffersPage = new Random().Next(1000, 100000).ToString() + System.IO.Path.GetExtension(fileName);
                string folder = Server.MapPath("~/OfferImage/");
                string strFolder = "OfferImage";
                Directory.CreateDirectory(folder);
                fpoffer.PostedFile.SaveAs(Path.Combine(folder, strOffersPage));

                OffersUpdate.ImageLocation = "pack://siteoforigin:,,,/" + strFolder + "/" + strOffersPage;

                OffersUpdate.LastUpdatedTime = DateTime.Now;
                Apps.lotContext.SaveChanges();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Offer Updated Successfully ');window.location='AddOffersPage.aspx'", true);
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "name", "UpdateMsg();", true);
                txttitle.Text = "";
            }
        }

        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Please Enter All Fields ');", true);
        }
        
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        txttitle.Text = "";
    }
}