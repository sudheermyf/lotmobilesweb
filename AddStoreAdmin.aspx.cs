﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AddStoreAdmin : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LotAdminUser"] == null)
        {
            Response.Redirect("Default.aspx");
        }
        else
        {
            if (!IsPostBack)
            {

            }
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        LotDBEntity.Models.Admin createAdmin = new LotDBEntity.Models.Admin();
        createAdmin.UserName = txtUserName.Text;
        createAdmin.Password = txtPassword.Text;
        createAdmin.StoreName = txtStrName.Text;
        createAdmin.StoreAddress = txtStrAddress.Text;
        if (FUStoreImage.HasFile)
        {
            string extension = Path.GetExtension(FUStoreImage.PostedFile.FileName);

            string fileName = txtStrName.Text;
            string folder = Server.MapPath("~/StoreImages/");
            Directory.CreateDirectory(folder);
            //createAdmin.StoreImage = "StoreImages\\" + fileName + extension;
            FUStoreImage.PostedFile.SaveAs(Path.Combine(folder, fileName + extension));
           
        }
        createAdmin.StoreID = txtStrId.Text;
        createAdmin.StoreManagerName = txtStrManagerName.Text;
        createAdmin.NumberOfEmployees = Convert.ToInt32(txtNoOfEmployee.Text);
        createAdmin.StoreContactNumber = txtStrContactNo.Text;
        Apps.lotContext.Admins.Add(createAdmin);
        Apps.lotContext.SaveChanges();
    }
    protected void txtUserName_TextChanged(object sender, EventArgs e)
    {
        string strVal = txtUserName.Text;
        List<LotDBEntity.Models.Admin> lstAdmin = new List<LotDBEntity.Models.Admin>();
        lstAdmin = Apps.lotContext.Admins.Where(c => c.UserName == strVal).ToList();
        if (lstAdmin.Count > 0)
        {
            txtexist.Text = "Already Exists";
        }
        else
        {
            txtexist.Text = "";
        }
        
    }
}