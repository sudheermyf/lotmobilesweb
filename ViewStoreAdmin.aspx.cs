﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class ViewStoreAdmin : System.Web.UI.Page
{
    List<LotDBEntity.Models.Admin> lstAvailStores = new List<LotDBEntity.Models.Admin>();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LotAdminUser"] == null)
        {
            Response.Redirect("Default.aspx");
        }
        else
        {
            if (!IsPostBack)
            {
                lstAvailStores = Apps.lotContext.Admins.ToList();
                lstStoreAdmin.DataSource = lstAvailStores;
                lstStoreAdmin.DataBind();
                lstStoreAdmin.Focus();
            }
        }
    }

    protected void lstStoreAdmin_SelectedIndexChanged(object sender, EventArgs e)
    {
        long val = Convert.ToInt64(lstStoreAdmin.SelectedValue);

        LotDBEntity.Models.Admin SelectedStore = Apps.lotContext.Admins.Where(c => c.SNo == val).FirstOrDefault();

        //imgStore.ImageUrl = SelectedStore.StoreImage;

        txtusername.Text = SelectedStore.UserName;
        lblStoreName.Text = SelectedStore.StoreName;
        txtStoreAddress.Text = SelectedStore.StoreAddress;
        lblStoreID.Text = SelectedStore.StoreID;
        txtmanager.Text = SelectedStore.StoreManagerName;
        txtemp.Text = SelectedStore.NumberOfEmployees.ToString();
        txtcontact.Text = SelectedStore.StoreContactNumber;
        lstStoreAdmin.Focus();

    }
    protected void btnEdit_Click(object sender, EventArgs e)
    {

    }
    protected void btnDelet_Click(object sender, EventArgs e)
    {

    }
}