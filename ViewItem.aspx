﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMasterPage.master" AutoEventWireup="true" CodeFile="ViewItem.aspx.cs" Inherits="ViewItem" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
   <script type="text/javascript">    

       function Validate() {
           
           var btnSubmit = document.getElementById('<%=btnSubmit.ClientID %>');
           var ddlbrand = document.getElementById('<%=ddlbrand.ClientID %>'); 
           var errddlBrand = document.getElementById('<%=errddlBrand.ClientID %>');
           var ddlcateg = document.getElementById('<%=ddlcateg.ClientID %>'); 
           var errddlCateg = document.getElementById('<%=errddlCateg.ClientID %>');
           var ddlmodel = document.getElementById('<%=ddlmodel.ClientID %>');
           var errddlModel = document.getElementById('<%=errddlModel.ClientID %>'); 
           var txtitemname = document.getElementById('<%=txtitemname.ClientID %>'); 
           var lstItems = document.getElementById('<%=lstItems.ClientID %>');
           var errLstItems = document.getElementById('<%=errLstItems.ClientID %>'); 
           var ErrMsg = document.getElementById('<%=ErrMsg.ClientID %>'); 


           if (ddlbrand.options[ddlbrand.selectedIndex].text == "Select") {
               errddlBrand.innerHTML = "Select Brand";
               ddlbrand.focus();
               ddlbrand.Select();
           }
           else {
               errddlBrand.innerHTML = "";
           }

           if (ddlcateg.options[ddlcateg.selectedIndex].text == "Select") {
               errddlCateg.innerHTML = "Select Category";
               ddlcateg.focus();
               ddlcateg.Select();
           }
           else {
               errddlCateg.innerHTML = "";
           }

           if (ddlmodel.options[ddlmodel.selectedIndex].text == "Select") {
               errddlModel.innerHTML = "Select Model";
               ddlmodel.focus();
               ddlmodel.Select();
           }
           else {
               errddlModel.innerHTML = "";
           }
           if (txtitemname.value.length == 0 || lstItems.options.length == 0) {
               errLstItems.innerHTML = "Select Item";
               txtitemname.value = "";
               lstItems.focus();
               lstItems.Select();
           }
           else {
               errLstItems.innerHTML = "";
           }
           if (ddlbrand.options[ddlbrand.selectedIndex].text != "Select" && ddlcateg.options[ddlcateg.selectedIndex].text != "Select" && ddlmodel.options[ddlmodel.selectedIndex].text != "Select" && txtitemname.value.length != 0 && lstmodels.options.length != 0) {
               ErrMsg.innerHTML = "";
               return true;
           }
           ErrMsg.innerHTML = "Enter Required Fields";
           return false;
       }
       </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <style type="text/css">
        #ContentPlaceHolder1_lbln,
        #ContentPlaceHolder1_lblitemimage,
        #ContentPlaceHolder1_lbldimensions,
        #ContentPlaceHolder1_lblcolor,
        #ContentPlaceHolder1_lblitemweight,
        #ContentPlaceHolder1_lblinternalmemory,
        #ContentPlaceHolder1_lblexternalmem,
        #ContentPlaceHolder1_lblpcamera,
        #ContentPlaceHolder1_lblscamera,
        #ContentPlaceHolder1_lblcamspec,
        #ContentPlaceHolder1_lblptype,
        #ContentPlaceHolder1_lblspeed,
        #ContentPlaceHolder1_lblram,
        #ContentPlaceHolder1_lblbatterytype,
        #ContentPlaceHolder1_lbltalktime,
        #ContentPlaceHolder1_lblstandby,
        #ContentPlaceHolder1_lbldisplaytype,
        #ContentPlaceHolder1_lbldisplaysize,
        #ContentPlaceHolder1_lblprice,
        #ContentPlaceHolder1_lbldisc,
        #ContentPlaceHolder1_lblhotlist,
        #ContentPlaceHolder1_lblosname,
        #ContentPlaceHolder1_lblosversion,
        #ContentPlaceHolder1_lblsim,
        #ContentPlaceHolder1_lblsimtype,
        #ContentPlaceHolder1_Label1,
        #ContentPlaceHolder1_Label2,
        #ContentPlaceHolder1_Label3,
        #ContentPlaceHolder1_Label4,
        #ContentPlaceHolder1_Label5,
        #ContentPlaceHolder1_Label6,
        #ContentPlaceHolder1_lblwifi,
        #ContentPlaceHolder1_lbledge,
        #ContentPlaceHolder1_lblgprs,
        #ContentPlaceHolder1_lbl3g,
        #ContentPlaceHolder1_lblBluetooth,
        #ContentPlaceHolder1_lblflash,
        #ContentPlaceHolder1_lbltouch,
        #ContentPlaceHolder1_lblmusic,
        #ContentPlaceHolder1_lblQwerty { font-weight: bold;font-size: 15px;color:#333;
        }
        #ContentPlaceHolder1_dataItems {  width:auto;
        }
    form input[type="text"] {margin: 2% 0 0 0;
    }
    form label {
/* display: block; */

float: left;
text-align:center;
}
        form input[type="radio"] {
        
        float:left;
        }
        .radlabel {
            font-size: 20px;
        float:left;
        padding:0 0 4% 0;
        }
        .droplabel {color: #690996;
font-size: 20px;
           }
        .ddlHead {
            font-weight: 500;
            font-family: sans-serif;
            font-size: 18px;
            color:#7a1380;
        }
        .txtHead {
            font-weight: 500;
            font-family: sans-serif;
            font-size: 16px;
            color:#333;
        }
        .ErrMsg {
            color: #DD4B39;
            font-family: Arial;
            font-size: 10px;
        }
        .pgHeading {
            font-weight: 600;
            font-family: sans-serif;
            color: #92278f;
        }
        </style>
      <div style="margin:5% 0 0 0;text-align:left;padding: 2% 0 0% 11.5%;"><h2 style="font-size:28px;">
           <asp:Label ID="lbladd" runat="server" Text="View Item" class="pgHeading"/></h2></div>
     <div style="margin: 0% 0 0 12%;" >
         <asp:UpdatePanel ID="upItem" runat="server" UpdateMode="Always">
             <ContentTemplate>
    <div style="margin: 0% 0 4% 0;">
        <ul class="select_main" style="padding: 0 0 3% 18%;">
            <li>
                <asp:Label ID="lblselectbrand" runat="server" Text="Select Brand" CssClass="ddlHead">
                    
                </asp:Label>
                <asp:DropDownList ID="ddlbrand" CssClass="logroll1" runat="server" onblur="javascript: return Validate()" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlbrand_SelectedIndexChanged">
                  <asp:ListItem Value="-1">Select</asp:ListItem>
                </asp:DropDownList>
                <asp:Label ID="errddlBrand" runat="server" class="ErrMsg" />

            </li>
            <li>
                <asp:Label ID="lblselectcateg" runat="server" Text="Select Category" CssClass="ddlHead">

                </asp:Label>
                <asp:DropDownList ID="ddlcateg" CssClass="logroll1" runat="server" onblur="javascript: return Validate()" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlcateg_SelectedIndexChanged">
                    <asp:ListItem Value="-1">Select</asp:ListItem>
                </asp:DropDownList>
                <asp:Label ID="errddlCateg" runat="server" class="ErrMsg" />
            </li>
            <li>
                <asp:Label ID="lblselectmodel" runat="server" Text="Select Model" CssClass="ddlHead">

                </asp:Label>
                <asp:DropDownList ID="ddlmodel" CssClass="logroll1" runat="server" onblur="javascript: return Validate()" OnSelectedIndexChanged="ddlmodel_SelectedIndexChanged" AppendDataBoundItems="true" AutoPostBack="true">
                    <asp:ListItem Value="-1">Select</asp:ListItem>
                </asp:DropDownList>
                <asp:Label ID="errddlModel" runat="server" class="ErrMsg" />
            </li>
        </ul>
       
    </div>
                  <div style="margin: 0% 0 4% 0;">
        <ul class="select_main" style="padding: 0 0 3% 18%;">
            <li>
                <asp:Label ID="Label7" runat="server" Text="Select Item" CssClass="ddlHead" />
                    
                <asp:Label ID="errLstItems" runat="server" class="ErrMsg" />
                <asp:ListBox ID="lstItems" runat="server" Width="200px" OnSelectedIndexChanged="lstItems_SelectedIndexChanged" AutoPostBack="true"></asp:ListBox>
            </li>            
        </ul>       
    </div>
           </ContentTemplate>
         </asp:UpdatePanel>
    <br />
         <br />
    <asp:UpdatePanel ID="upItemDetail" runat="server" UpdateMode="Always" >
             <ContentTemplate>
                 <table class="tablecls" style="width: 87%">
                     <tr>
                         <td style="color: #7a1380; font-weight: bold; padding: 0% 0 0% 0; height: 100px; font-size: 18px; width: 16%">Item Details</td>
                         <td style="width: 25%;">

                             <asp:Label ID="lbln" runat="server" Text="Item Name" CssClass="txtHead"></asp:Label><br />
                             <asp:TextBox ID="txtitemname" runat="server" ReadOnly="true" onblur="javascript: return Validate()"></asp:TextBox>

                             <asp:Label ID="lblId" runat="server" Visible="false"></asp:Label>
                         </td>
                     </tr>
                     <tr>
                         <td></td>
                         <td colspan="3">
                             <asp:DataList ID="dataItems" runat="server" DataKeyField="SNo" RepeatDirection="Horizontal" RepeatColumns="6">
                                 <ItemTemplate>
                                     <asp:Image Width="26%" ID="Image1" ImageUrl='<%# Bind("ImageName", "~/ItemImages/{0}") %>' Height="20%" runat="server" />
                                 </ItemTemplate>
                             </asp:DataList>
                         </td>
                     <tr>
                         <td style="color: #7a1380; font-weight: bold; padding: 0% 0 0% 0; height: 80px;">Price</td>
                         <td>
                             <asp:Label ID="lblprice" runat="server" Text="Price" CssClass="txtHead" /><br />
                             <asp:TextBox ID="txtprice" runat="server" required="required" ReadOnly="true" Text="0"></asp:TextBox>
                         </td>
                         <td></td>
                         <td></td>
                     </tr>
                     <tr>
                         <td></td>

                         <td>
                             <asp:Label ID="lbldimensions" runat="server" Text="Dimensions" CssClass="txtHead" /><br />
                             <asp:TextBox ID="txtd1" runat="server" Style="width: 30%; float: left; margin: 4% 0 0 1px;" ReadOnly="true" Text="0"></asp:TextBox>
                             <asp:TextBox ID="txtd2" runat="server" Style="width: 30%; float: left; margin: 4% 0 0 1px;" ReadOnly="true" Text="0"></asp:TextBox>
                             <asp:TextBox ID="txtd3" runat="server" Style="width: 30%; float: left; margin: 4% 0 0 1px;" ReadOnly="true" Text="0"></asp:TextBox>
                         </td>

                         <td>
                             <asp:Label ID="lblcolor" runat="server" Text="Color" CssClass="txtHead" /><br />
                             <asp:TextBox ID="txtcolor" runat="server" ReadOnly="true"></asp:TextBox>

                         </td>

                         <td>

                             <asp:Label ID="lblitemweight" runat="server" Text="Weight" CssClass="txtHead" /><br />

                             <asp:TextBox ID="txtweight" runat="server" ReadOnly="true" Text="0"></asp:TextBox>
                         </td>
                     </tr>

                     <tr>
                         <td style="color: #7a1380; font-weight: bold; padding: 0% 0 0% 0; height: 80px;">Storage</td>
                         <td>

                             <asp:Label ID="lblinternalmemory" runat="server" Text="Internal Memory" CssClass="txtHead" /><br />
                             <asp:TextBox ID="txtinternalmem" runat="server" ReadOnly="true"></asp:TextBox>

                         </td>
                         <td>
                             <asp:Label ID="lblexternalmem" runat="server" Text="External Memory" CssClass="txtHead" /><br />
                             <asp:TextBox ID="txtexternalmem" runat="server" ReadOnly="true"></asp:TextBox></td>

                         <td></td>
                     </tr>
                     <tr>
                         <td style="color: #7a1380; font-weight: bold; padding: 0% 0 0% 0; height: 80px;">Camera</td>

                         <td>

                             <asp:Label ID="lblpcamera" runat="server" Text="Primary Camera" CssClass="txtHead" /><br />
                             <asp:TextBox ID="txtpcamera" runat="server" ReadOnly="true"></asp:TextBox>
                         </td>

                         <td>
                             <asp:Label ID="lblscamera" runat="server" Text="Secondary Camera" CssClass="txtHead" /><br />
                             <asp:TextBox ID="txtscamera" runat="server" ReadOnly="true"></asp:TextBox>
                         </td>

                         <td>
                             <asp:Label ID="lblcamspec" runat="server" Text="Camera Specifications" CssClass="txtHead" /><br />
                             <asp:TextBox ID="txtcamspec" runat="server" ReadOnly="true"></asp:TextBox>
                         </td>
                     </tr>
                     <tr>
                         <td style="color: #7a1380; font-weight: bold; padding: 0% 0 0% 0; height: 80px;">Processor</td>
                         <td>
                             <asp:Label ID="lblptype" runat="server" Text="Processor Type" CssClass="txtHead" /><br />
                             <asp:TextBox ID="txtptype" runat="server" ReadOnly="true"></asp:TextBox>
                         </td>
                         <td>
                             <asp:Label ID="lblspeed" runat="server" Text="Processor Speed" CssClass="txtHead" /><br />
                             <asp:TextBox ID="txtpspeed" runat="server" ReadOnly="true"></asp:TextBox></td>
                         <td>
                             <asp:Label ID="lblram" runat="server" Text="Ram" CssClass="txtHead" /><br />
                             <asp:TextBox ID="txtram" runat="server" ReadOnly="true"></asp:TextBox></td>
                     </tr>
                     <tr>
                         <td style="color: #7a1380; font-weight: bold; padding: 0% 0 0% 0; height: 80px;">Battery</td>

                         <td>
                             <asp:Label ID="lblbatterytype" runat="server" Text="Battery Type" CssClass="txtHead" /><br />
                             <asp:TextBox ID="txtbatterytype" runat="server" ReadOnly="true"></asp:TextBox>

                         </td>
                         <td>
                             <asp:Label ID="lbltalktime" runat="server" Text="Talk Time" CssClass="txtHead" /><br />
                             <asp:TextBox ID="txttalktime" runat="server" ReadOnly="true"></asp:TextBox>

                         </td>

                         <td>
                             <asp:Label ID="lblstandby" runat="server" Text="stand By" CssClass="txtHead" /><br />
                             <asp:TextBox ID="txtStandBy" runat="server" ReadOnly="true"></asp:TextBox>
                         </td>

                         <td></td>
                     </tr>
                     <tr>
                         <td style="color: #7a1380; font-weight: bold; padding: 0% 0 0% 0; height: 80px;">Display</td>


                         <td>
                             <asp:Label ID="lbldisplaytype" runat="server" Text="Display Type" CssClass="txtHead" /><br />
                             <asp:TextBox ID="txtdisplaytype" runat="server" ReadOnly="true"></asp:TextBox></td>

                         <td>
                             <asp:Label ID="lbldisplaysize" runat="server" Text="Display Size" CssClass="txtHead" /><br />
                             <asp:TextBox ID="txtdisplaysize" runat="server" ReadOnly="true"></asp:TextBox></td>

                         <td></td>
                     </tr>
                     <tr>
                         <td style="color: #7a1380; font-weight: bold; padding: 0% 0 0% 0; height: 80px;">Os</td>
                         <td>
                             <asp:Label ID="lblosname" runat="server" Text="OS Name" CssClass="txtHead" /><br />
                             <asp:TextBox ID="txtosname" runat="server" ReadOnly="true"></asp:TextBox></td>
                         <td>
                             <asp:Label ID="lblosversion" runat="server" Text="OS Version" CssClass="txtHead" /><br />
                             <asp:TextBox ID="txtosversion" runat="server" ReadOnly="true"></asp:TextBox></td>
                     </tr>
                     <tr>
                         <td style="color: #7a1380; font-weight: bold; padding: 0% 0 0% 0; height: 80px;">Sim Details</td>
                         <td>
                             <asp:Label ID="lblsim" runat="server" Text="Sim" CssClass="txtHead" /><br />
                             <asp:TextBox ID="lblsim1" runat="server" ReadOnly="true"></asp:TextBox>
                         </td>

                         <td>
                             <asp:Label ID="lblsimtype" runat="server" Text="Sim Type" CssClass="txtHead" /><br />
                             <asp:TextBox ID="txtsimtype" runat="server" ReadOnly="true"></asp:TextBox></td>



                     </tr>
                          <tr>
                         <td style="color: #7a1380; font-weight: bold; padding: 0% 0 0% 0; height: 80px;">Offer</td>
                         <td>
                             <asp:Label ID="lblofferdesc" runat="server" Text="Offer Desc" CssClass="txtHead" /><br />
                             <asp:TextBox ID="txtOfferDesc" runat="server" ReadOnly="true"></asp:TextBox>
                         </td>

                         <td>
                             <asp:Label ID="lblofferimg" runat="server" Text="Offer Image" CssClass="txtHead" /><br />
                              <asp:Image ID="imgOfferDesc" runat="server" Height="80" Width="120" Visible="true" AlternateText="img.jpg" />
</td>
                     </tr>
                         <tr>
                             <td style="color: #7a1380; font-weight: bold; padding: 0% 0 0% 0; height: 80px;">Images</td>
                             <td colspan="3">
                                 <asp:DataList ID="ddlCompareImages" runat="server" DataKeyField="SNo" RepeatDirection="Horizontal" RepeatColumns="6">
                                     <ItemTemplate>
                                         <asp:Image Width="65%" ID="Image1" ImageUrl='<%# Bind("Name", "~/CompareImagesAndVideos/{0}") %>' Height="13%" runat="server" />
                                     </ItemTemplate>
                                 </asp:DataList>
                             </td>
                         </tr>
                     </tr>

                     <tr>
                         <td style="color: #7a1380; font-weight: bold; padding: 0% 0 0% 0; height: 80px;">Lot Verdict/Pro's & Con's</td>


                         <td>
                             <asp:Label ID="Label4" runat="server" Text="Lot Verdict" CssClass="txtHead" /><br />
                             <asp:TextBox ID="txtverdict" runat="server" ReadOnly="true"></asp:TextBox>

                         </td>

                         <td>
                             <asp:Label ID="Label5" runat="server" Text="Pro's" CssClass="txtHead" /><br />
                             <asp:TextBox ID="txtpros" runat="server" ReadOnly="true"></asp:TextBox>

                         </td>

                         <td>
                             <asp:Label ID="Label6" runat="server" Text="Con's" CssClass="txtHead" /><br />
                             <asp:TextBox ID="txtcons" runat="server" ReadOnly="true"></asp:TextBox>

                         </td>
                     </tr>
                     <tr>
                         <td style="color: #7a1380; font-weight: bold; padding: 0% 0 0% 0; height: 80px;">Others</td>
                         <td>
                             <asp:Label ID="lblwifi" runat="server" Text="WIFI" CssClass="txtHead" Width="40%"></asp:Label><br />
                             <asp:TextBox ID="lblwifi1" runat="server" ReadOnly="true"></asp:TextBox>
                         </td>
                         <td>
                             <asp:Label ID="lbledge" runat="server" Text="EDGE" CssClass="txtHead" Width="40%"></asp:Label><br />
                             <asp:TextBox ID="lbledge1" runat="server" ReadOnly="true"></asp:TextBox>
                         </td>

                         <td>
                             <asp:Label ID="lblgprs" runat="server" Text="GPRS" CssClass="txtHead" Width="40%"></asp:Label><br />
                             <asp:TextBox ID="lblgprs1" runat="server" ReadOnly="true"></asp:TextBox>
                         </td>
                     </tr>
                     <tr>
                         <td></td>
                         <td>
                             <asp:Label ID="lbl3g" runat="server" Text="3G &nbsp; &nbsp;&nbsp;" CssClass="txtHead" Width="40%"></asp:Label><br />
                             <asp:TextBox ID="lbl3g1" runat="server" ReadOnly="true"></asp:TextBox></td>

                         <td>
                             <asp:Label ID="lblBluetooth" runat="server" Text="Bluetooth" CssClass="txtHead" Width="40%"></asp:Label><br />
                             <asp:TextBox ID="lblbluetooth1" runat="server" ReadOnly="true"></asp:TextBox></td>
                         <td>
                             <asp:Label ID="lblflash" runat="server" Text="FLASH" CssClass="txtHead" Width="40%"></asp:Label><br />
                             <asp:TextBox ID="lblflash1" runat="server" ReadOnly="true"></asp:TextBox></td>
                     </tr>
                     <tr>
                         <td></td>
                         <td>
                             <asp:Label ID="lbltouch" runat="server" Text="Touch Screen" CssClass="txtHead" Width="40%"></asp:Label><br />
                             <asp:TextBox ID="lbltouch1" runat="server" ReadOnly="true"></asp:TextBox></td>



                         <td>
                             <asp:Label ID="lblmusic" runat="server" Text="Music Player" CssClass="txtHead" Width="40%"></asp:Label><br />
                             <asp:TextBox ID="lblradmusic" runat="server" ReadOnly="true"></asp:TextBox></td>

                         <td>
                             <asp:Label ID="lblQwerty" runat="server" Text="Qwerty" CssClass="txtHead" Width="40%"></asp:Label><br />
                             <asp:TextBox ID="lblQwerty1" runat="server" ReadOnly="true"></asp:TextBox></td>
                     </tr>
                     <tr>

                         <td colspan="4" style="text-align: center; padding: 4% 0 5% 32.5%">
                             <asp:Button ID="btnSubmit" runat="server" Text="Edit" CssClass="lotbtn" OnClientClick="javascript: return Validate()" OnClick="btnSubmit_Click" />
                             <asp:Button ID="btnCancel" runat="server" Text="Delete" CssClass="lotbtn" OnClick="btnCancel_Click" />

                             
                <asp:Label ID="ErrMsg" runat="server" class="ErrMsg" />
                         </td>
                     </tr>
                 </table>
      </ContentTemplate>
                 </asp:UpdatePanel>  
    
    </div>
   
</asp:Content>

