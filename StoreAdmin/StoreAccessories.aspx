﻿<%@ Page Title="" Language="C#" MasterPageFile="~/StoreAdmin/StoreMasterPage.master" AutoEventWireup="true" CodeFile="StoreAccessories.aspx.cs" Inherits="StoreAdmin_StoreAccessories" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <div style="margin:5% 0 0 0;text-align:center;"><h2 style="font-size:28px;">Store Accessories</h2></div>
<div style="float:left;width:50%;margin: 5% 0 0 1%;text-align:center;"> 
    <asp:Label ID="Label1" Text="Add Accessories" runat="server" ></asp:Label>
     <asp:ListBox ID="lstAdminAccessories" runat="server" style="float: left;margin: 1% 0 0 18%;width: 74%;height: 250px;"></asp:ListBox>

</div>
  <div style="float:left;width:48%;margin: 5% 0 0 1%;">  
      <asp:Label  ID="lblprice" runat="server" Text="Price"></asp:Label>
    <asp:TextBox ID="txtprice" runat="server" ></asp:TextBox>
    <br />
    <asp:Label ID="lbldiscount" runat="server" Text="Discount"></asp:Label>
    <asp:TextBox ID="txtdiscount" runat="server"></asp:TextBox>
    <br />
      <asp:Button ID="btnSubmit" runat="server" Text="Save" CssClass="lotbtn" />
        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="lotbtn" />

   </div>
</asp:Content>

