﻿<%@ Page Title="" Language="C#" MasterPageFile="~/StoreAdmin/StoreMasterPage.master" AutoEventWireup="true" CodeFile="StoreBrand.aspx.cs" Inherits="StoreAdmin_StoreBrand" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <div style="margin:5% 0 0 0;text-align:center;"><h2 style="font-size:28px;">Store Brands</h2></div>
    <div style="margin:7% 0 0 10%;float:left;width:43%;">
                    <asp:ListBox ID="lstAvailableBrands" runat="server" Width="80%"  Height="353px">   </asp:ListBox>
       </div>      
                 <div style="margin:7% 0 0 -5%;float:left;width:10%;">
                    <asp:Button ID="btnSelect" runat="server"  Text=">" CssClass="lotbtn" OnClick="btnSelect_Click" /><br />
                    <asp:Button ID="btnSelectAll" runat="server"  Text=">>" OnClick="btnSelectAll_Click" CssClass="lotbtn" /><br />
                    <asp:Button ID="btnUnselect" runat="server"  Text="<" OnClick="btnUnselect_Click" CssClass="lotbtn"/><br />
                    <asp:Button ID="btnUnselectAll" runat="server"  Text="<<" OnClick="btnUnselectAll_Click" CssClass="lotbtn"/>
                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="lotbtn" OnClick="btnSave_Click"/>
                </div>
                <div style="margin:7% 0 0 0;float:left;width:42%;">
                    <asp:ListBox ID="lstSelectedBrands" runat="server"  Width="80%"  Height="353px" ></asp:ListBox>
         
    </div>
    <br /><br />
</asp:Content>

