﻿<%@ Page Title="" Language="C#" MasterPageFile="~/StoreAdmin/StoreMasterPage.master" AutoEventWireup="true" CodeFile="StoreItems.aspx.cs" Inherits="StoreAdmin_StoreItems" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <div style="margin:5% 0 0 0;text-align:center;"><h2 style="font-size:28px;">Store Items</h2></div>
    <div style="float:left;width:50%;text-align: center;">
        <asp:Label ID="lblselectbrand" runat="server" Text="Select Brands"></asp:Label>
        <asp:ListBox ID="lstItems" runat="server" style="margin: 7% 0 0 20%;width: 50%;height: 300px;">

        </asp:ListBox><br />
        </div>
   <div style="float:left;width:50%;margin: 5% 0 0 0%;">
        <asp:CheckBox ID="chkOffers" runat="server" Text="Offers Available" />
        <asp:TextBox ID="txtoffers" runat="server" ></asp:TextBox><br />
        <asp:CheckBox ID="chkEmi" runat="server" Text="EMI" />
        <asp:TextBox ID="txtemi" runat="server"></asp:TextBox><br />
        <asp:CheckBox ID="chkFinancing" runat="server" Text="Financing Option" />
        <asp:TextBox ID="txtfinancing" runat="server"></asp:TextBox><br />
        <asp:CheckBox ID="chlexchange" runat="server" Text="Exchange" /><br />
       <asp:TextBox ID="txtexchange" runat="server"></asp:TextBox><br />
        <asp:CheckBox id="chkHotItem" runat="server" Text="Hot Item" /><br />
        <asp:Label ID="lblPrice" runat="server" Text="Price"></asp:Label>
        <asp:TextBox ID="txtPrice" runat="server" ></asp:TextBox><br />
        <asp:Label ID="lblDiscount" runat="server" Text="Discount"></asp:Label>
        <asp:TextBox ID="txtDiscount" runat="server"></asp:TextBox><br />
        <asp:Button ID="btnSubmit" runat="server" Text="Save" CssClass="lotbtn"/>
        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="lotbtn"/>
    </div>
</asp:Content>

