﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class StoreAdmin_StoreBrand : System.Web.UI.Page
{
    LotDBEntity.Models.Admin admin = new LotDBEntity.Models.Admin();
    List<LotDBEntity.Models.Brands> lstAvailBrands =new List<LotDBEntity.Models.Brands>(); 
    List<LotDBEntity.Models.Brands> lstSelectBrands=new List<LotDBEntity.Models.Brands>();
    LotDBEntity.Models.Brands selectedBrand;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            
              //Apps.lotContext.Brands.ToList();
            if (admin != null && admin.LstBrands != null)
                lstSelectBrands = admin.LstBrands.ToList();
            else
                lstSelectBrands = new List<LotDBEntity.Models.Brands>();

            LoadSelectedBrands();

            lstAvailBrands = Apps.lotContext.Brands.ToList();
            if (lstSelectBrands != null && lstSelectBrands.ToList().Count > 0)
            {
                lstSelectBrands.ForEach(c =>
                {
                    if (lstAvailBrands.Contains(c))
                        lstAvailBrands.Remove(c);
                });
            }

            LoadAvailableBrands();
        }
    }
    
    
    
    protected void btnSelect_Click(object sender, EventArgs e)
    {
        long val =Convert.ToInt64(lstAvailableBrands.SelectedValue);
       
        selectedBrand = Apps.lotContext.Brands.Where(c =>c.SNo == val).FirstOrDefault();
        //LotDBEntity.Models.Brands selectedBrand = lstAvailableBrands.SelectedItem as LotDBEntity.Models.Brands;
         if (selectedBrand != null &&   !(lstSelectBrands.Contains(selectedBrand)))
            {
                lstSelectBrands.Add(selectedBrand);
                LoadSelectedBrands();
                lstAvailBrands.Remove(selectedBrand);
                LoadAvailableBrands();
            }
    }
    private void LoadAvailableBrands()
    {
        lstAvailableBrands.DataSource = null;
        lstAvailableBrands.DataSource = lstAvailBrands;
        lstAvailableBrands.DataTextField = "Name";
        lstAvailableBrands.DataValueField = "SNo";
        lstAvailableBrands.DataBind();
    }

    private void LoadSelectedBrands()
    {
        lstSelectedBrands.DataSource = null;
        lstSelectedBrands.DataSource = lstSelectBrands;
        lstSelectedBrands.DataTextField = "Name";
        lstSelectedBrands.DataValueField = "SNo";
        lstSelectedBrands.DataBind();   
    }

    protected void btnSelectAll_Click(object sender, EventArgs e)
    {
        lstSelectBrands.AddRange(lstAvailBrands);
        lstAvailBrands.Clear();

        LoadSelectedBrands();
        LoadAvailableBrands();
    }
    protected void btnUnselect_Click(object sender, EventArgs e)
    {
        long val = Convert.ToInt64(lstAvailableBrands.SelectedValue);
        
        LotDBEntity.Models.Brands selectedBrand = Apps.lotContext.Brands.Where(c => c.SNo == val).FirstOrDefault();
        if (selectedBrand != null && !lstAvailBrands.Contains(selectedBrand))
        {
            lstAvailBrands.Add(selectedBrand);
            lstSelectBrands.Remove(selectedBrand);

            LoadSelectedBrands();
            LoadAvailableBrands();
        }
    }
    protected void btnUnselectAll_Click(object sender, EventArgs e)
    {
        lstAvailBrands.AddRange(lstSelectBrands);
        lstSelectBrands.Clear();

        LoadSelectedBrands();
        LoadAvailableBrands();
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {

        if (admin != null)
        {
            List<LotDBEntity.Models.Brands> lstBrands = new List<LotDBEntity.Models.Brands>();
            foreach (var item in lstSelectBrands)
            {
                selectedBrand = Apps.lotContext.Brands.Where(c => c.SNo == item.SNo).FirstOrDefault();
                lstBrands.Add(item);
            }
            admin.LstBrands = lstBrands;
            Apps.lotContext.SaveChanges();
        }
    }
}