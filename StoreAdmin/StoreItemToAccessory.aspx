﻿<%@ Page Title="" Language="C#" MasterPageFile="~/StoreAdmin/StoreMasterPage.master" AutoEventWireup="true" CodeFile="StoreItemToAccessory.aspx.cs" Inherits="StoreAdmin_StoreItemToAccessory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div style="margin:5% 0 0 0;text-align:center;"><h2 style="font-size:28px;">Item To Accessory</h2></div>
    <div style="margin:30px 0 0 120px;"><asp:DropDownList ID="ddlbrands" runat="server" CssClass="ddl" AutoPostBack="true" ></asp:DropDownList></div>
    <div style="margin:2% 0 0 10%;float:left;width:43%;">
        <table>
            <tr>
                <td> <asp:ListBox ID="lstAvalilableItems" runat="server" Width="90%"  Height="353px">   </asp:ListBox></td>
                <td><asp:ListBox ID="lstAvalableAccessories" runat="server" Width="80%"  Height="353px">   </asp:ListBox></td>
            </tr>
        </table>
    </div>
                 <div style="margin:2% 0 0 -5%;float:left;width:10%;">
                    <asp:Button ID="btnSelect" runat="server"  Text=">" CssClass="lotbtn"/><br />
                    <asp:Button ID="btnUnselect" runat="server"  Text="<"  CssClass="lotbtn"/><br />
                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="lotbtn"/>
                </div>
                <div style="margin:2% 0 0 0;float:left;width:42%;">
                    <asp:ListBox ID="lstSelectedItems" runat="server"  Width="80%"  Height="353px" ></asp:ListBox>
         
    </div>
    <br /><br />
</asp:Content>

