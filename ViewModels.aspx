﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMasterPage.master" AutoEventWireup="true" CodeFile="ViewModels.aspx.cs" Inherits="ViewModels" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .ErrMsg {
            color: #DD4B39;
            font-family: Arial;
            font-size: 10px;
        }
        .pgHeading {
            font-weight: 600;
            font-family: sans-serif;
            color: #92278f;
        }
    </style>
    <script type="text/javascript">        
        function Validate() {
            var ddlBrand = document.getElementById('<%=ddlBrand.ClientID %>');  
            var ddlCategory = document.getElementById('<%=ddlCategory.ClientID %>');
            var txtname = document.getElementById('<%=txtname.ClientID %>'); 
            var btnedit = document.getElementById('<%=btnedit.ClientID %>');
            var ErrddlBrand = document.getElementById('<%=ErrddlBrand.ClientID %>');
            var ErrddlCateg = document.getElementById('<%=ErrddlCateg.ClientID %>'); 
            var lstmodels = document.getElementById('<%=lstmodels.ClientID %>'); 
            var txtexist = document.getElementById('<%=txtexist.ClientID %>'); 
            var ErrName = document.getElementById('<%=ErrName.ClientID %>');

            if (ddlBrand.options[ddlBrand.selectedIndex].text == "Select") {
                ErrddlBrand.innerHTML = "Select Brand";
            }
            else {
                ErrddlBrand.innerHTML = "";
            }

            if (ddlCategory.options[ddlCategory.selectedIndex].text == "Select") {
                ErrddlCateg.innerHTML = "Select Category";
            }
            else {
                ErrddlCateg.innerHTML = "";
            }
           
            if (txtname.value.length == 0 || lstmodels.options.length == 0) {
                ErrName.innerHTML = "Select Model";
                txtname.value = "";
            }
            else {
                ErrName.innerHTML = "";
            }

            if (btnedit.value == "Edit") {
                if ( lstmodels.options.length != 0 && ddlBrand.options[ddlBrand.selectedIndex].text != "Select" && ddlCategory.options[ddlCategory.selectedIndex].text != "Select" && txtname.value.length != 0) {
                    return true;
                }
            }
            else if (btnedit.value == "Update") {
                
                if ( lstmodels.options.length != 0 && txtexist.innerHTML != "Already Exists" && ddlBrand.options[ddlBrand.selectedIndex].text != "Select" && ddlCategory.options[ddlCategory.selectedIndex].text != "Select" && txtname.value.length != 0) {
                    return true;
                }
                else {
                    txtexist.value == ""
                }
                return false;
            }
            return false;
        }
    </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div style="margin: 5% 0 0 0; text-align: left; padding: 2% 0 0% 11.5%;">
        <h2 style="font-size: 28px;">
            <asp:Label ID="lbladd" runat="server" Text="View Model" class="pgHeading" /></h2>
    </div>

        <asp:UpdatePanel ID="upmodel" runat="server">
            <ContentTemplate>
                
            <div style="width: 100%; padding: 3% 0 0 27.5%; margin: -2% 0 0 0;">

                <div style="width: 35%; float: left;">

                    <asp:Label ID="Label2" runat="server" Text="Select Brand" />
                    <asp:Label ID="ErrddlBrand" runat="server" CssClass="ErrMsg"/>

                    <asp:DropDownList ID="ddlBrand" runat="server" onblur="javascript: return Validate()" OnSelectedIndexChanged="ddlBrand_SelectedIndexChanged" Style="margin: 1% 0 4% 0;width:85%" AutoPostBack="true" AppendDataBoundItems="true" CssClass="ddl">
                        <asp:ListItem Value="-1">Select</asp:ListItem>
                    </asp:DropDownList>

                    <asp:Label ID="Label3" runat="server" Text="Select Category" />                    
                    <asp:Label ID="ErrddlCateg" runat="server" CssClass="ErrMsg"/>
                    <asp:DropDownList ID="ddlCategory" runat="server" AutoPostBack="true" onblur="javascript: return Validate()" AppendDataBoundItems="true" Style="margin: 1% 0 4% 0;width:85%" OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged" CssClass="ddl">
                        <asp:ListItem Value="-1">Select</asp:ListItem>
                    </asp:DropDownList>

                    <asp:ListBox ID="lstmodels" runat="server" AutoPostBack="true" onblur="javascript: return Validate()" OnSelectedIndexChanged="lstmodels_SelectedIndexChanged" Width="85%" Height="310px"></asp:ListBox>

                </div>

                <div style="width: 60%; float: left; margin: 4% 0 3% 0%;">
                    <div>
                        <asp:Label ID="lblName" runat="server" Text="Name"></asp:Label>
                        <asp:TextBox ID="txtname" runat="server" AutoPostBack="true" onblur="javascript: return Validate()" OnTextChanged="txtname_TextChanged" ReadOnly="true"></asp:TextBox>
                        <asp:Label ID="txtexist" runat="server" CssClass="ErrMsg"/>
                        <asp:Label ID="ErrName" runat="server" CssClass="ErrMsg"/>
                        <br />
                        <br />
                        <asp:Label ID="lbldesc" runat="server" Text="Description"></asp:Label>
                        <asp:TextBox ID="txtdesc" runat="server" AutoPostBack="true" ReadOnly="true"></asp:TextBox>
                        <asp:Label ID="lblId" runat="server" Visible="false"></asp:Label>
                    </div>
                </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    <div style="margin:16% 0 0 62%;">
        <asp:Button ID="btnedit" runat="server" Text="Edit" CssClass="lotbtn" OnClientClick="javascript: return Validate()" OnClick="btnedit_Click" />
        <asp:Button ID="btndelete" runat="server" Text="Delete" CssClass="lotbtn" OnClick="btndelete_Click" />
    </div>
  
</asp:Content>

