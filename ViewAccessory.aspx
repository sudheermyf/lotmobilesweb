﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMasterPage.master" AutoEventWireup="true" CodeFile="ViewAccessory.aspx.cs" Inherits="ViewAccessory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .ErrMsg {
            color: #DD4B39;
            font-family: Arial;
            font-size: 10px;
        }
        .pgHeading {
            font-weight: 600;
            font-family: sans-serif;
            color: #92278f;
        }
    </style>
    
     <script type="text/javascript">
         function Validate() {
             var ddlBrand = document.getElementById('<%=ddlBrand.ClientID %>'); 
             var ddlCategory = document.getElementById('<%=ddlCategory.ClientID %>');
             var lstaccessories = document.getElementById('<%=lstaccessories.ClientID %>'); 
             var ErrddlBrand = document.getElementById('<%=ErrddlBrand.ClientID %>'); 
             var ErrddlCateg = document.getElementById('<%=ErrddlCateg.ClientID %>'); 
             var lstaccessories = document.getElementById('<%=lstaccessories.ClientID %>'); 
             var txtname = document.getElementById('<%=txtname.ClientID %>');
             var ErrlstAcc = document.getElementById('<%=ErrlstAcc.ClientID %>');


             var txtprice = document.getElementById('<%=txtprice.ClientID %>');
             var txtkey1 = document.getElementById('<%=txtkey1.ClientID %>');
             var txtkey2 = document.getElementById('<%=txtkey2.ClientID %>');
             var txtkey3 = document.getElementById('<%=txtkey3.ClientID %>');
             var txtkey4 = document.getElementById('<%=txtkey4.ClientID %>');

             if (ddlBrand.options[ddlBrand.selectedIndex].text == "Select") {
                 ErrddlBrand.innerHTML = "Select Brand";
                 txtprice.value = "";
                 txtkey1.value = "";
                 txtkey2.value = "";
                 txtkey3.value = "";
                 txtkey4.value = "";
                 txtname.value = "";
             }
             else {
                 ErrddlBrand.innerHTML = "";
             }

             if (ddlCategory.options[ddlCategory.selectedIndex].text == "Select") {
                 ErrddlCateg.innerHTML = "Select Category";
                 txtprice.value = "";
                 txtkey1.value = "";
                 txtkey2.value = "";
                 txtkey3.value = "";
                 txtkey4.value = "";
                 txtname.value = "";
             }
             else {
                 ErrddlCateg.innerHTML = "";
             }


             if (txtname.value.length == 0 || lstaccessories.options.length == 0) {
                 ErrlstAcc.innerHTML = "Select Accessory";
                 txtprice.value = "";
                 txtkey1.value = "";
                 txtkey2.value = "";
                 txtkey3.value = "";
                 txtkey4.value = "";
                 txtname.value = "";
             }
             else {
                 ErrlstAcc.innerHTML = "";
             }

             if (ddlBrand.options[ddlBrand.selectedIndex].text != "Select" && ddlCategory.options[ddlCategory.selectedIndex].text != "Select" && txtname.value.length != 0 && lstaccessories.options.length != 0) {               
                 return true;
             }
             return false;
         }
     </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div style="margin: 3% 0 0 0; text-align: left; padding: 2% 0 0% 11.5%;">
        <h2 style="font-size: 28px;">
            <asp:Label ID="lbladd" runat="server" Text="View Accessory" class="pgHeading"/>
        </h2>
    </div>
    <div style="width: 100%;">
        <asp:UpdatePanel ID="upmodel" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <div style="width: 100%; padding: 0 0 0 27.5%; margin: 2% 0 0 0;">

                    <div style="width: 35%; float: left;">
                        <asp:Label ID="lblId" runat="server" Visible="false"/>
                        <asp:Label ID="Label2" runat="server" Text="Select Brand"/>
                        <asp:Label ID="ErrddlBrand" runat="server" CssClass="ErrMsg"/>

                        <asp:DropDownList ID="ddlBrand" runat="server" onblur="javascript:return Validate()" Style="margin: 1% 0 4% 0;width:85%" OnSelectedIndexChanged="ddlBrand_SelectedIndexChanged" AutoPostBack="true" AppendDataBoundItems="true" CssClass="ddl">
                            <asp:ListItem Value="-1">Select</asp:ListItem>
                        </asp:DropDownList>

                        <asp:Label ID="Label3" runat="server" Text="Select Category"/>
                        <asp:Label ID="ErrddlCateg" runat="server" CssClass="ErrMsg"/>

                        <asp:DropDownList ID="ddlCategory" runat="server" onblur="javascript:return Validate()" Style="margin: 1% 0 4% 0;width:85%" AutoPostBack="true" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged" CssClass="ddl">
                            <asp:ListItem Value="-1">Select</asp:ListItem>
                        </asp:DropDownList>
                        
                        <asp:Label ID="ErrlstAcc" runat="server" CssClass="ErrMsg"/>
                        <asp:ListBox ID="lstaccessories" runat="server" onblur="javascript:return Validate()" Style="margin: 1% 0 4% 0;width:85%;height:330px;" AutoPostBack="true" Width="100%" OnSelectedIndexChanged="lstaccessories_SelectedIndexChanged"></asp:ListBox>

                    </div>

                    <div style="width: 60%; float: left;margin: 0 0 5% 0;">
                        
                        <asp:Label ID="Label6" Style="float: left;" runat="server" Text="Image"/><br />
                        <asp:Image ID="imgaccessorylogo" runat="server" Height="80" Width="120" Visible="true" AlternateText="img.jpg" />
                        <br />
                        <asp:Label ID="lblName" runat="server" Text="Name" />
                        <asp:TextBox ID="txtname" runat="server" AutoPostBack="true" Width="60%" OnTextChanged="txtname_TextChanged" ReadOnly="true"/>
                        <asp:Label ID="txtexist" runat="server"/>
                        <br />

                        <asp:Label ID="lblprice" runat="server" Text="Price"/>
                        <asp:TextBox ID="txtprice" runat="server" ReadOnly="true" Width="60%"/>
                        <br />


                        <asp:Label ID="lblkey1" runat="server" Text="Key Feature1"/>
                        <asp:TextBox ID="txtkey1" runat="server" AutoPostBack="true" Style="width: 60%" ReadOnly="true"/>
                        <br />

                        <asp:Label ID="lblkey2" runat="server" Text="Key Feature2"/>
                        <asp:TextBox ID="txtkey2" runat="server" AutoPostBack="true" Style="width: 60%" ReadOnly="true"/>
                        <br />

                        <asp:Label ID="lblkey3" runat="server" Text="Key Feature3"/>
                        <asp:TextBox ID="txtkey3" runat="server" AutoPostBack="true" Style="width: 60%" ReadOnly="true"/>                      
                        <br />

                        <asp:Label ID="lblkey4" runat="server" Text="Key Feature4"/>
                        <asp:TextBox ID="txtkey4" runat="server" AutoPostBack="true" Style="width: 60%" ReadOnly="true"/>
                    </div>
                    </div>
                    <br />
                  
            </ContentTemplate>
           
        </asp:UpdatePanel>
        <br />
       <div style="margin: 0 0 0 62%;">
                        <asp:Button ID="btnedit" runat="server" Text="Edit" OnClientClick="javascript:return Validate()" CssClass="lotbtn" OnClick="btnedit_Click" />
                        <asp:Button ID="btndelete" runat="server" Text="Delete" CssClass="lotbtn" OnClick="btndelete_Click" />
                    </div>
    </div>
</asp:Content>

