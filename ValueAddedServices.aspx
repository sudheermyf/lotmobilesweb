﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMasterPage.master" AutoEventWireup="true" CodeFile="ValueAddedServices.aspx.cs" Inherits="ValueAddedServices" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .ErrMsg {
            color: #DD4B39;
            font-family: Arial;
            font-size: 10px;
        }
        .pgHeading {
            font-weight: 600;
            font-family: sans-serif;
            color: #92278f;
        }
    </style>

      <style type="text/css">
           #ContentPlaceHolder1_DlstItemGallery {  width:auto;
        }
    form input[type="text"] {margin: 2% 0 0 0;
    }
    form label {
/* display: block; */

float: left;
text-align:center;
}
        form input[type="radio"] {
        
        float:left;
        }
        .radlabel {
            font-size: 20px;
        float:left;
        padding:0 0 4% 0;
        }
        .droplabel {color: #690996;
font-size: 20px;
           }
        .auto-style1 {
            height: 56px;
        }
        /*----------------data Table GridView Css----------------------*/
        .grd_TableScroll
        {
            max-height: 275px;
            overflow: auto;
            border:1px solid #ccc;           
        }
        .grd_DataTable
        {
            border-collapse: collapse;
            border:1px solid #ccc;
            width:100%;
        }
            .grd_DataTable tr th
            {
                background-color: #7a1380;
                color: #ffffff;
                padding: 10px 5px 10px 5px;
                border: 1px solid #8A4387;
                font-family: Arial, Helvetica, sans-serif;
                font-size: 12px;
                font-weight: normal;
                text-transform:capitalize;
            }
            .grd_DataTable tr:nth-child(2n+2)
            {
                background-color: #f3f4f5; 
                border: 1px solid #ccc;              
            }

            .EU_DataTable tr:nth-child(2n+1) 
            {
                background-color: #d6dadf;
                color: #454545;
            }
            .grd_DataTable tr td
            {
                padding: 5px 10px 5px 10px;
                color: #454545;
                font-family: Arial, Helvetica, sans-serif;
                font-size: 11px;
                
                vertical-align: middle;
            }
                .grd_DataTable tr td:first-child
                {
                    text-align: center;
                }
        /*-----------------------------------------------------------------*/


     </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <asp:Label ID="lblId" runat="server" />
     <div style="margin:5% 0 0 0;text-align:left;padding: 2% 0 0% 11.5%;"><h2 style="font-size:28px;">
         <asp:HiddenField runat="server" ID="hfGUID" />
         <asp:Label ID="lbladd" runat="server" Text="Juke Box" class="pgHeading"/>
                                                                        </h2></div>
    <br />
     <div>
            <asp:GridView ID="gvServices" runat="server" ShowFooter="True" AutoGenerateColumns="False"
                CellPadding="4" ForeColor="#333333" OnRowDeleting="gvServices_RowDeleting"
                Width="80%" Style="text-align: center; margin: 0 auto;" CssClass="grd_DataTable">
                <Columns>
                    <asp:BoundField DataField="RowNumber" HeaderText="SNo" />
                    <asp:TemplateField HeaderText="Browse">
                        <ItemTemplate>
                            <asp:FileUpload ID="fp1" runat="server" AllowMultiple="true" />
                           <%-- <asp:Label ID="lblfp" runat="server" ></asp:Label>--%>
                         <%--   <asp:Image ID="imgFp" runat="server" />--%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="File Types" HeaderStyle-Width="12%">
                        <ItemTemplate>
                           <asp:DropDownList ID="ddlfiletype" DataTextField="Name" DataValueField="SNo" AppendDataBoundItems="true" runat="server" Width="100%">
                                <asp:ListItem Value="-1">Select</asp:ListItem>
                            </asp:DropDownList>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Category"  HeaderStyle-Width="12%">
                        <ItemTemplate>
                           <asp:DropDownList ID="ddlcateg" runat="server" DataTextField="Name" DataValueField="SNo" AppendDataBoundItems="true"  Width="100%">
                                <asp:ListItem Value="-1">Select</asp:ListItem>
                            </asp:DropDownList>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Artist" HeaderStyle-Width="12%">
                        <ItemTemplate>
                            <asp:DropDownList ID="ddlauthor" runat="server" DataTextField="Name" AppendDataBoundItems="true" DataValueField="SNo" Width="100%">
                                <asp:ListItem Value="-1">Select</asp:ListItem>
                            </asp:DropDownList>
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText="Music Director" HeaderStyle-Width="12%">
                        <ItemTemplate>
                            <asp:DropDownList ID="ddl4" runat="server" DataTextField="Name" DataValueField="SNo" AppendDataBoundItems="true" Width="100%">
                                <asp:ListItem Value="-1">Select</asp:ListItem>
                            </asp:DropDownList>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Album" HeaderStyle-Width="12%">
                        <ItemTemplate>
                            <asp:DropDownList ID="ddl5" runat="server" Width="100%" DataTextField="Name" DataValueField="SNo" AppendDataBoundItems="true">
                                <asp:ListItem Value="-1">Select</asp:ListItem>
                            </asp:DropDownList>
                        </ItemTemplate>
                        <FooterStyle HorizontalAlign="Right" />
                        <FooterTemplate>
                            <asp:Button ID="ButtonAdd" runat="server" Text="Add New Row" Visible="false"   Style="padding: 0;font-size: 12pt;margin: 0;" class="lotbtn" />
                        </FooterTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="checklbl" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lblfp" runat="server" ></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:CommandField ShowDeleteButton="True" Visible="false" />
                </Columns>              
            </asp:GridView>
            <br />
            <center>
                <asp:Button ID="btnSave" runat="server" Text="Save Data" OnClick="btnSave_Click" Style="margin: 0 auto;" class="lotbtn" />
            </center>
        </div>
</asp:Content>

