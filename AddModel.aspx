﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMasterPage.master" AutoEventWireup="true" CodeFile="AddModel.aspx.cs" Inherits="AddModel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .ErrMsg {
            color: #DD4B39;
            font-family: Arial;
            font-size: 10px;
        }
        .pgHeading {
            font-weight: 600;
            font-family: sans-serif;
            color: #92278f;
        }
    </style>
    <script type="text/javascript">
        function Validate() {
            var ddlBrand = document.getElementById('<%=ddlBrand.ClientID %>');
            var errBrand = document.getElementById('<%=errBrand.ClientID %>');
            var ddlCategory = document.getElementById('<%=ddlCategory.ClientID %>');
            var errCateg = document.getElementById('<%=errCateg.ClientID %>');
            var txtmodelname = document.getElementById('<%=txtmodelname.ClientID %>');
            var ErrModelName = document.getElementById('<%=ErrModelName.ClientID %>'); 
            var txtexist = document.getElementById('<%=txtexist.ClientID %>');
            var ddlBrand1 = $(ddlBrand).find('option').filter(':selected').text();
            var ddlCategory1 = $(ddlCategory).find('option').filter(':selected').text();

            if (ddlBrand1 == "Select" || ddlBrand1 == "") {
                errBrand.innerHTML = "Select Brand";
            }
            else {
                errBrand.innerHTML = "";
            }

            if (ddlCategory1 == "Select" || ddlCategory1 == "") {
                errCateg.innerHTML = "Select Category";
            }
            else {
                errCateg.innerHTML = "";
            }

            if (txtmodelname.value == "") {
                ErrModelName.innerHTML = "Enter Model Name";
            }
            else {
                ErrModelName.innerHTML = "";
            }
            
            if (txtexist.textContent == "Already Exists") {
                ErrModelName.innerHTML = "";
            }
            else {
                txtexist.innerHTML = "";
            }

            if (errBrand.innerHTML == "" &&
                txtexist.textContent != "Already Exists" && errCateg.innerHTML == "" && ErrModelName.innerHTML == "") {
                return true;
            }
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div style="margin: 5% 0 0 0; text-align: left; padding: 2% 0 0% 11.5%;">
        <h2 style="font-size: 28px;"><asp:Label ID="lbladd" runat="server" Text="Add Model" class="pgHeading"/></h2>
    </div>
    <div style="width: 100%; padding: 3% 0 0 23%; margin: -8% 0 0 0;">
        <asp:UpdatePanel ID="upmodel" runat="server" UpdateMode="Always">
            <ContentTemplate>

                <table style="width: 100%; margin: 6% 0 4% 6%;">
                    <tr>
                        <td style="width: 15%;">
                            <asp:Label ID="Label2" runat="server" Text="Select Brand"></asp:Label></td>
                        <td>

                            <asp:UpdatePanel ID="upBrand" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList ID="ddlBrand" runat="server" Width="45%" onblur="javascript: return Validate()" OnSelectedIndexChanged="ddlBrand_SelectedIndexChanged" AutoPostBack="true" AppendDataBoundItems="true" CssClass="ddl">
                                        <asp:ListItem Value="-1">Select</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:Label ID="errBrand" runat="server" class="ErrMsg" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label3" runat="server" Text="Select Category"></asp:Label></td>
                        <td>

                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList ID="ddlCategory" runat="server" CssClass="ddl" Width="45%" onblur="javascript: return Validate()"></asp:DropDownList>
                                    <asp:Label ID="errCateg" runat="server" class="ErrMsg" />
                                </ContentTemplate>
                            </asp:UpdatePanel>

                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label1" runat="server" Text="Model Name"></asp:Label></td>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtmodelname" runat="server" style="text-transform:uppercase" placeholder="Model Name" Width="45%" onblur="javascript: return Validate()" AutoPostBack="true" OnTextChanged="txtItemName_TextChanged"></asp:TextBox>

                                    <asp:Label ID="ErrModelName" runat="server" class="ErrMsg" />
                                    <asp:Label ID="txtexist" runat="server" class="ErrMsg" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label4" runat="server" Text="Model Description"></asp:Label><br />
                            <asp:Label ID="Label7" Style="Font-Size: 12px;" runat="server" Text="(Optional)"></asp:Label></td>
                        <td>
                            <asp:TextBox ID="txtDesc" runat="server" placeholder="Description" Width="45%" TextMode="MultiLine"></asp:TextBox></td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <table>
            <tr>
                <td>&nbsp;</td>
                <td style="padding: 0 0 0 35.8%">
                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClientClick="javascript: return Validate()" CssClass="lotbtn" OnClick="btnSubmit_Click"/>
                    <asp:Button ID="Button1" runat="server" Text="Cancel" OnClick="Button1_Click" CssClass="lotbtn" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

