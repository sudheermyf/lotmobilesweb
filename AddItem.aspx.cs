﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;

public partial class AddItem : System.Web.UI.Page
{
    LotDBEntity.Models.Models model;
    //LotDBEntity.Models.BasicItem item;
    object lockTarget = new object();
    string sGuid;
    //public List<LotDBEntity.Models.ImageCollection> lstImageCollection;
    //public List<LotDBEntity.Models.ItemCamImgsAndVideos> lstImagesAndVideos;
    public string strCmpImgRandomName = new Random().Next(1000, 100000).ToString();
    protected void Page_Load(object sender, EventArgs e)
    {       
        
        if (Session["LotAdminUser"] == null)
        {
            Response.Redirect("Default.aspx");
        }
        else
        {
            if (!IsPostBack)
            {
                sGuid = Guid.NewGuid().ToString();
                hfGUID.Value = sGuid;
                LogHelper.Logger.Info("Starts PageLoad in AddItems Page");
                lock (lockTarget)
                {
                    var brands = Apps.lotContext.Brands.ToList();
                    ddlbrand.DataSource = brands;
                    ddlbrand.DataTextField = "Name";
                    ddlbrand.DataValueField = "SNo";
                    ddlbrand.DataBind();
                }
                if (Request.QueryString["SNo"] != null)
                {
                    LogHelper.Logger.Info("Edit SNO starts");
                    btnSubmit.Text = "Update";
                    btnCancel.Text = "Reset";
                    lbladd.Text = "Edit Item";
                    LogHelper.Logger.Info("Edit SNO Ends");
                    getUpdatedData();
                }
                LogHelper.Logger.Info("Starts PageLoad in AddItems Page");
            }
        }
    }

    private void getUpdatedData()
    {
        try
        {
            LogHelper.Logger.Info("getUpdatedData() starts");
            long Id = Convert.ToInt64(Request.QueryString["SNo"]);
            lblId.Text = Request.QueryString["SNo"].ToString();
            lock (lockTarget)
            {
                LotDBEntity.Models.BasicItem SelectedItem = Apps.lotContext.BasicItems.Where(c => c.SNo == Id).FirstOrDefault();
                ddlbrand.Items.FindByText(SelectedItem.ItemBrand.Name).Selected = true;
                ddlbrand.Enabled = false;
                string brandname = SelectedItem.ItemBrand.Name;
                var Categ = Apps.lotContext.Categories.Where(c => c.Brand.Name == brandname).ToList();
                ddlcateg.DataSource = Categ;
                ddlcateg.DataTextField = "Name";
                ddlcateg.DataValueField = "SNo";
                ddlcateg.DataBind();
                ddlcateg.Items.FindByText(SelectedItem.ItemCateg.Name).Selected = true;
                ddlcateg.Enabled = false;
                string categname = SelectedItem.ItemCateg.Name;
                var model = Apps.lotContext.Models.Where(c => c.Category.Name == categname && c.Brand.Name == brandname).ToList();
                ddlmodel.DataSource = model;
                ddlmodel.DataTextField = "Name";
                ddlmodel.DataValueField = "SNo";
                ddlmodel.DataBind();
                ddlmodel.Items.FindByText(SelectedItem.ItemModel.Name).Selected = true;
                ddlmodel.Enabled = false;
                txtitemname.Text = SelectedItem.ItemName;
                txtd1.Text = SelectedItem.Length.ToString();
                txtd2.Text = SelectedItem.Breadth.ToString();
                txtd3.Text = SelectedItem.Height.ToString();
                txtcolor.Text = SelectedItem.ItemColor;
                txtweight.Text = SelectedItem.Weight.ToString();
                imgOfferDesc.ImageUrl = "~/OfferImage/" + SelectedItem.ExtraOfferImageName;
                txtinternalmem.Text = SelectedItem.InternalStorage;
                txtexternalmem.Text = SelectedItem.ExternalStorage;
                txtpcamera.Text = SelectedItem.PrimaryCamera;
                txtscamera.Text = SelectedItem.SecondaryCamera;
                txtcamspec.Text = SelectedItem.CameraSpecifications;
                txtptype.Text = SelectedItem.ProcessorType;
                txtpspeed.Text = SelectedItem.ProcessorSpeed;
                txtram.Text = SelectedItem.RAM;
                txtbatterytype.Text = SelectedItem.BatteryType;
                txttalktime.Text = SelectedItem.TalkTime;
                txtStandBy.Text = SelectedItem.StandBy;
                txtdisplaytype.Text = SelectedItem.DisplayType;
                txtdisplaysize.Text = SelectedItem.DisplaySize;
                txtprice.Text = SelectedItem.DefaultPrice.ToString();
                txtosname.Text = SelectedItem.OSName;
                txtosversion.Text = SelectedItem.OSVersion;
                ddlsim.Items.FindByText(SelectedItem.SIM).Selected = true;
                txtsimtype.Text = SelectedItem.SIMType;
                txtverdict.Text = SelectedItem.Verdict;
                txtpros.Text = SelectedItem.Pros;
                txtcons.Text = SelectedItem.Cons;
                txtOfferDesc.Text = SelectedItem.ExtraOfferDesc;
                DlstItemGallery.DataSource = SelectedItem.LstImages;
                DlstItemGallery.DataBind();
                ddlCompareImages.DataSource = SelectedItem.LstImgsAndVideos.Where(c => c.IsVideo == false).ToList();
                ddlCompareImages.DataBind();
                List<LotDBEntity.Models.ItemCamImgsAndVideos> lstCompare = SelectedItem.LstImgsAndVideos.Where(c => c.IsVideo == true).ToList();
                if (lstCompare.Count > 0)
                {
                    lblcomparevideo.Text = SelectedItem.LstImgsAndVideos.Where(c => c.IsVideo == true).FirstOrDefault().Name.ToString();
                }
                else
                {
                    lblcomparevideo.Text = "";
                }
                if (SelectedItem.Supports3G == true)
                {
                    rad3g.SelectedValue = "Yes";
                }
                else
                {
                    rad3g.SelectedValue = "No";
                }
                if (SelectedItem.Wifi == true)
                {
                    radwifi.SelectedValue = "Yes";
                }
                else
                {
                    radwifi.SelectedValue = "No";
                }
                if (SelectedItem.FlashAvailable == true)
                {
                    radflash.SelectedValue = "Yes";
                }
                else
                {
                    radflash.SelectedValue = "No";
                }
                if (SelectedItem.Bluetooth == true)
                {
                    radBluetooth.SelectedValue = "Yes";
                }
                else
                {
                    radBluetooth.SelectedValue = "No";
                }
                if (SelectedItem.EDGE == true)
                {
                    radedge.SelectedValue = "Yes";
                }
                else
                {
                    radedge.SelectedValue = "No";
                }
                if (SelectedItem.Quert == true)
                {
                    radQwerty.SelectedValue = "Yes";
                }
                else
                {
                    radQwerty.SelectedValue = "No";
                }
                if (SelectedItem.TouchScreen == true)
                {
                    radtouch.SelectedValue = "Yes";
                }
                else
                {
                    radtouch.SelectedValue = "No";
                }
                if (SelectedItem.MusicPlayer == true)
                {
                    radmusic.SelectedValue = "Yes";
                }
                else
                {
                    radmusic.SelectedValue = "No";
                }
                if (SelectedItem.GPRS == true)
                {
                    radgprs.SelectedValue = "Yes";
                }
                else
                {
                    radgprs.SelectedValue = "No";
                }
                txtexist.Text = "";
            }
            LogHelper.Logger.Info("getUpdatedData() Ends");
        }
        catch (Exception ex)
        {
           LogHelper.Logger.ErrorException("Exception: getUpdatedDate : addItem: " + ex.Message, ex);
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (btnSubmit.Text == "Submit")
        {
            try
            {
                LotDBEntity.Models.BasicItem item;
                if (fpimg.PostedFiles.Count < 5 && fpUploadImg.PostedFiles.Count < 5 && fpUploadVideo.PostedFile.ContentLength < 70 * 1024 * 1024)
                {
                    if (txtexist.Text != "Already Exists" && ddlmodel.SelectedItem.Text != "Select" && ddlcateg.SelectedItem.Text != "Select" && ddlbrand.SelectedItem.Text != "Select")
                    {
                        lock (lockTarget)
                        {
                            string strName = txtitemname.Text.ToUpper().Trim();
                            long val1 = Convert.ToInt64(ddlbrand.SelectedValue);
                            LotDBEntity.Models.Brands brand;
                            lock (lockTarget)
                            {
                                brand = Apps.lotContext.Brands.Where(c => c.SNo == val1).FirstOrDefault();
                            }
                            long val = Convert.ToInt64(ddlcateg.SelectedValue);
                            LotDBEntity.Models.Category Categ;
                            lock (lockTarget)
                            {
                                Categ = Apps.lotContext.Categories.Where(c => c.SNo == val).FirstOrDefault();
                            }
                            long val2 = Convert.ToInt64(ddlmodel.SelectedValue);
                            lock (lockTarget)
                            {
                                model = Apps.lotContext.Models.Where(c => c.SNo == val2).FirstOrDefault();
                            }
                            LogHelper.Logger.Info("Starts Checking ItemName Exists in Submit Button Of AddItem Page");
                            CheckItemNameExist();
                            LogHelper.Logger.Info("Ended Checking ItemName Exists in Submit Button Of AddItem Page");
                            if (txtexist.Text == "Already Exists")
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Item name already exists');", true);
                            }
                            else
                            {
                                btnSubmit.Enabled = false;
                                item = new LotDBEntity.Models.BasicItem();
                                item.ItemName = txtitemname.Text.ToUpper().Trim();
                                item.ItemColor = txtcolor.Text;
                                List<LotDBEntity.Models.ItemCamImgsAndVideos> lstImagesAndVideos = new List<LotDBEntity.Models.ItemCamImgsAndVideos>();
                                if (fpUploadImg.HasFiles)
                                {
                                    lock (lockTarget)
                                    {
                                        int j = 1;
                                        long cmpImgCnt = 0;
                                        if (Apps.lotContext.ItemCamImgsVideos.ToList().Count > 0)
                                            cmpImgCnt = Apps.lotContext.ItemCamImgsVideos.Max(c => c.ID);

                                        foreach (HttpPostedFile Img in fpUploadImg.PostedFiles)
                                        {
                                            string Imagesfolder1 = Server.MapPath("~/CompareImagesAndVideos/");
                                            string strImagesFolder = "CompareImagesAndVideos";

                                            if (cmpImgCnt > 0)
                                            {
                                                cmpImgCnt = cmpImgCnt + 1;
                                            }
                                            else
                                            {
                                                cmpImgCnt = 1;
                                            }
                                            string strRandomName = txtitemname.Text + j + System.IO.Path.GetExtension(Img.FileName);
                                            lstImagesAndVideos.Add(new LotDBEntity.Models.ItemCamImgsAndVideos { ID = cmpImgCnt, Name = hfGUID.Value + "~" + strRandomName, Location = "pack://siteoforigin:,,,/" + strImagesFolder + "/" + hfGUID.Value + "~" + strRandomName, WebLocation = Imagesfolder1 + hfGUID.Value + "~" + strRandomName, IsVideo = false, LastUpdatedTime = DateTime.Now });
                                            Img.SaveAs((Path.Combine(Imagesfolder1, hfGUID.Value + "~" + strRandomName)));
                                            j++;
                                        }
                                    }
                                }

                                if (fpUploadVideo.HasFile)
                                {
                                    lock (lockTarget)
                                    {
                                        int x = 10;
                                        long videoCnt = 0;
                                        if (Apps.lotContext.ItemCamImgsVideos.ToList().Count > 0)
                                            videoCnt = Apps.lotContext.ItemCamImgsVideos.Max(c => c.ID);
                                        string Imagesfolder2 = Server.MapPath("~/CompareImagesAndVideos/");
                                        string strImagesFolder = "CompareImagesAndVideos";
                                        if (videoCnt > 0)
                                        {
                                            videoCnt = videoCnt + 1;
                                        }
                                        else
                                        {
                                            videoCnt = 1;
                                        }
                                        string strCmpVideoRandomName = txtitemname.Text + x + System.IO.Path.GetExtension(fpUploadVideo.FileName);
                                        lstImagesAndVideos.Add(new LotDBEntity.Models.ItemCamImgsAndVideos { ID = videoCnt, Name = hfGUID.Value + "~" + strCmpVideoRandomName, Location = "pack://siteoforigin:,,,/" + strImagesFolder + "/" + hfGUID.Value + "~" + strCmpVideoRandomName, WebLocation = Imagesfolder2 + hfGUID.Value + "~" + strCmpVideoRandomName, IsVideo = true, LastUpdatedTime = DateTime.Now });
                                        HttpPostedFile Video = fpUploadVideo.PostedFile;
                                        Video.SaveAs((Path.Combine(Imagesfolder2, hfGUID.Value + "~" + strCmpVideoRandomName)));
                                    }
                                }
                                item.LstImgsAndVideos = lstImagesAndVideos;
                                List<LotDBEntity.Models.ImageCollection> lstImageCollection = new List<LotDBEntity.Models.ImageCollection>();


                                if (fpimg.HasFiles)
                                {
                                    lock (lockTarget)
                                    {
                                        int i = 1;
                                        long imgCnt = 0;
                                        if (Apps.lotContext.ImageCollection.ToList().Count > 0)
                                            imgCnt = Apps.lotContext.ImageCollection.Max(c => c.ID);
                                        foreach (HttpPostedFile item1 in fpimg.PostedFiles)
                                        {
                                            string folder = Server.MapPath("~/ItemImages/");
                                            string strFolder = "ItemImages";

                                            if (imgCnt > 0)
                                            {
                                                imgCnt = imgCnt + 1;
                                            }
                                            else
                                            {
                                                imgCnt = 1;
                                            }
                                            string strCmpImgRandomName = txtitemname.Text + i + System.IO.Path.GetExtension(item1.FileName);
                                            lstImageCollection.Add(new LotDBEntity.Models.ImageCollection { ID = imgCnt, ImageName = hfGUID.Value + "~" + strCmpImgRandomName, ImageLocation = "pack://siteoforigin:,,,/" + strFolder + "/" + hfGUID.Value + "~" + strCmpImgRandomName, WEBImageLocation = folder + hfGUID.Value + "~" + strCmpImgRandomName, LastUpdatedTime = DateTime.Now });
                                            item1.SaveAs(Path.Combine(folder, hfGUID.Value + "~" + strCmpImgRandomName)); // ~ is for splitting later      
                                            i++;

                                        }
                                    }
                                }
                                if (lstImageCollection.Count > 0)
                                {
                                    item.ImageName = lstImageCollection[0].ImageName;
                                    item.ImageLocation = lstImageCollection[0].ImageLocation;
                                }

                                item.LstImages = lstImageCollection;
                                if (radQwerty.SelectedItem != null && radQwerty.SelectedItem.Text == "Yes")
                                {
                                    item.Quert = true;
                                }
                                else
                                {
                                    item.Quert = false;
                                }
                                if (radgprs.SelectedItem != null && radgprs.SelectedItem.Text == "Yes")
                                {
                                    item.GPRS = true;
                                }
                                else
                                {
                                    item.GPRS = false;
                                }
                                if (radedge.SelectedItem != null && radedge.SelectedItem.Text == "Yes")
                                {
                                    item.EDGE = true;
                                }
                                else
                                {
                                    item.EDGE = false;
                                }
                                if (rad3g.SelectedItem != null && rad3g.SelectedItem.Text == "Yes")
                                {
                                    item.Supports3G = true;
                                }
                                else
                                {
                                    item.Supports3G = false;
                                }
                                if (radBluetooth.SelectedItem != null && radBluetooth.SelectedItem.Text == "Yes")
                                {
                                    item.Bluetooth = true;
                                }
                                else
                                {
                                    item.Bluetooth = false;
                                }
                                if (radtouch.SelectedItem != null && radtouch.SelectedItem.Text == "Yes")
                                {
                                    item.TouchScreen = true;
                                }
                                else
                                {
                                    item.TouchScreen = false;
                                }
                                long id1 = 0;
                                if (Apps.lotContext.BasicItems.ToList().Count > 0)
                                    id1 = Apps.lotContext.BasicItems.Max(c => c.ID);
                                if (id1 > 0)
                                {
                                    item.ID = id1 + 1;
                                }
                                else
                                {
                                    item.ID = 1;
                                }
                                item.DefaultPrice = Convert.ToDecimal(txtprice.Text);
                                if (!string.IsNullOrEmpty(txtptype.Text))
                                {
                                    item.ProcessorType = txtptype.Text.ToUpper();
                                }
                                else
                                {
                                    item.ProcessorType = "NOT AVAILABLE";
                                }
                                if (!string.IsNullOrEmpty(txtpspeed.Text))
                                {
                                    item.ProcessorSpeed = txtpspeed.Text.ToUpper();
                                }
                                else
                                {
                                    item.ProcessorSpeed = "NOT AVAILABLE";
                                }
                                if (!string.IsNullOrEmpty(txtdisplaytype.Text))
                                {
                                    item.DisplayType = txtdisplaytype.Text.ToUpper();
                                }
                                else
                                {
                                    item.DisplayType = "NOT AVAILABLE";
                                }
                                if (!string.IsNullOrEmpty(txtdisplaysize.Text))
                                {
                                    item.DisplaySize = txtdisplaysize.Text.ToUpper();
                                }
                                else
                                {
                                    item.DisplaySize = "NOT AVAILABLE";
                                }
                                if (!string.IsNullOrEmpty(txtosname.Text))
                                {
                                    item.OSName = txtosname.Text.ToUpper();
                                }
                                else
                                {
                                    item.OSName = "NOT AVAILABLE";
                                }
                                if (!string.IsNullOrEmpty(txtosversion.Text))
                                {
                                    item.OSVersion = txtosversion.Text.ToUpper();
                                }
                                else
                                {
                                    item.OSVersion = "NOT AVAILABLE";
                                }
                                item.ExtraOfferDesc = txtOfferDesc.Text.ToUpper();
                                if (fpOfferImage.HasFile)
                                {
                                    string extension = Path.GetExtension(fpOfferImage.PostedFile.FileName);
                                    string fileName = Path.GetFileName(fpOfferImage.PostedFile.FileName);
                                    string strOffersPage = new Random().Next(1000, 100000).ToString() + System.IO.Path.GetExtension(fileName);
                                    string folder = Server.MapPath("~/OfferImage/");
                                    string strFolder = "OfferImage";
                                    Directory.CreateDirectory(folder);
                                    fpOfferImage.PostedFile.SaveAs(Path.Combine(folder, hfGUID.Value + "~" + strOffersPage));
                                    item.ExtraOfferImageName = hfGUID.Value + "~" + strOffersPage;
                                    item.ExtraOfferImageLocation = "pack://siteoforigin:,,,/" + strFolder + "/"+hfGUID.Value + "~"  + strOffersPage;
                                }
                                item.SIM = ddlsim.SelectedItem.Text;
                                item.SIMType = txtsimtype.Text.ToUpper();
                                item.Weight = txtweight.Text;
                                item.CameraSpecifications = txtcamspec.Text;
                                if (!string.IsNullOrEmpty(txtram.Text))
                                {
                                    item.RAM = txtram.Text.ToUpper();
                                }
                                else
                                {
                                    item.RAM = "NOT AVAILABLE";
                                }
                                if (!string.IsNullOrEmpty(txtinternalmem.Text))
                                {
                                    item.InternalStorage = txtinternalmem.Text.ToUpper();
                                }
                                else
                                {
                                    item.InternalStorage = "NOT AVAILABLE";
                                }
                                if (!string.IsNullOrEmpty(txtexternalmem.Text))
                                {
                                    item.ExternalStorage = txtexternalmem.Text.ToUpper();
                                }
                                else
                                {
                                    item.ExternalStorage = "NOT AVAILABLE";
                                }
                                item.Length = txtd1.Text.ToUpper();
                                item.Breadth = txtd2.Text.ToUpper();
                                item.Height = txtd3.Text.ToUpper();
                                if (!string.IsNullOrEmpty(txttalktime.Text))
                                {
                                    item.TalkTime = txttalktime.Text.ToUpper();
                                }
                                else
                                {
                                    item.TalkTime = "NOT AVAILABLE";
                                }
                                if (!string.IsNullOrEmpty(txtStandBy.Text))
                                {
                                    item.StandBy = txtStandBy.Text.ToUpper();
                                }
                                else
                                {
                                    item.StandBy = "NOT AVAILABLE";
                                }
                                if (radwifi.SelectedItem != null && radwifi.SelectedItem.Text == "Yes")
                                {
                                    item.Wifi = true;
                                }
                                else
                                {
                                    item.Wifi = false;
                                }
                                if (radmusic.SelectedItem != null && radmusic.SelectedItem.Text == "Yes")
                                {
                                    item.MusicPlayer = true;
                                }
                                else
                                {
                                    item.MusicPlayer = false;
                                }
                                if (radflash.SelectedItem != null && radflash.SelectedItem.Text == "Yes")
                                {
                                    item.FlashAvailable = true;
                                }
                                else
                                {
                                    item.FlashAvailable = false;
                                }
                                if (!string.IsNullOrEmpty(txtverdict.Text))
                                {
                                    item.Verdict = txtverdict.Text.ToUpper();
                                }
                                else
                                {
                                    item.Verdict = "NOT AVAILABLE";
                                }
                                if (!string.IsNullOrEmpty(txtpros.Text))
                                {
                                    item.Pros = txtpros.Text.ToUpper();
                                }
                                else
                                {
                                    item.Pros = "NOT AVAILABLE";
                                }
                                if (!string.IsNullOrEmpty(txtcons.Text))
                                {
                                    item.Cons = txtcons.Text.ToUpper();
                                }
                                else
                                {
                                    item.Cons = "NOT AVAILABLE";
                                }
                                if (!string.IsNullOrEmpty(txtscamera.Text))
                                {
                                    item.SecondaryCamera = txtscamera.Text.ToUpper();
                                }
                                else
                                {
                                    item.SecondaryCamera = "NOT AVAILABLE";
                                }
                                if (!string.IsNullOrEmpty(txtpcamera.Text))
                                {
                                    item.PrimaryCamera = txtpcamera.Text.ToUpper();
                                }
                                else
                                {
                                    item.PrimaryCamera = "NOT AVAILABLE";
                                }
                                if (!string.IsNullOrEmpty(txtbatterytype.Text))
                                {
                                    item.BatteryType = txtbatterytype.Text.ToUpper();
                                }
                                else
                                {
                                    item.BatteryType = "NOT AVAILABLE";
                                }
                                item.ItemBrand = brand;
                                item.ItemCateg = Categ;
                                item.ItemModel = model;
                                item.LastUpdatedTime = DateTime.Now;
                                Apps.lotContext.BasicItems.Add(item);
                                Apps.lotContext.SaveChanges();
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Item saved successfully ');", true);
                                //ScriptManager.RegisterStartupScript(this, this.GetType(), "name", "SuccessMsg();", true);
                                ClearAll();
                                ddlbrand.ClearSelection();
                                ddlbrand.Items.FindByText("Select").Selected = true;
                                ddlcateg.Items.Clear();
                                ddlcateg.Items.Add("Select");
                                ddlmodel.Items.Clear();
                                ddlmodel.Items.Add("Select");
                                btnSubmit.Enabled = true;
                            }
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Enter mandatory fields');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Maximum 4 Images Only || Video Size Should not Exceed 70MB');", true);
                    if (fpimg.PostedFiles.Count > 4)
                    {
                        fperror.Text = "Maximum 4 Images Only";
                    }
                    else
                    {
                        fperror.Text = "";
                    }
                    if (fpUploadImg.PostedFiles.Count > 4)
                    {
                        myLabel.Text = "Maximum 4 Images Only";
                    }
                    else
                    {
                        myLabel.Text = "";
                    }
                    if (fpUploadVideo.PostedFile.ContentLength > 70 * 1024 * 1024)
                    {
                        myLabel1.Text = "Video Size Should Not Exceed 70MB";
                    }
                    else
                    {
                        myLabel1.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception: Submit  : AddItem: " + ex.Message, ex);
            }
        }
        else if (btnSubmit.Text == "Update")
        {
            try
            {
                //LotDBEntity.Models.BasicItem item;

                if (fpimg.PostedFiles.Count < 5 && fpUploadImg.PostedFiles.Count < 5 && fpUploadVideo.PostedFile.ContentLength < 70 * 1024 * 1024)
                {
                    if (txtexist.Text != "Already Exists" && ddlmodel.SelectedItem.Text != "Select" && ddlcateg.SelectedItem.Text != "Select" && ddlbrand.SelectedItem.Text != "Select")
                    {
                        lock (lockTarget)
                        {
                            string strName = txtitemname.Text.ToUpper().Trim();
                            long val2 = Convert.ToInt64(ddlmodel.SelectedValue);
                            lock (lockTarget)
                            {
                                model = Apps.lotContext.Models.Where(c => c.SNo == val2).FirstOrDefault();
                            }
                            long Id = Convert.ToInt64(Request.QueryString["SNo"]);
                            lblId.Text = Request.QueryString["SNo"].ToString();
                            lock (lockTarget)
                            {
                                LotDBEntity.Models.BasicItem SelectedItem = Apps.lotContext.BasicItems.Where(c => c.SNo == Id).FirstOrDefault();

                                if (SelectedItem.ItemName != txtitemname.Text.Trim())
                                {
                                    CheckItemNameExist();
                                }
                            }
                            if (txtexist.Text == "Already Exists")
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Item name already exists');", true);
                            }
                            else
                            {
                                btnSubmit.Enabled = false;
                                lblId.Text = Request.QueryString["SNo"].ToString();
                                lock (lockTarget)
                                {
                                    LotDBEntity.Models.BasicItem item2 = Apps.lotContext.BasicItems.Where(c => c.SNo == Id).FirstOrDefault();
                                    //item = new LotDBEntity.Models.BasicItem();
                                    item2.ItemName = txtitemname.Text.ToUpper().Trim();
                                    item2.ItemColor = txtcolor.Text.ToUpper();

                                    List<LotDBEntity.Models.ItemCamImgsAndVideos> lstImagesAndVideos = new List<LotDBEntity.Models.ItemCamImgsAndVideos>();

                                    if (fpUploadImg.HasFiles)
                                    {
                                        long Sno = Convert.ToInt64(lblId.Text);
                                        lock (lockTarget)
                                        {
                                            List<LotDBEntity.Models.ItemCamImgsAndVideos> ImgCollect1 = Apps.lotContext.ItemCamImgsVideos.Where(c => c.basicItem.SNo == Sno && c.IsVideo == false).ToList();
                                            foreach (var ritem in ImgCollect1)
                                            {
                                                Apps.lotContext.ItemCamImgsVideos.Remove(ritem);
                                                Apps.lotContext.SaveChanges();

                                            }
                                        }
                                        string folder = Server.MapPath("~/CompareImagesAndVideos/");
                                        lock (lockTarget)
                                        {
                                            long id = 0;
                                            long strID;
                                            if (Apps.lotContext.ItemCamImgsVideos.ToList().Count > 0)
                                                id = Apps.lotContext.ItemCamImgsVideos.Max(c => c.ID);
                                            if (id > 0)
                                            {
                                                strID = id;
                                            }
                                            else
                                            {
                                                strID = 1;
                                            }


                                            int j = 1;
                                            foreach (HttpPostedFile Img in fpUploadImg.PostedFiles)
                                            {
                                                strID = id + 1;
                                                string Imagesfolder1 = Server.MapPath("~/CompareImagesAndVideos/");
                                                string strImagesFolder = "CompareImagesAndVideos";
                                                string strRandomName = txtitemname.Text + j + System.IO.Path.GetExtension(Img.FileName);
                                                lstImagesAndVideos.Add(new LotDBEntity.Models.ItemCamImgsAndVideos { ID = strID, Name = hfGUID.Value + "~" + strRandomName, Location = "pack://siteoforigin:,,,/" + strImagesFolder + "/" + hfGUID.Value + "~" + strRandomName, WebLocation = Imagesfolder1 + hfGUID.Value + "~" + strRandomName, IsVideo = false, LastUpdatedTime = DateTime.Now });
                                                Img.SaveAs((Path.Combine(Imagesfolder1, hfGUID.Value + "~" + strRandomName)));
                                                j++;
                                            }
                                        }
                                    }

                                    if (fpUploadVideo.HasFile)
                                    {

                                        long Sno = Convert.ToInt64(lblId.Text);
                                        if (Apps.lotContext.ItemCamImgsVideos.Where(c => c.basicItem.SNo == Sno && c.IsVideo == true).ToList().Count > 0)
                                        {
                                            lock (lockTarget)
                                            {
                                                LotDBEntity.Models.ItemCamImgsAndVideos ImgCollect2 = Apps.lotContext.ItemCamImgsVideos.Where(c => c.basicItem.SNo == Sno && c.IsVideo == true).FirstOrDefault();
                                                Apps.lotContext.ItemCamImgsVideos.Remove(ImgCollect2);
                                                Apps.lotContext.SaveChanges();
                                            }
                                        }
                                        lock (lockTarget)
                                        {
                                            long id = 0;
                                            long strID;
                                            if (Apps.lotContext.ItemCamImgsVideos.ToList().Count > 0)
                                                id = Apps.lotContext.ItemCamImgsVideos.Max(c => c.ID);
                                            if (id > 0)
                                            {
                                                strID = id;
                                            }
                                            else
                                            {
                                                strID = 1;
                                            }
                                           
                                            int x = 10;
                                            string Imagesfolder2 = Server.MapPath("~/CompareImagesAndVideos/");

                                            string strImagesFolder = "CompareImagesAndVideos";
                                            //string strCmpVideoRandomName = txtitemname.Text + x + System.IO.Path.GetExtension(fpUploadVideo.FileName);
                                            //lstImagesAndVideos.Add(new LotDBEntity.Models.ItemCamImgsAndVideos { ID = strID, Name = hfGUID.Value + "~" + strCmpVideoRandomName, Location = "pack://siteoforigin:,,,/" + strImagesFolder + "/" + hfGUID.Value + "~" + strCmpVideoRandomName, WebLocation = Imagesfolder2 + hfGUID.Value + "~" + strCmpVideoRandomName, IsVideo = true, LastUpdatedTime = DateTime.Now });
                                            //HttpPostedFile Video = fpUploadVideo.PostedFile;
                                            //Video.SaveAs((Path.Combine(Imagesfolder2, hfGUID.Value + "~" + strCmpVideoRandomName)));
                                            string strCmpVideoRandomName = txtitemname.Text + x + System.IO.Path.GetExtension(fpUploadVideo.FileName);
                                            lstImagesAndVideos.Add(new LotDBEntity.Models.ItemCamImgsAndVideos { ID = strID, Name = hfGUID.Value + "~" + strCmpVideoRandomName, Location = "pack://siteoforigin:,,,/" + strImagesFolder + "/" + hfGUID.Value + "~" + strCmpVideoRandomName, WebLocation = Imagesfolder2 + hfGUID.Value + "~" + strCmpVideoRandomName, IsVideo = true, LastUpdatedTime = DateTime.Now });
                                            HttpPostedFile Video = fpUploadVideo.PostedFile;
                                            Video.SaveAs((Path.Combine(Imagesfolder2, hfGUID.Value + "~" + strCmpVideoRandomName)));


                                        }
                                    }
                                    item2.LstImgsAndVideos = lstImagesAndVideos;
                                    List<LotDBEntity.Models.ImageCollection> lstImageCollection = new List<LotDBEntity.Models.ImageCollection>();

                                    if (fpimg.HasFiles)
                                    {
                                        lock (lockTarget)
                                        {
                                            long Sno = Convert.ToInt64(lblId.Text);
                                            List<LotDBEntity.Models.ImageCollection> ImgCollect = Apps.lotContext.ImageCollection.Where(c => c.basicItem.SNo == Sno).ToList();
                                            foreach (var ritem in ImgCollect)
                                            {
                                                Apps.lotContext.ImageCollection.Remove(ritem);
                                                Apps.lotContext.SaveChanges();
                                            }
                                        }
                                        //sGuid = hfGUID.Value;
                                        lock (lockTarget)
                                        {
                                            string folder = Server.MapPath("~/ItemImages/");

                                            long id = 0;
                                            long strID;
                                            if (Apps.lotContext.ImageCollection.ToList().Count > 0)
                                                id = Apps.lotContext.ImageCollection.Max(c => c.ID);
                                            if (id > 0)
                                            {
                                                strID = id;
                                            }
                                            else
                                            {
                                                strID = 1;
                                            }

                                            int i = 1;
                                            foreach (HttpPostedFile item1 in fpimg.PostedFiles)
                                            {
                                                strID = strID + 1;
                                                string strFolder = "ItemImages";
                                                string strCmpImgRandomName = txtitemname.Text + i + System.IO.Path.GetExtension(item1.FileName);
                                                lstImageCollection.Add(new LotDBEntity.Models.ImageCollection { ID = strID, ImageName = hfGUID.Value + "~" + strCmpImgRandomName, ImageLocation = "pack://siteoforigin:,,,/" + strFolder + "/" + hfGUID.Value + "~" + strCmpImgRandomName, WEBImageLocation = folder + hfGUID.Value + "~" + strCmpImgRandomName, LastUpdatedTime = DateTime.Now });
                                                item1.SaveAs(Path.Combine(folder, hfGUID.Value + "~" + strCmpImgRandomName)); // ~ is for splitting later           
                                                item2.LstImages = lstImageCollection;
                                                i++;
                                            }

                                            if (lstImageCollection.Count > 0)
                                            {
                                                item2.ImageName = lstImageCollection[0].ImageName;
                                                item2.ImageLocation = lstImageCollection[0].ImageLocation;
                                            }
                                        }
                                    }


                                    if (radQwerty.SelectedItem != null && radQwerty.SelectedItem.Text == "Yes")
                                    {
                                        item2.Quert = true;
                                    }
                                    else
                                    {
                                        item2.Quert = false;
                                    }
                                    if (radgprs.SelectedItem != null && radgprs.SelectedItem.Text == "Yes")
                                    {
                                        item2.GPRS = true;
                                    }
                                    else
                                    {
                                        item2.GPRS = false;
                                    }
                                    if (radedge.SelectedItem != null && radedge.SelectedItem.Text == "Yes")
                                    {
                                        item2.EDGE = true;
                                    }
                                    else
                                    {
                                        item2.EDGE = false;
                                    }
                                    if (rad3g.SelectedItem != null && rad3g.SelectedItem.Text == "Yes")
                                    {
                                        item2.Supports3G = true;
                                    }
                                    else
                                    {
                                        item2.Supports3G = false;
                                    }
                                    if (radBluetooth.SelectedItem != null && radBluetooth.SelectedItem.Text == "Yes")
                                    {
                                        item2.Bluetooth = true;
                                    }
                                    else
                                    {
                                        item2.Bluetooth = false;
                                    }
                                    if (radtouch.SelectedItem != null && radtouch.SelectedItem.Text == "Yes")
                                    {
                                        item2.TouchScreen = true;
                                    }
                                    else
                                    {
                                        item2.TouchScreen = false;
                                    }

                                    item2.DefaultPrice = Convert.ToDecimal(txtprice.Text);
                                    if (!string.IsNullOrEmpty(txtptype.Text))
                                    {
                                        item2.ProcessorType = txtptype.Text.ToUpper();
                                    }
                                    else
                                    {
                                        item2.ProcessorType = "NOT AVAILABLE";
                                    }
                                    if (!string.IsNullOrEmpty(txtpspeed.Text))
                                    {
                                        item2.ProcessorSpeed = txtpspeed.Text.ToUpper();
                                    }
                                    else
                                    {
                                        item2.ProcessorSpeed = "NOT AVAILABLE";
                                    }
                                    if (!string.IsNullOrEmpty(txtdisplaytype.Text))
                                    {
                                        item2.DisplayType = txtdisplaytype.Text.ToUpper();
                                    }
                                    else
                                    {
                                        item2.DisplayType = "NOT AVAILABLE";
                                    }
                                    if (!string.IsNullOrEmpty(txtdisplaysize.Text))
                                    {
                                        item2.DisplaySize = txtdisplaysize.Text.ToUpper();
                                    }
                                    else
                                    {
                                        item2.DisplaySize = "NOT AVAILABLE";
                                    }
                                    if (!string.IsNullOrEmpty(txtosname.Text))
                                    {
                                        item2.OSName = txtosname.Text.ToUpper();
                                    }
                                    else
                                    {
                                        item2.OSName = "NOT AVAILABLE";
                                    }
                                    if (!string.IsNullOrEmpty(txtosversion.Text))
                                    {
                                        item2.OSVersion = txtosversion.Text.ToUpper();
                                    }
                                    else
                                    {
                                        item2.OSVersion = "NOT AVAILABLE";
                                    }
                                    item2.ExtraOfferDesc = txtOfferDesc.Text.ToUpper();
                                    if (fpOfferImage.HasFile)
                                    {
                                        string extension = Path.GetExtension(fpOfferImage.PostedFile.FileName);
                                        string fileName = Path.GetFileName(fpOfferImage.PostedFile.FileName);
                                        string strOffersPage = new Random().Next(1000, 100000).ToString() + System.IO.Path.GetExtension(fileName);
                                        string folder = Server.MapPath("~/OfferImage/");
                                        string strFolder = "OfferImage";
                                        Directory.CreateDirectory(folder);
                                        fpOfferImage.PostedFile.SaveAs(Path.Combine(folder, hfGUID.Value + "~" + strOffersPage));
                                        item2.ExtraOfferImageName = hfGUID.Value + "~" + strOffersPage;
                                        item2.ExtraOfferImageLocation = "pack://siteoforigin:,,,/" + strFolder + "/" + hfGUID.Value + "~" + strOffersPage;
                                    }
                                    item2.SIM = ddlsim.SelectedItem.Text;
                                    item2.SIMType = txtsimtype.Text.ToUpper();
                                    item2.Weight = txtweight.Text;
                                    item2.CameraSpecifications = txtcamspec.Text;
                                    if (!string.IsNullOrEmpty(txtram.Text))
                                    {
                                        item2.RAM = txtram.Text.ToUpper();
                                    }
                                    else
                                    {
                                        item2.RAM = "NOT AVAILABLE";
                                    }
                                    if (!string.IsNullOrEmpty(txtinternalmem.Text))
                                    {
                                        item2.InternalStorage = txtinternalmem.Text.ToUpper();
                                    }
                                    else
                                    {
                                        item2.InternalStorage = "NOT AVAILABLE";
                                    }
                                    if (!string.IsNullOrEmpty(txtexternalmem.Text))
                                    {
                                        item2.ExternalStorage = txtexternalmem.Text.ToUpper();
                                    }
                                    else
                                    {
                                        item2.ExternalStorage = "NOT AVAILABLE";
                                    }
                                    item2.Length = txtd1.Text;
                                    item2.Breadth = txtd2.Text;
                                    item2.Height = txtd3.Text;
                                    if (!string.IsNullOrEmpty(txttalktime.Text))
                                    {
                                        item2.TalkTime = txttalktime.Text.ToUpper();
                                    }
                                    else
                                    {
                                        item2.TalkTime = "NOT AVAILABLE";
                                    }
                                    if (!string.IsNullOrEmpty(txtStandBy.Text))
                                    {
                                        item2.StandBy = txtStandBy.Text.ToUpper();
                                    }
                                    else
                                    {
                                        item2.StandBy = "NOT AVAILABLE";
                                    }
                                    if (radwifi.SelectedItem != null && radwifi.SelectedItem.Text == "Yes")
                                    {
                                        item2.Wifi = true;
                                    }
                                    else
                                    {
                                        item2.Wifi = false;
                                    }
                                    if (radmusic.SelectedItem != null && radmusic.SelectedItem.Text == "Yes")
                                    {
                                        item2.MusicPlayer = true;
                                    }
                                    else
                                    {
                                        item2.MusicPlayer = false;
                                    }
                                    if (radflash.SelectedItem != null && radflash.SelectedItem.Text == "Yes")
                                    {
                                        item2.FlashAvailable = true;
                                    }
                                    else
                                    {
                                        item2.FlashAvailable = false;
                                    }
                                    if (!string.IsNullOrEmpty(txtverdict.Text))
                                    {
                                        item2.Verdict = txtverdict.Text.ToUpper();
                                    }
                                    else
                                    {
                                        item2.Verdict = "NOT AVAILABLE";
                                    }
                                    if (!string.IsNullOrEmpty(txtpros.Text))
                                    {
                                        item2.Pros = txtpros.Text.ToUpper();
                                    }
                                    else
                                    {
                                        item2.Pros = "NOT AVAILABLE";
                                    }
                                    if (!string.IsNullOrEmpty(txtcons.Text))
                                    {
                                        item2.Cons = txtcons.Text.ToUpper();
                                    }
                                    else
                                    {
                                        item2.Cons = "NOT AVAILABLE";
                                    }
                                    if (!string.IsNullOrEmpty(txtscamera.Text))
                                    {
                                        item2.SecondaryCamera = txtscamera.Text.ToUpper();
                                    }
                                    else
                                    {
                                        item2.SecondaryCamera = "NOT AVAILABLE";
                                    }
                                    if (!string.IsNullOrEmpty(txtpcamera.Text))
                                    {
                                        item2.PrimaryCamera = txtpcamera.Text.ToUpper();
                                    }
                                    else
                                    {
                                        item2.PrimaryCamera = "NOT AVAILABLE";
                                    }
                                    if (!string.IsNullOrEmpty(txtbatterytype.Text))
                                    {
                                        item2.BatteryType = txtbatterytype.Text.ToUpper();
                                    }
                                    else
                                    {
                                        item2.BatteryType = "NOT AVAILABLE";
                                    }
                                    item2.LastUpdatedTime = DateTime.Now;
                                    //Apps.lotContext.SaveChanges();

                                    List<LotDBEntity.Models.SimilarProducts> lstSimilarProds = Apps.lotContext.SimilarProducts.Where(x => x.ItemSKU == item2.SNo).ToList();
                                    lstSimilarProds.ForEach(y =>
                                    {
                                        y.ImageLocation = item2.ImageLocation;
                                        y.ImageName = item2.ImageName;
                                    });
                                    Apps.lotContext.SaveChanges();

                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Item Updated Successfully ');window.location='ViewItem.aspx'", true);
                                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "name", "UpdateMsg();", true);

                                }
                            }
                        }
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Maximum 4 Images Only || Video Size Should not Exceed 70MB');", true);
                    if (fpimg.PostedFiles.Count > 4)
                    {
                        fperror.Text = "Maximum 4 Images Only";
                    }
                    else
                    {
                        fperror.Text = "";
                    }
                    if (fpUploadImg.PostedFiles.Count > 4)
                    {
                        myLabel.Text = "Maximum 4 Images Only";
                    }
                    else
                    {
                        myLabel.Text = "";
                    }
                    if (fpUploadVideo.PostedFile.ContentLength > 70 * 1024 * 1024)
                    {
                        myLabel1.Text = "Video Size Should Not Exceed 70MB";
                    }
                    else
                    {
                        myLabel1.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception: Update  : AddItem: " + ex.Message, ex);
            }
        }
    }

    private void ClearAll()
    {
        txtitemname.Text = "";
        txtd1.Text = "0";
        txtd2.Text = "0";
        txtd3.Text = "0";
        txtcolor.Text = "";
        txtweight.Text = "0";
        txtinternalmem.Text = "";
        txtexternalmem.Text = "";
        txtpcamera.Text = "";
        txtscamera.Text = "";
        txtcamspec.Text = "";
        txtOfferDesc.Text = "";
        txtptype.Text = "";
        txtpspeed.Text = "";
        txtram.Text = "";
        txtbatterytype.Text = "";
        txttalktime.Text = "";
        txtStandBy.Text = "";
        txtdisplaytype.Text = "";
        txtdisplaysize.Text = "";
        txtprice.Text = "0";
        txtosname.Text = "";
        txtosversion.Text = "";
        txtsimtype.Text = "";
        txtverdict.Text = "";
        txtpros.Text = "";
        txtcons.Text = "";
        errtxtPrice.Text = "";
        errBrand.Text = "";
        errCateg.Text = "";
        errModel.Text = "";
        fperror.Text = "";
        ErrFUimages.Text = "";
        ErrFUvideo.Text = "";
        ErrItemNull.Text = "";
        txtexist.Text = "";
        ErrMsg.Text = "";
        rad3g.ClearSelection();
        radBluetooth.ClearSelection();
        radedge.ClearSelection();
        radwifi.ClearSelection();
        radgprs.ClearSelection();
        radflash.ClearSelection();
        radtouch.ClearSelection();
        radmusic.ClearSelection();
        radQwerty.ClearSelection();

    }
    protected void ddlbrand_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlbrand.SelectedItem.Text != "Select")
        {
            ddlcateg.Items.Clear();
            ddlmodel.Items.Clear();
            long val = Convert.ToInt64(ddlbrand.SelectedValue);
            LotDBEntity.Models.Brands brand = Apps.lotContext.Brands.Where(c => c.SNo == val).FirstOrDefault();
            if (brand.LstCategory == null)
            {
                ddlcateg.Items.Clear();
                ddlcateg.Items.Add("Select");
                ddlmodel.Items.Clear();
                ddlmodel.Items.Add("Select");
            }
            else
            {
                ddlcateg.Items.Add("Select");
                ddlmodel.Items.Add("Select");
                ddlcateg.DataSource = brand.LstCategory;
                ddlcateg.DataTextField = "Name";
                ddlcateg.DataValueField = "SNo";
                ddlcateg.DataBind();
            }
        }
        else
        {
            ddlcateg.Items.Clear();
            ddlcateg.Items.Add("Select");
            ddlmodel.Items.Clear();
            ddlmodel.Items.Add("Select");
            ClearAll();
        }
    }
    protected void ddlcateg_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlcateg.SelectedItem.Text != "Select")
        {
            ddlmodel.Items.Clear();
            long val = Convert.ToInt64(ddlcateg.SelectedValue);
            LotDBEntity.Models.Category Categ = Apps.lotContext.Categories.Where(c => c.SNo == val).FirstOrDefault();
            if (Categ.LstModels == null)
            {
                ClearAll();
                ddlmodel.Items.Clear();
                ddlmodel.Items.Add("Select");
            }
            else
            {
                ddlmodel.Items.Clear();
                ddlmodel.DataSource = Categ.LstModels;
                ddlmodel.DataTextField = "Name";
                ddlmodel.DataValueField = "SNo";
                ddlmodel.DataBind();
            }
        }
        else
        {
            ddlmodel.Items.Clear();
            ddlmodel.Items.Add("Select");
            ClearAll();
        }
    }
    protected void txtitemname_TextChanged(object sender, EventArgs e)
    {
        CheckItemNameExist();
    }

    private void CheckItemNameExist()
    {
        if (ddlbrand.SelectedItem.Text != "Select" && ddlmodel.SelectedItem.Text != "Select" && ddlcateg.SelectedItem.Text != "Select")
        {
            LotDBEntity.Models.BasicItem item = null;
            string strName = txtitemname.Text.ToUpper().Trim();
            long val = Convert.ToInt64(ddlmodel.SelectedValue);
            lock (lockTarget)
            {
                model = Apps.lotContext.Models.Where(c => c.SNo == val).FirstOrDefault();
                if (model != null)
                {
                    if (model.LstBasicItems != null)
                    {
                        lock (lockTarget)
                        {
                            item = model.LstBasicItems.Where(c => c.ItemName == strName).FirstOrDefault();
                        }
                    }
                    else
                    {
                        item = null;
                    }
                }
            }
            if (item != null)
            {
                txtexist.Text = "Already Exists";
            }
            else
            {
                item = new LotDBEntity.Models.BasicItem();
                txtexist.Text = "";
            }
        }
    }
    protected void ddlmodel_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlmodel.SelectedItem.Text == "Select")
        {
            ClearAll();
        }
       
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        if (btnCancel.Text == "Cancel")
        {
            ClearAll();
            ddlbrand.ClearSelection();
            ddlbrand.Items.FindByText("Select").Selected = true;
            ddlcateg.Items.Clear();
            ddlcateg.Items.Add("Select");
            ddlmodel.Items.Clear();
            ddlmodel.Items.Add("Select");
        }
        else
        {
            getUpdatedData();
        }
    }    
}