﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMasterPage.master" AutoEventWireup="true" CodeFile="AddPriceRange.aspx.cs" Inherits="AddPriceRange" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:GridView ID="gdview" runat="server" AutoGenerateColumns="false" DataKeyNames="SNo" GridLines="Horizontal" Width="500px" BackColor="WhiteSmoke" CellSpacing="5" CellPadding="5" BorderColor="LightGray" AllowPaging="True" OnRowEditing="gdview_RowEditing" OnRowUpdating="gdview_RowUpdating" OnRowCancelingEdit="gdview_RowCancelingEdit"    Style="margin:13% 0% 0% 35%;">
       <Columns>
  
           <asp:TemplateField HeaderText="Price From">
               <EditItemTemplate>
                   <asp:TextBox ID="txtPriceFrom" runat="server" Text='<%#Eval("PriceFrom") %>'></asp:TextBox>
               </EditItemTemplate>
               <ItemTemplate>
                   <asp:Label ID="lblFrom" runat="server" Text='<%#Eval("PriceFrom") %>'></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           <asp:TemplateField HeaderText="Price To">
               <EditItemTemplate>
                   <asp:TextBox ID="txtPriceTo" runat="server" Text='<%#Eval("PriceTo") %>'></asp:TextBox>
               </EditItemTemplate>
               <ItemTemplate>
                   <asp:Label ID="lblTo" runat="server" Text='<%#Eval("PriceTo") %>'></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
            <asp:TemplateField HeaderText="Set Order" SortExpression="ID">
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtorderid" runat="server" Text='<%# Bind("ID") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblorderid" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                   <asp:TemplateField ShowHeader="False">
                                <EditItemTemplate>
                                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                                    &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                                    &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
       </Columns>
        <EditRowStyle BackColor="#999999" />
    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White"  />
    <HeaderStyle BackColor="White" ForeColor="GrayText" BorderColor="LightGray" />
                        <RowStyle BorderColor="LightGray" HorizontalAlign="Center" />
    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
   <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
    <SortedAscendingCellStyle BackColor="#E9E7E2" />
    <SortedAscendingHeaderStyle BackColor="#506C8C" />
    <SortedDescendingCellStyle BackColor="#FFFDF8"/>
    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />

   </asp:GridView>
</asp:Content>

