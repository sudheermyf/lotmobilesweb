﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ViewCategories : System.Web.UI.Page
{
    List<LotDBEntity.Models.Category> SelectCateg = new List<LotDBEntity.Models.Category>();
    object lockTarget = new object();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LotAdminUser"] == null)
        {
            Response.Redirect("Default.aspx");
        }
        else
        {
            if (!IsPostBack)
            {
                LogHelper.Logger.Info("Starts when PageLoad in ViewCategories");
                lock (lockTarget)
                {
                    ddlBrand.DataSource = Apps.lotContext.Brands.ToList();
                    ddlBrand.DataBind();
                    lblDesc.ReadOnly = true;
                }
                LogHelper.Logger.Info("Ends when PageLoad in ViewCategories");
            }
        }
    }

    protected void lstCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (lstCategory.Items.Count > 0)
        {
            lock (lockTarget)
            {
                long val = Convert.ToInt64(lstCategory.SelectedValue);
                LotDBEntity.Models.Category SelectedCateg = Apps.lotContext.Categories.Where(c => c.SNo == val).FirstOrDefault();
                lblCategoryName.Text = SelectedCateg.Name;
                lblDesc.Text = SelectedCateg.Description;
                lblId.Text = SelectedCateg.SNo.ToString();
                lstCategory.Focus();
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Please Select One Item in the List ');", true);

        }
    }
    protected void ddlBrand_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(ddlBrand.SelectedItem.Text))
        {
            lblCategoryName.Text ="";
            lblDesc.Text ="";
            lblId.Text = "";
            lock (lockTarget)
            {
                string SelectedBrand = ddlBrand.SelectedItem.ToString();
                SelectCateg = Apps.lotContext.Categories.Where(c => c.Brand.Name == SelectedBrand).ToList();
                lstCategory.DataSource = SelectCateg;
                lstCategory.DataBind();
                lstCategory.Focus();
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Please Select One Item in the Brand DropDown List ');", true);
            
        }

    }
   
    protected void btnEdit_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(lblId.Text))
        {
            Response.Redirect("AddCategories.aspx?SNo=" + lblId.Text + "");
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Please Select One Category Item in the List To Edit');", true);
        }
    }

    protected void brnDelete_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(lblId.Text))
        {
            lock (lockTarget)
            {
                long Id = Convert.ToInt64(lblId.Text);
                LotDBEntity.Models.Category categ = Apps.lotContext.Categories.Where(c => c.SNo == Id).FirstOrDefault();
                if (categ.LstModels != null && categ.LstModels.Count > 0 )
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Categories have Different Models Please Delete Models');", true);
                }
                else if (categ.LstAccessories != null && categ.LstAccessories.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Categories have  Different Accessories || Different Models Please Delete Accessories');", true);
                }
                else
                {
                    Apps.lotContext.Categories.Remove(categ);
                    Apps.lotContext.SaveChanges();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Category Deleted Sucessfully');window.location='ViewCategories.aspx'", true);
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "name", "ErrorMsg();", true);
                }
                
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Please Select One Category Item in the List To Delete');", true);
        }
    }
}