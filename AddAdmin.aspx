﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMasterPage.master" AutoEventWireup="true" CodeFile="AddAdmin.aspx.cs" Inherits="AddAdmin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <div style="margin:5% 0 0 0;text-align:left;padding: 2% 0 0% 10%;"><h2 style="font-size:28px;">Add Super-Admin</h2></div>
   <div style=" width: 100%;
padding: 3% 0 0 23%;margin: -4% 0 0 0;">
    <table style="width:70%; margin: 6% 0 10% 0%;">
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text="USER NAME"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtUserName" runat="server" placeholder="User Name" required="required"  width="50%"></asp:TextBox></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label2" runat="server" Text="PASSWORD"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtPassword" runat="server" placeholder="Password" TextMode="Password" required="required"></asp:TextBox></td>
        </tr>       
        <tr>
            <td>&nbsp;</td>
            <td>
                   <asp:Button ID="btnSubmit" runat="server" Text="Submit"  OnClick="btnSubmit_Click" CssClass="lotbtn"/>
            </td>
        </tr>
    </table>
       </div>
</asp:Content>

